<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'frontpage';

$route['perusahaan/login']					= 'login/login_perusahaan';
$route['perusahaan/register'] 				= 'login/register_perusahaan';
$route['perusahaan/reset-password'] 		= 'login/reset_password_perusahaan';
$route['perusahaan/reset/(:any)'] 		    = 'login/valid_reset_password/$1';
$route['perusahaan/out']                    = 'login/logout_perusahaan';

$route['pelamar/login']                     = 'login/login_pelamar';
$route['pelamar/register']                  = 'login/register_pelamar';
$route['pelamar/reset-password']            = 'login/reset_password_pelamar';
$route['pelamar/reset/(:any)/(:any)']		= 'login/valid_reset_password_pelamar/$1/$2';
$route['pelamar/out']						= 'login/logout_pelamar';
$route['pelamar/out-front']					= 'login/logout_pelamar_front';

$route['pelamar/admin']						= 'pelamar/index';
$route['pelamar/admin/profil']				= 'pelamar/view_profile';
$route['pelamar/admin/remove-resume/(:any)'] = 'pelamar/action_delete_resume/$1';
$route['pelamar/admin/remove-pendidikan/(:any)'] = 'pelamar/action_delete_data_pendidikan/$1';
$route['pelamar/admin/remove-pengalaman/(:any)'] = 'pelamar/action_delete_pengalaman/$1';
$route['pelamar/admin/download-cv/(:any)']       = 'pelamar/download_cv_pelamar/$1';

// $route['perusahaan/admin']     				= 'perusahaan/index';
$route['perusahaan/admin']     					= 'perusahaan/view_data_lowongan';
$route['perusahaan/admin/tambah_lowongan']  	= 'perusahaan/tambah_data_lowongan';
$route['perusahaan/admin/data_lowongan']    	= 'perusahaan/view_data_lowongan';
$route['perusahaan/admin/data_pelamar/(:any)'] 	  = 'perusahaan/data_pelamar/$1';
$route['perusahaan/admin/profil']           	  = 'perusahaan/profile_perusahaan';
$route['perusahaan/admin/edit-lowongan/(:any)']   = 'perusahaan/edit_data_lowongan/$1';
$route['perusahaan/admin/remove-lowongan/(:any)'] = 'perusahaan/action_delete_lowongan/$1';
$route['perusahaan/admin/remove-metode/(:any)']   = 'perusahaan/action_delete_metode_requiredment/$1';
$route['perusahaan/admin/cv_pdf/(:any)/(:any)']   = 'perusahaan/convert_pdf_cv/$1/$2';



// route superuser
$route['super/root'] 				= 'superroot';
$route['super/root/detail/(:any)']  = 'superroot/view_detail_lowongan/$1'; 
$route['super/root/data_pelamar']   = 'superroot/view_data_pelamar';


$route['perusahaan'] 						= 'frontpage/front_perusahaan';
$route['cari-lowongan/(:any)'] 				= 'frontpage/front_cari_lowongan/$1';
$route['pilih-wilayah']             		= 'frontpage/frontpage_provinsi_view';
$route['detail-lowongan/(:any)/(:any)'] 	= 'frontpage/frontpage_lowongan_detail/$1/$2';
$route['thanks-apply']       				= 'frontpage/thanks_apply_view';
$route['kontak']							= 'frontpage/front_kontak';
$route['peluang-usaha']                     = 'frontpage/front_peluang_usaha';

$route['503'] = 'frontpage/mantenance_view';


$route['404_override'] = 'frontpage';
$route['translate_uri_dashes'] = FALSE;
