<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');

		if($this->session->userdata('status_perusahaan') != "login_perusahaan"){
			redirect('perusahaan/login','refresh');
		}
		
	}

	public function index()
	{
		$this->load->view('perusahaan/page/home_view');
	}

	public function tambah_data_lowongan() {
		$this->load->model('Lib_model');
		$m_lib = new Lib_model();

		$data['data_provinsi'] = $m_lib->view_provinsi()->result();
		$this->load->view('perusahaan/page/tambah_data_lowongan_view', $data);
	}

	public function edit_data_lowongan($kode_lowongan) {
		$kode_perusahaan = $this->session->userdata('kode_perusahaan');
		$this->load->model('Lib_model');
		$m_lib = new Lib_model();

		$data['data_provinsi'] = $m_lib->view_provinsi()->result();
		$sql_lowongan    = "SELECT * FROM Tbl_Lowongan WHERE kode_lowongan = '$kode_lowongan' AND kode_perusahaan = '$kode_perusahaan'";

		$valid = $this->db->query($sql_lowongan)->num_rows();
		if ($valid > 0) {
			$data['data_lowongan'] =  $this->db->query($sql_lowongan)->result();
			$this->load->view('perusahaan/page/edit_data_lowongan_view', $data);
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Lowongan ini tidak tersedia.');
			redirect($this->agent->referrer());
		}
	}

	public function data_pelamar($kode_lowongan) {
		$this->load->model('Pelamar_model');
		$this->load->model('Querydetail_model');
		$m_pelamar = new Pelamar_model();
		$m_query_detail = new Querydetail_model();

		$data['data_apply'] = $m_query_detail->view_data_pelamar_detail_lowongan($kode_lowongan)->result();

		$this->load->view('perusahaan/page/data_pelamar_view', $data);
		// $sql = "SELECT * FROM Tbl_Lowongan WHERE slug_url = '$slug_lowongan'";
		// $valid = $this->db->query($sql)->num_rows();
		// if ($valid > 0) {
		// 	$data_lowongan = $this->db->query($sql)->row();
		// 	$data['data_pelamar'] = $m_pelamar->view_data_pelamar_by_id_lowongan('Tbl_Pelamar', $data_lowongan->kode_lowongan)->result();
		// 	$this->load->view('perusahaan/page/data_pelamar_view', $data);
		// } else {
		// 	redirect('perusahaan/admin/data_lowongan','refresh');
		// }
		
	}

	public function view_data_lowongan() {
		$this->load->model('Lowongan_model');
		$this->load->model('Lib_model');
		$m_lib = new Lib_model();

		$data['data_provinsi'] = $m_lib->view_provinsi()->result();
		$m_lowongan = new Lowongan_model();
		$kode_perusahaan = $this->session->userdata('kode_perusahaan');
		$data['data_lowongan'] = $m_lowongan->view_all_lowongan_and_perusahaan($kode_perusahaan)->result();

		$this->load->view('perusahaan/page/data_lowongan_view', $data);
	}

	public function profile_perusahaan() {
		$this->load->model('Perusahaan_model');
		$m_perusahaan = new Perusahaan_model();

		$kode_perusahaan = $this->session->userdata('kode_perusahaan');
		$apikey          = $this->session->userdata('api_key');

		$data['data_perusahaan'] = $m_perusahaan->find_perusahaan_by_kode_api('Tbl_Perusahaan', $apikey, $kode_perusahaan)->result();
		$this->load->view('perusahaan/page/profile_perusahaan_view', $data);
	}

	public function action_tambah_lowongan() {

		$this->load->model('Lib_model');
		$this->load->model('Lowongan_model');
		$m_lib = new Lib_model();
		$m_lowongan = new Lowongan_model();

		$kode_perusahaan  = htmlspecialchars($this->input->post('kode_perusahaan'));
		$nama_lowongan    = htmlspecialchars($this->input->post('nama_lowongan'));
		$pekerjaan_jenis  = htmlspecialchars($this->input->post('pekerjaan_jenis'));
		$posisi_spesialis = htmlspecialchars($this->input->post('posisi_spesialis'));
		$tanggal_tutup    = htmlspecialchars($this->input->post('tanggal_tutup'));
		$salary_max       = htmlspecialchars($this->input->post('salary_max'));
		$salary_min       = htmlspecialchars($this->input->post('salary_min'));
		$keahlian         = htmlspecialchars($this->input->post('keahlian'));
		$kategori_daerah  = implode(', ',$this->input->post('kategori_lowongan'));

		if ($this->input->post('show_salary') !== '') {
			$show_salary = htmlspecialchars($this->input->post('show_salary'));
		} else {
			$show_salary = '';
		}

		$tinggkat_pendidikan = htmlspecialchars($this->input->post('tinggkat_pendidikan'));
		$bidang_studi        = htmlspecialchars($this->input->post('bidang_studi'));
		$pengalaman          =  htmlspecialchars($this->input->post('pengalaman'));
		$deskripsi_pekerjaan = $this->input->post('deskripsi_pekerjaan');

		if (empty($this->input->post('show_requirement'))) {
			$show_requirement = '';
			$metode_rekut = "";
			$deskripsi_requirement = '';
		} else {
			$show_requirement = $this->input->post('show_requirement');
			$deskripsi_requirement = $this->input->post('deskripsi_requirement');
			$metode_rekut     = $this->input->post('tipe_metode_rekut');
		}

		$bln_thun = date('ym-');
		$apikey = implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 30), 6));

		if($_FILES['gambar_iklan']['error'] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Gambar tidak boleh kosong </strong>');
			redirect($this->agent->referrer());
		} else {
			
			$check_data_lowongan = $this->db->query("SELECT * FROM Tbl_Lowongan")->num_rows();

			if ($check_data_lowongan > 0) {
				$get_data_lowongan =  $this->db->order_by('kode_lowongan', 'desc')->get('Tbl_Lowongan')->result_array();
				$id_key_lowongan   = $get_data_lowongan[0]['kode_lowongan'];
				$kode_lowongan = $m_lowongan->buat_kode(substr($id_key_lowongan, 8), $bln_thun.'LWN', 8);

				$nama_gambar_iklan = $_FILES['gambar_iklan']['name'];
				$tmp_gambar_iklan  = $_FILES['gambar_iklan']['tmp_name'];
				$type_gambar_iklan = $_FILES['gambar_iklan']['type'];
				$nama_extension_gambar_iklan = pathinfo($nama_gambar_iklan, PATHINFO_EXTENSION);

				$slug_gambar = $m_lib->create_slug($nama_lowongan.'-'.$kode_perusahaan.'-'.$kode_lowongan);
				$buat_nama_gamabar_baru = $slug_gambar.'.'.$nama_extension_gambar_iklan;

				$config['upload_path'] = './assets_storage/img_iklan/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG';
				$config['max_size']  = '1024';
				$config['file_name'] = $buat_nama_gamabar_baru;
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('gambar_iklan')){
					$error = array('error' => $this->upload->display_errors());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

				$name_berkas_gamabaran_requirement      = $_FILES['gamabaran_requirement']['name'];
				$tmp_name_gamabaran_requirement      	= $_FILES['gamabaran_requirement']['tmp_name'];
				$error_gamabaran_requirement         	= $_FILES['gamabaran_requirement']['error'];
				$type_gamabaran_requirement   		 	= $_FILES['gamabaran_requirement']['type'];
				$file_name_requirement_extension 		= pathinfo($name_berkas_gamabaran_requirement, PATHINFO_EXTENSION);
				$slug_gambar_requirement                = $m_lib->create_slug('imgrekutmen-'.$kode_lowongan);
				$file_name_requirement                  = $slug_gambar_requirement.'.'.$file_name_requirement_extension;

				$dir_requirement 				= "./assets_storage/img_metode/";
				$extensi_file_requirement 		= array('png','jpg','jpeg','PNG','JPG');
				$new_file_requirement 			= $dir_requirement.$file_name_requirement;

				if ($_FILES['gamabaran_requirement']['error'] > 0) {
					$file_name_requirement = "";
				} else {
					if(!in_array($file_name_requirement_extension, $extensi_file_requirement)) {
						$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Type file untuk gambaran requirement tak mendukung.');
						redirect($this->agent->referrer());
					} else {
						move_uploaded_file($tmp_name_gamabaran_requirement, $new_file_requirement);
					}
				}

				$data_lowongan = array(
										'kode_lowongan' 	=> $kode_lowongan,
										'api_key'		  	=> $apikey,
										'slug_url'			=> $m_lib->create_slug($nama_lowongan.'-'.$apikey.'-'.$kode_lowongan),
										'kode_perusahaan' 	=> $kode_perusahaan,
										'nama_lowongan'	 	=> $nama_lowongan,
										'pekerjaan_jenis' 	=> $pekerjaan_jenis,
										'posisi_spesialis' 	=> $posisi_spesialis,
										'salary_max' 		=> $salary_max,
										'salary_min' 		=> $salary_min,
										'show_salary' 		=> $show_salary,
										'keahlian'			=> $keahlian,
										'tingkat_pendidikan' 	=> $tinggkat_pendidikan,
										'bidang_studi'			=> $bidang_studi,
										'pengalaman' 			=> $pengalaman,
										'deskripsi_pekerjaan'	=> $deskripsi_pekerjaan,
										'cover_iklan'			=> $buat_nama_gamabar_baru,
										'tanggal_tutup'			=> $tanggal_tutup,
										'tgl_insert'			=> date("Y-m-d"),
										'status_lowongan'		=> 'Active',
										'metode_rekut'           => $metode_rekut,
										'cover_metode_rekut'     => $file_name_requirement,
										'deskripsi_metode_rekut' => $deskripsi_requirement,
										'kategori_daerah'		 => $kategori_daerah,
										'status_requment'        => $show_requirement
									);

				$m_lowongan->insert_data_lowongan('Tbl_Lowongan', $data_lowongan);
				$this->session->set_flashdata('sendmessage', '<strong>Lowongan berhasil ditambahkan oleh sistem kami </strong>');
				// redirect($this->agent->referrer());
				redirect('perusahaan/admin/data_lowongan','refresh');

			} else {
				$kode_lowongan = $m_lib->buat_kode('', $bln_thun.'LWN', 8);

				$nama_gambar_iklan = $_FILES['gambar_iklan']['name'];
				$tmp_gambar_iklan  = $_FILES['gambar_iklan']['tmp_name'];
				$type_gambar_iklan = $_FILES['gambar_iklan']['type'];
				$nama_extension_gambar_iklan = pathinfo($nama_gambar_iklan, PATHINFO_EXTENSION);

				$slug_gambar = $m_lib->create_slug($nama_lowongan.'-'.$kode_perusahaan.'-'.$kode_lowongan);
				$buat_nama_gamabar_baru = $slug_gambar.'.'.$nama_extension_gambar_iklan;

				$config['upload_path'] = './assets_storage/img_iklan/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG';
				$config['max_size']  = '1024';
				$config['file_name'] = $buat_nama_gamabar_baru;
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('gambar_iklan')){
					$error = array('error' => $this->upload->display_errors());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}


				$name_berkas_gamabaran_requirement      = $_FILES['gamabaran_requirement']['name'];
				$tmp_name_gamabaran_requirement      	= $_FILES['gamabaran_requirement']['tmp_name'];
				$error_gamabaran_requirement         	= $_FILES['gamabaran_requirement']['error'];
				$type_gamabaran_requirement   		 	= $_FILES['gamabaran_requirement']['type'];
				$file_name_requirement_extension 		= pathinfo($name_berkas_gamabaran_requirement, PATHINFO_EXTENSION);
				$slug_gambar_requirement                = $m_lib->create_slug('imgrekutmen-'.$kode_lowongan);
				$file_name_requirement                  = $slug_gambar_requirement.'.'.$file_name_requirement_extension;

				$dir_requirement 				= "./assets_storage/img_metode/";
				$extensi_file_requirement 		= array('png','jpg','jpeg','PNG','JPG');
				$new_file_requirement 			= $dir_requirement.$file_name_requirement;

				if ($_FILES['gamabaran_requirement']['error'] > 0) {
					$file_name_requirement = "";
				} else {
					if(!in_array($file_name_requirement_extension, $extensi_file_requirement)) {
						$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Type file untuk gambaran requirement tak mendukung.');
						redirect($this->agent->referrer());
					} else {
						move_uploaded_file($tmp_name_gamabaran_requirement, $new_file_requirement);
					}
				}

				$data_lowongan = array(
										'kode_lowongan' 	=> $kode_lowongan,
										'api_key'		  	=> $apikey,
										'slug_url'			=> $m_lib->create_slug($nama_lowongan.'-'.$apikey.'-'.$kode_lowongan),
										'kode_perusahaan' 	=> $kode_perusahaan,
										'nama_lowongan'	 	=> $nama_lowongan,
										'pekerjaan_jenis' 	=> $pekerjaan_jenis,
										'posisi_spesialis' 	=> $posisi_spesialis,
										'salary_max' 		=> $salary_max,
										'salary_min' 		=> $salary_min,
										'show_salary' 		=> $show_salary,
										'keahlian'			=> $keahlian,
										'tingkat_pendidikan' 	=> $tinggkat_pendidikan,
										'bidang_studi'			=> $bidang_studi,
										'pengalaman' 			=> $pengalaman,
										'deskripsi_pekerjaan'	=> $deskripsi_pekerjaan,
										'cover_iklan'			=> $buat_nama_gamabar_baru,
										'tanggal_tutup'			=> $tanggal_tutup,
										'tgl_insert'			=> date("Y-m-d"),
										'status_lowongan'		=> 'Active',
										'metode_rekut'           => $metode_rekut,
										'cover_metode_rekut'     => $file_name_requirement,
										'deskripsi_metode_rekut' => $deskripsi_requirement,
										'kategori_daerah'	 	 => $kategori_daerah,
										'status_requment'        => $show_requirement
									);

				$m_lowongan->insert_data_lowongan('Tbl_Lowongan', $data_lowongan);
				$this->session->set_flashdata('sendmessage', '<strong>Lowongan berhasil ditambahkan oleh sistem kami </strong>');
				// redirect($this->agent->referrer());
				redirect('perusahaan/admin/data_lowongan','refresh');

			}

		}

	}

	public function action_update_data_perusahaan() {
		$this->load->model('Perusahaan_model');
		$m_perusahaan = new Perusahaan_model();
        
        $apikey                 = $this->session->userdata('api_key');
		$kode_perusahaan 		= htmlspecialchars($this->input->post('kode_perusahaan'));
		$nama_perusahaan 		= htmlspecialchars($this->input->post('nama_perusahaan'));
		$nama_pengguna   		= htmlspecialchars($this->input->post('nama_pengguna'));
		$nomor_telepon  		= htmlspecialchars($this->input->post('nomor_telepon'));
		$alamat_email           = htmlspecialchars($this->input->post('alamat_email'));
		$alamat_perusahan 		= htmlspecialchars($this->input->post('alamat_perusahan'));
		$nomor_telepon_kantor   = htmlspecialchars($this->input->post('nomor_telepon_kantor'));
		$alamat_website         = htmlspecialchars($this->input->post('alamat_website'));
		$industri               = htmlspecialchars($this->input->post('industri'));
		$img_perusahaan_old     = htmlspecialchars($this->input->post('img_perusahaan_old'));
	    $negara                 = htmlspecialchars($this->input->post('negara'));
		$deskripsi_perusahaan 	= $this->input->post('deskripsi_perusahaan');

		$valid_update = $this->db->query("SELECT * FROM Tbl_Perusahaan WHERE Kode_Perusahaan = '$kode_perusahaan' AND Api_Key = '$apikey'")->num_rows();
		if ($valid_update > 0) {
			
			if ($_FILES['img_perusahaan']['error'] > 0) {

				$data_update = array(
					'Nama_Perusahaan' 		=> $nama_perusahaan,
					'Nama_Pengguna'   		=> $nama_pengguna,
					'Nomor_Tlp_kantor'      => $nomor_telepon_kantor,
					'Industri'              => $industri,
					'Negara'				=> $negara,
					'Website'				=> $alamat_website,
					'Logo_perusahaan'		=> $img_perusahaan_old,
					'Alamat_perusahaan' 	=> $alamat_perusahan,
					'Deskripsi_perusahaan'  => $deskripsi_perusahaan
				);

				$m_perusahaan->update_data_perusahaan('Tbl_Perusahaan', $kode_perusahaan, $apikey, $data_update);
				$this->session->set_flashdata('sendmessage', '<strong>Profil perusahaan berhasil diupdate </strong>');
				redirect($this->agent->referrer());

			} else {

				if ($img_perusahaan_old !== "default_perusahaan.png") {
					$dir = "./assets_storage/img_perusahaan/".$img_perusahaan_old;
					if (file_exists($dir)) {
						unlink($dir);
					}
				} 

				$name_img_perusahaan 	 	= $_FILES['img_perusahaan']['name'];
				$file_name_foto_extension 	= pathinfo($name_img_perusahaan, PATHINFO_EXTENSION);
				$file_name_foto             = $kode_perusahaan.'-'.$name_img_perusahaan;
				$config['upload_path'] = './assets_storage/img_perusahaan/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']  = '*';
				$config['file_name'] = $file_name_foto;
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('img_perusahaan')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Ups!! Gagal upload logo perusahaan, mohon periksa kembali </strong>');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
					// echo "success";
				}

				$data_update = array(
					'Nama_Perusahaan' 		=> $nama_perusahaan,
					'Nama_Pengguna'   		=> $nama_pengguna,
					'Nomor_Tlp_kantor'      => $nomor_telepon_kantor,
					'Industri'              => $industri,
					'Negara'				=> $negara,
					'Website'				=> $alamat_website,
					'Logo_perusahaan'		=> $file_name_foto,
					'Alamat_perusahaan' 	=> $alamat_perusahan,
					'Deskripsi_perusahaan'  => $deskripsi_perusahaan
				);

				$m_perusahaan->update_data_perusahaan('Tbl_Perusahaan', $kode_perusahaan, $apikey, $data_update);
				$this->session->set_flashdata('sendmessage', '<strong>Profil perusahaan berhasil diupdate </strong>');
				redirect($this->agent->referrer());
			}

		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!! Gagal update data, mohon periksa kembali </strong>');
			redirect($this->agent->referrer());
		}
		
	}

	public function action_tambah_requiredment() {
		$this->load->model('Lib_model');
		$this->load->model('Lowongan_model');
		$m_lib = new Lib_model();
		$m_lowongan = new Lowongan_model();

        $kode_lowongan                          = $this->input->post('kode_lowongan');
        $deskripsi_requirement                  = $this->input->post('deskripsi_requirement');
        $show_requirement        				= 'show_requirement';
        $metode_rekut                           = $this->input->post('tipe_metode_rekut');

		$name_berkas_gamabaran_requirement      = $_FILES['gamabaran_requirement']['name'];
		$tmp_name_gamabaran_requirement      	= $_FILES['gamabaran_requirement']['tmp_name'];
		$error_gamabaran_requirement         	= $_FILES['gamabaran_requirement']['error'];
		$type_gamabaran_requirement   		 	= $_FILES['gamabaran_requirement']['type'];
		$file_name_requirement_extension 		= pathinfo($name_berkas_gamabaran_requirement, PATHINFO_EXTENSION);
		$slug_gambar_requirement                = $m_lib->create_slug('imgrekutmen-'.$kode_lowongan);
		$file_name_requirement                  = $slug_gambar_requirement.'.'.$file_name_requirement_extension;

		$dir_requirement 				= "./assets_storage/img_metode/";
		$extensi_file_requirement 		= array('png','jpg','jpeg','PNG','JPG');
		$new_file_requirement 			= $dir_requirement.$file_name_requirement;

		$valid = $this->db->query("SELECT * FROM Tbl_Lowongan WHERE kode_lowongan = '$kode_lowongan'")->num_rows();
		if ($valid > 0) {

			if ($_FILES['gamabaran_requirement']['error'] > 0) {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Gambar tidak tersedia mohon ulang kembali.');
				redirect($this->agent->referrer());
			} else {
				if(!in_array($file_name_requirement_extension, $extensi_file_requirement)) {
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Type file untuk gambaran requirement tak mendukung.');
					redirect($this->agent->referrer());
				} else {
					move_uploaded_file($tmp_name_gamabaran_requirement, $new_file_requirement);
				}
			}

			$data_requiredment = array(
				'cover_metode_rekut' => $file_name_requirement,
				'deskripsi_metode_rekut' =>  $deskripsi_requirement,
				'status_requment' => $show_requirement,
				'metode_rekut' => $metode_rekut
			);

			$m_lowongan->edit_data_lowongan('Tbl_Lowongan', $kode_lowongan, $data_requiredment);
			$this->session->set_flashdata('sendmessage', '<strong>Metode berhasil ditambahkan oleh sistem kami </strong>');
			redirect($this->agent->referrer());

		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Lowongan ini tidak tersedia.');
			redirect($this->agent->referrer());
		}
		
	}

	public function action_edit_requiredment() {
		$this->load->model('Lib_model');
		$this->load->model('Lowongan_model');
		$m_lib = new Lib_model();
		$m_lowongan = new Lowongan_model();

        $kode_lowongan                          = $this->input->post('kode_lowongan');
        $deskripsi_requirement                  = $this->input->post('deskripsi_requirement');
        $show_requirement        				= 'show_requirement';
        $img_rekut_olg                          = $this->input->post('img_rekut_olg');
        $metode_rekut                           = $this->input->post('tipe_metode_rekut');

		$name_berkas_gamabaran_requirement      = $_FILES['gamabaran_requirement']['name'];
		$tmp_name_gamabaran_requirement      	= $_FILES['gamabaran_requirement']['tmp_name'];
		$error_gamabaran_requirement         	= $_FILES['gamabaran_requirement']['error'];
		$type_gamabaran_requirement   		 	= $_FILES['gamabaran_requirement']['type'];
		$file_name_requirement_extension 		= pathinfo($name_berkas_gamabaran_requirement, PATHINFO_EXTENSION);
		$slug_gambar_requirement                = $m_lib->create_slug('imgrekutmen-'.$kode_lowongan);
		$file_name_requirement                  = $slug_gambar_requirement.'.'.$file_name_requirement_extension;

		$dir_requirement 				= "./assets_storage/img_metode/";
		$extensi_file_requirement 		= array('png','jpg','jpeg','PNG','JPG');
		$new_file_requirement 			= $dir_requirement.$file_name_requirement;

		$valid = $this->db->query("SELECT * FROM Tbl_Lowongan WHERE kode_lowongan = '$kode_lowongan'")->num_rows();
		if ($valid > 0) {

			if ($_FILES['gamabaran_requirement']['error'] > 0) {
				$file_name_requirement = $img_rekut_olg;
			} else {
				if(!in_array($file_name_requirement_extension, $extensi_file_requirement)) {
					$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Type file untuk gambaran requirement tak mendukung.');
					redirect($this->agent->referrer());
				} else {
                    $dir_file_old 				= "./assets_storage/img_metode/".$img_rekut_olg;
					if (file_exists($dir_file_old)) {
						unlink($dir_file_old);
					} 
					move_uploaded_file($tmp_name_gamabaran_requirement, $new_file_requirement);
				}
			}

			$data_requiredment = array(
				'metode_rekut'	=> $metode_rekut,
				'cover_metode_rekut' => $file_name_requirement,
				'deskripsi_metode_rekut' =>  $deskripsi_requirement,
				'status_requment' => $show_requirement
			);

			$m_lowongan->edit_data_lowongan('Tbl_Lowongan', $kode_lowongan, $data_requiredment);
			$this->session->set_flashdata('sendmessage', '<strong>Metode berhasil ditambahkan oleh sistem kami </strong>');
			redirect($this->agent->referrer());

		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Lowongan ini tidak tersedia.');
			redirect($this->agent->referrer());
		}
		
	}

	public function action_edit_lowongan() {

		$this->load->model('Lib_model');
		$this->load->model('Lowongan_model');
		$m_lib = new Lib_model();
		$m_lowongan = new Lowongan_model();

		$kode_lowongan    = htmlspecialchars($this->input->post('kode_lowongan'));
		$kode_perusahaan  = $this->session->userdata('kode_perusahaan');
		$nama_lowongan    = htmlspecialchars($this->input->post('nama_lowongan'));
		$pekerjaan_jenis  = htmlspecialchars($this->input->post('pekerjaan_jenis'));
		$posisi_spesialis = htmlspecialchars($this->input->post('posisi_spesialis'));
		$tanggal_tutup    = htmlspecialchars($this->input->post('tanggal_tutup'));
		$salary_max       = htmlspecialchars($this->input->post('salary_max'));
		$salary_min       = htmlspecialchars($this->input->post('salary_min'));
		$keahlian         = htmlspecialchars($this->input->post('keahlian'));
		$img_iklan_old    = htmlspecialchars($this->input->post('img_iklan_olg'));
		$kategori_daerah  = implode(', ',$this->input->post('kategori_lowongan'));

		if ($this->input->post('show_salary') !== '') {
			$show_salary = htmlspecialchars($this->input->post('show_salary'));
		} else {
			$show_salary = '';
		}

		$tinggkat_pendidikan = htmlspecialchars($this->input->post('tinggkat_pendidikan'));
		$bidang_studi        = htmlspecialchars($this->input->post('bidang_studi'));
		$pengalaman          = htmlspecialchars($this->input->post('pengalaman'));
		$deskripsi_pekerjaan = $this->input->post('deskripsi_pekerjaan');

		$nama_gambar_iklan = $_FILES['gambar_iklan']['name'];
		$tmp_gambar_iklan  = $_FILES['gambar_iklan']['tmp_name'];
		$type_gambar_iklan = $_FILES['gambar_iklan']['type'];
		$nama_extension_gambar_iklan = pathinfo($nama_gambar_iklan, PATHINFO_EXTENSION);
		$slug_gambar = $m_lib->create_slug($nama_lowongan.'-'.$kode_perusahaan.'-'.$kode_lowongan);
		$buat_nama_gamabar_baru = $slug_gambar.'.'.$nama_extension_gambar_iklan;
		

        $apikey = implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 30), 6));
		$valid = $this->db->query("SELECT * FROM Tbl_Lowongan WHERE kode_lowongan = '$kode_lowongan' AND kode_perusahaan = '$kode_perusahaan'")->num_rows();
		if ($valid > 0) {

			if ($_FILES['gambar_iklan']['error'] > 0) {
				echo "gambar tidak ada";
				$buat_nama_gamabar_baru = $img_iklan_old;
				$data_lowongan = array(
										'slug_url'			=> $m_lib->create_slug($nama_lowongan.'-'.$apikey.'-'.$kode_lowongan),
										'nama_lowongan'	 	=> $nama_lowongan,
										'pekerjaan_jenis' 	=> $pekerjaan_jenis,
										'posisi_spesialis' 	=> $posisi_spesialis,
										'salary_max' 		=> $salary_max,
										'salary_min' 		=> $salary_min,
										'show_salary' 		=> $show_salary,
										'keahlian'			=> $keahlian,
										'tingkat_pendidikan' 	=> $tinggkat_pendidikan,
										'bidang_studi'			=> $bidang_studi,
										'pengalaman' 			=> $pengalaman,
										'deskripsi_pekerjaan'	=> $deskripsi_pekerjaan,
										'cover_iklan'			=> $buat_nama_gamabar_baru,
										'tanggal_tutup'			=> $tanggal_tutup,
										'kategori_daerah'       => $kategori_daerah 
									);

				$m_lowongan->edit_data_lowongan_perusahaan('Tbl_Lowongan', $kode_lowongan, $kode_perusahaan, $data_lowongan);
				$this->session->set_flashdata('sendmessage', '<strong>Lowongan berhasil diperbaharui oleh sistem kami </strong>');
				redirect($this->agent->referrer());

			} else {
				$dir = "./assets_storage/img_iklan/".$img_iklan_old;
				if (file_exists($dir)) {
					unlink($dir);
				} 

				$config['upload_path'] = './assets_storage/img_iklan/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG';
				$config['max_size']  = '1024';
				$config['file_name'] = $buat_nama_gamabar_baru;

				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('gambar_iklan')){
					$error = array('error' => $this->upload->display_errors());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}

				$data_lowongan = array(
										'slug_url'			=> $m_lib->create_slug($nama_lowongan.'-'.$apikey.'-'.$kode_lowongan),
										'nama_lowongan'	 	=> $nama_lowongan,
										'pekerjaan_jenis' 	=> $pekerjaan_jenis,
										'posisi_spesialis' 	=> $posisi_spesialis,
										'salary_max' 		=> $salary_max,
										'salary_min' 		=> $salary_min,
										'show_salary' 		=> $show_salary,
										'keahlian'			=> $keahlian,
										'tingkat_pendidikan' 	=> $tinggkat_pendidikan,
										'bidang_studi'			=> $bidang_studi,
										'pengalaman' 			=> $pengalaman,
										'deskripsi_pekerjaan'	=> $deskripsi_pekerjaan,
										'cover_iklan'			=> $buat_nama_gamabar_baru,
										'tanggal_tutup'			=> $tanggal_tutup,
										'kategori_daerah'       => $kategori_daerah 
									);

				$m_lowongan->edit_data_lowongan_perusahaan('Tbl_Lowongan', $kode_lowongan, $kode_perusahaan, $data_lowongan);
				$this->session->set_flashdata('sendmessage', '<strong>Lowongan berhasil diperbaharui oleh sistem kami </strong>');
				redirect($this->agent->referrer());

			}

		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Data lowongan tidak dapat di perbaharui sistem kami.');
			redirect($this->agent->referrer());
		}

	}

	public function action_delete_lowongan($kode_lowongan) {
		$this->load->model('Lowongan_model');
		$this->load->model('Querydetail_model');

		$m_lowongan = new Lowongan_model();
		$m_query_detail = new Querydetail_model();

		$kode_perusahaan = $this->session->userdata('kode_perusahaan');
		$sql_lowongan = "SELECT * FROM Tbl_Lowongan WHERE kode_lowongan = '$kode_lowongan' AND kode_perusahaan = '$kode_perusahaan'";
		$valid = $this->db->query($sql_lowongan)->num_rows();
		
		if ($valid > 0) {
			$data_lowongan = $this->db->query($sql_lowongan)->row();

            $dir_iklan 				= "./assets_storage/img_iklan/".$data_lowongan->cover_iklan;
            $dir_requirement 		= "./assets_storage/img_metode/".$data_lowongan->cover_metode_rekut;

			if (file_exists($dir_iklan)) {
				unlink($dir_iklan);
			} 
			if (file_exists($dir_requirement)) {
				unlink($dir_requirement);
			} 

			$m_lowongan->delete_data_lowongan("Tbl_Lowongan", $kode_lowongan);
			$m_query_detail->delete_data_apply_lowongan("Tbl_Apply", $kode_lowongan);
			$this->session->set_flashdata('sendmessage', '<strong>Lowongan berhasil dihapus oleh sistem kami </strong>');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Gagal menghapus lowongan.');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_metode_requiredment($kode_lowongan) {
		$this->load->model('Lowongan_model');
		$m_lowongan = new Lowongan_model();

		$kode_perusahaan = $this->session->userdata('kode_perusahaan');
		$sql_rekut = "SELECT * FROM Tbl_Lowongan WHERE kode_lowongan = '$kode_lowongan' AND kode_perusahaan = '$kode_perusahaan'";
		$valid = $this->db->query($sql_rekut)->num_rows();
		if($valid) {

			$get_img_rekut = $this->db->query($sql_rekut)->row();
			$dir = "./assets_storage/img_metode/".$get_img_rekut->cover_metode_rekut;

			if (file_exists($dir)) {
				unlink($dir);
			} 

            $data_requiredment = array(
				'cover_metode_rekut' => NULL,
				'metode_rekut' => 3,
				'deskripsi_metode_rekut' =>  NULL,
				'status_requment' => NULL
			);
			$m_lowongan->edit_data_lowongan('Tbl_Lowongan', $kode_lowongan, $data_requiredment);
			$this->session->set_flashdata('sendmessage', '<strong>Metode berhasil dihapus oleh sistem kami </strong>');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong>Lowongan ini tidak tersedia.');
			redirect($this->agent->referrer());
		}
	}

	public function convert_pdf_cv($kode_pelamar, $kode_lowongan) {
		$this->load->model('Pelamar_model');
		$this->load->model('Querydetail_model');

		$m_pelamar = new Pelamar_model();
		$m_query_detail = new Querydetail_model();

		$data['data_apply'] = $m_query_detail->view_data_pelamar_detail_lowongan_cv($kode_pelamar, $kode_lowongan)->result();
		$data['nama_lowongan'] = $this->db->query("SELECT nama_lowongan FROM Tbl_Lowongan WHERE kode_lowongan = '$kode_lowongan'")->row();
		$this->load->view('perusahaan/page/convert_pdf_file_view', $data);
		
	}


}

/* End of file Perusahaan.php */
/* Location: ./application/controllers/Perusahaan.php */