<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Superroot extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{

		$this->load->model('Perusahaan_model');
		$this->load->model('Pelamar_model');
		$this->load->model('Querydetail_model');
		$this->load->model('Lowongan_model');

		$m_perusahaan = new Perusahaan_model();
		$m_pelamar    = new Pelamar_model();
		$m_apply      = new Querydetail_model();
		$m_lowongan   = new Lowongan_model();


		$data['data_perusahan']  = $m_perusahaan->view_data_perusahaan()->result();
		$data['total_perusahan'] = $m_perusahaan->view_data_perusahaan()->num_rows();

		$data['data_pelamar']  = $m_pelamar->view_data_pelamar()->result();
		$data['total_pelamar'] = $m_pelamar->view_data_pelamar()->num_rows();

		$data['total_apply'] = $m_apply->view_appaly()->num_rows();

		$data['data_lowongan'] = $m_lowongan->view_all_lowongan()->result();
		$data['total_lowongan'] = $m_lowongan->view_all_lowongan()->num_rows();

		$this->load->view('superroot/pages/home_view', $data);	
	}

	public function view_detail_lowongan($kode_perusahaan) {
		$this->load->model('Querydetail_model');
		$m_query_detail = new Querydetail_model();

		$data['data_detail_lowongan'] = $m_query_detail->view_lowongan_by_perusahaan($kode_perusahaan)->result();

		$this->load->view('superroot/pages/detail_lowongan_company_view', $data);
	}

	public function view_data_pelamar() {
		$this->load->model('Pelamar_model');
		$m_pelamar = new Pelamar_model();

		$data['data_pelamar'] = $m_pelamar->view_data_pelamar()->result();
		$this->load->view('superroot/pages/data_pelamar_company_view', $data);
	}



}

/* End of file Superroot.php */
/* Location: ./application/controllers/Superroot.php */