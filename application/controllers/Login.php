<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	// public function index()
	// {
		
	// }
	// 
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function login_perusahaan() {
		$this->load->view('login_reg/perusahaan/login_view');
	}

	public function register_perusahaan() {
		$this->load->view('login_reg/perusahaan/register_view');
	}

	public function reset_password_perusahaan() {
		$this->load->view('login_reg/perusahaan/reset_password_view');
	}


	public function action_login() {
		$this->load->model('Perusahaan_model');
		$m_perusahaan = new Perusahaan_model();

		$alamat_email = htmlspecialchars($this->input->post('alamat_email'));
		$password     = htmlspecialchars($this->input->post('password'));

		if (!empty($alamat_email) && !empty($password)) {
			$where_login = array(
				'Alamat_Email' => $alamat_email,
				'Password'    => md5($password)
			);

			$valid = $m_perusahaan->cek_login_perusahaan('Tbl_Perusahaan', $where_login)->num_rows();
			if($valid > 0) {
				$data_perusahaan = $m_perusahaan->cek_login_perusahaan('Tbl_Perusahaan', $where_login)->row();
				$data_session = array(
					'kode_perusahaan'  => $data_perusahaan->Kode_Perusahaan,
					'api_key'		   => $data_perusahaan->Api_Key,
					'nama_pengguna'	   => $data_perusahaan->Nama_Pengguna,
					'nama_perusahaan'  => $data_perusahaan->Nama_Perusahaan,
					'logo_perusahaan'  => $data_perusahaan->Logo_perusahaan,
					'status_perusahaan' => 'login_perusahaan'
				);
				$this->session->set_userdata($data_session);
				redirect('perusahaan/admin/data_lowongan','refresh');
			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Periksa kembali login.');
				redirect($this->agent->referrer());
			}
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Periksa kembali login.');
			redirect($this->agent->referrer());
		}
	}

	public function action_register() {
		$this->load->model('Lib_model');
		$this->load->model('Perusahaan_model');

		$m_lib = new Lib_model();
		$m_perusahaan = new Perusahaan_model();

		$nama_pengguna   = htmlspecialchars($this->input->post('nama_pengguna'));
		$nama_perusahaan = htmlspecialchars($this->input->post('nama_perusahaan'));
		$nomor_telepon   = htmlspecialchars($this->input->post('nomor_telepon'));
		$alamat_email    = htmlspecialchars($this->input->post('alamat_email'));
		$password        = htmlspecialchars($this->input->post('password'));
		$apikey = implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 30), 6));

		if (empty($nama_pengguna) && empty($nama_perusahaan) && empty($nomor_telepon) && empty($password)) {

			$this->session->set_flashdata('error_data', '<strong>Data tidak boleh kosong </strong>');
			redirect($this->agent->referrer());
		} else {

			$valid_data_perusahaan = $this->db->query("SELECT * FROM Tbl_Perusahaan")->num_rows();
			if ($valid_data_perusahaan > 0) {

				$get_id_perusahaan =  $this->db->order_by('Kode_Perusahaan', 'desc')->get('Tbl_Perusahaan')->result_array();
				$id_key_perusahaan = $get_id_perusahaan[0]['Kode_Perusahaan'];
				$kode_perusahaan = $m_lib->buat_kode(substr($id_key_perusahaan, 3), 'PER', 9) ;
				$data_input_data_perusahaan = array(
					'Kode_Perusahaan' => $kode_perusahaan,
					'Api_Key'         => $apikey,
					'Nama_Pengguna'	  => $nama_pengguna,
					'Nama_Perusahaan' => $nama_perusahaan,
					'Nomor_Telepon'	  => $nomor_telepon,
					'Alamat_Email'	  => $alamat_email,
					'Password'		  => md5($password),
					'Logo_perusahaan' => 'default_perusahaan.png',
					'Tgl_Daftar'	  => date("Y-m-d H:i:s")
				);

				
				// print_r($data_input_data_perusahaan);
				$m_perusahaan->insert_data_perusahaan('Tbl_Perusahaan', $data_input_data_perusahaan);
				$this->session->set_flashdata('sendmessage', '<strong>Data berhasil ditambahakan harap login</strong>');
				redirect('perusahaan/login','refresh');

			} else {

				$kode_perusahaan = $m_lib->buat_kode('', 'PER', 9);
				$data_input_data_perusahaan = array(
					'Kode_Perusahaan' => $kode_perusahaan,
					'Api_Key'         => $apikey,
					'Nama_Pengguna'	  => $nama_pengguna,
					'Nama_Perusahaan' => $nama_perusahaan,
					'Nomor_Telepon'	  => $nomor_telepon,
					'Alamat_Email'	  => $alamat_email,
					'Password'		  => md5($password),
					'Tgl_Daftar'	  => date("Y-m-d H:i:s")
				);

				// print_r($data_input_data_perusahaan);
				$m_perusahaan->insert_data_perusahaan('Tbl_Perusahaan', $data_input_data_perusahaan);
				$this->session->set_flashdata('sendmessage', '<strong>Data berhasil ditambahakan harap login</strong>');
				redirect('perusahaan/login','refresh');
			}

		}
	}

	public function action_reset_password() {

		$alamat_email 		= $this->input->post('email');
		$nomor_telepon 		= $this->input->post('nomor_telepon');
		$nama_perusahaan 	= $this->input->post('nama_perusahaan');

		$sql = "SELECT * FROM Tbl_Perusahaan WHERE Alamat_Email = '$alamat_email' AND Nomor_Telepon = '$nomor_telepon' AND Nama_Perusahaan = '$nama_perusahaan'";
		$valid_check_data = $this->db->query($sql)->num_rows();

		if ($valid_check_data > 0) {
			$data_row = $this->db->query($sql)->row();
			redirect('perusahaan/reset/'.$data_row->Api_Key,'refresh');
		} else {
			$this->session->set_flashdata('error_data', '<strong>Data ini tidak tersedia pada sistem kami</strong>');
			redirect($this->agent->referrer());
		}
	}

	public function valid_reset_password($apikey) {
		$data['apikey'] = $apikey;
		$this->load->view('login_reg/perusahaan/input_data_password', $data);
	}

	public function action_update_password() {
		$this->load->model('Perusahaan_model');
		$m_perusahaan = new Perusahaan_model();

		$apikey = $this->input->post('apikey');
		if (!empty($this->input->post('apikey'))) {
			if ($this->input->post('password') === $this->input->post('confirm_password')) {
				$valid = $this->db->query("SELECT * FROM Tbl_Perusahaan WHERE Api_Key = '$apikey'")->num_rows();
				if($valid > 0) {
					$data = array(
						'Password' => md5($this->input->post('password'))
					);
					$m_perusahaan->update_data_password('Tbl_Perusahaan', $apikey, $data);
					$this->session->set_flashdata('sendmessage', '<strong>Password berhasil di perbaharui mohon login kembali</strong>');
					redirect('perusahaan/login','refresh');
				} else {
					$this->session->set_flashdata('error_data', '<strong>Data valid tidak tersedia pada sistem kami</strong>');
					redirect($this->agent->referrer());
				}

			} else {
				$this->session->set_flashdata('error_data', '<strong>Password tidak sama mohon ulang kembali</strong>');
				redirect($this->agent->referrer());
			}
		} else {
			$this->session->set_flashdata('error_data', '<strong>Data ini tidak tersedia pada sistem kami</strong>');
			redirect($this->agent->referrer());
		}

	}

	public function logout_perusahaan() {
		$data_session = array(
					'kode_perusahaan'  => '',
					'api_key'		   => '',
					'nama_pengguna'	   => '',
					'nama_perusahaan'  => '',
					'status_perusahaan' => ''
				);
		$this->session->unset_userdata(array('kode_pelanggan'=> '', 'nomor_telepon' => '', 'status' => ''));
		session_destroy();
		redirect('perusahaan','refresh');
	}


/* system login pelamar */

	public function login_pelamar() {
		$this->load->view('login_reg/pelamar/login_view');
	}

	public function register_pelamar() {
		$this->load->view('login_reg/pelamar/register_view');
	}

	public function reset_password_pelamar() {
		$this->load->view('login_reg/pelamar/reset_password_view');
	}

	public function logout_pelamar() {
		$data_session = array(
					'kode_pelamar' => '',
					'alamat_email' => '',
					'nomor_telepon' => '',
					'nama_pelamar' => '',
					'status_pelamar' => ''
				);
		$this->session->unset_userdata($data_session);
		session_destroy();
		redirect('/','refresh');
	}

	public function logout_pelamar_front() {
		$data_session = array(
					'kode_pelamar' => '',
					'alamat_email' => '',
					'nomor_telepon' => '',
					'nama_pelamar' => '',
					'status_pelamar' => ''
				);
		$this->session->unset_userdata($data_session);
		session_destroy();
		redirect('/','refresh');
	}

	public function action_register_pelamar() {
		$this->load->model('Pelamar_model');
		$this->load->model('Lib_model');
		$m_pelamar  = new Pelamar_model();
		$m_lib 		= new Lib_model();

		$nama_pengguna = htmlspecialchars($this->input->post('nama_pengguna'));
		$alamat_email  = htmlspecialchars($this->input->post('email'));
		$nomor_telepon = htmlspecialchars($this->input->post('nomor_telepon'));
		$password      = htmlspecialchars($this->input->post('password'));
        $mounth = date('ym');

		$valid_data = $this->db->query("SELECT * FROM Tbl_Pelamar WHERE alamat_email = '$alamat_email'")->num_rows();

		if ($valid_data > 0) {
            $this->session->set_flashdata('error_data', '<strong>Data '.$alamat_email.' sudah tersedia silahkan login</strong>');
			redirect($this->agent->referrer());
		} else {

			$cek_data = $this->db->query("SELECT * FROM Tbl_Pelamar")->num_rows();
			if ($cek_data > 0) {
				$get_id =  $this->db->order_by('kode_pelamar', 'desc')->get('Tbl_Pelamar')->result_array();
				$id_key_pelamar = $get_id[0]['kode_pelamar'];
				$kode = substr($id_key_pelamar, 9);
				$buat_kode = $m_lib->buat_kode($kode, 'PLMR'.$mounth.'-', 9);

				$data_pelamar = array(
					'kode_pelamar'  => $buat_kode,
					'nama_pelamar'  => $nama_pengguna,
					'alamat_email'  => $alamat_email,
					'nomor_telepon' => $nomor_telepon,
					'password'  	=> md5($password),
					'password_text' => $password,
					'img_profile'   => 'default_pelamar.png',
					'tanggal_daftar' => date("Y-m-d")
				);
				$m_pelamar->insert_data_pelamar('Tbl_Pelamar',$data_pelamar);
				$this->session->set_flashdata('sendmessage', '<strong>Data berhasil ditambahakan harap login</strong>');
				redirect('pelamar/login','refresh');

			} else {

				$buat_kode = $m_lib->buat_kode('', 'PLMR'.$mounth.'-', 9);
				$data_pelamar = array(
					'kode_pelamar'  => $buat_kode,
					'nama_pelamar'  => $nama_pengguna,
					'alamat_email'  => $alamat_email,
					'nomor_telepon' => $nomor_telepon,
					'password'  	=> md5($password),
					'password_text' => $password,
					'img_profile'   => 'default_pelamar.png',
					'tanggal_daftar' => date("Y-m-d")
				);
				$m_pelamar->insert_data_pelamar('Tbl_Pelamar',$data_pelamar);
				$this->session->set_flashdata('sendmessage', '<strong>Data berhasil ditambahakan harap login</strong>');
				redirect('pelamar/login','refresh');
			}
		}
	}

	public function action_login_pelamar() {
		$this->load->model('Pelamar_model');
		$m_pelamar = new Pelamar_model();

		$alamat_email = htmlspecialchars($this->input->post('email'));
		$password     = htmlspecialchars($this->input->post('password'));

		if (!empty($alamat_email) && !empty($password)) {
			$where = array('alamat_email' => $alamat_email, 'password' => md5($password));
			$check_valid = $m_pelamar->cek_login_pelamar('Tbl_Pelamar', $where)->num_rows();

			if ($check_valid > 0) {
				$data_pelamar = $m_pelamar->cek_login_pelamar('Tbl_Pelamar', $where)->row();
				$data_session = array(
					'kode_pelamar' => $data_pelamar->kode_pelamar,
					'alamat_email' => $data_pelamar->alamat_email,
					'nomor_telepon' => $data_pelamar->nomor_telepon,
					'nama_pelamar' => $data_pelamar->nama_pelamar,
					'img_profile' => $data_pelamar->img_profile,
					'status_pelamar' => 'login_pelamar'
				);
				$this->session->set_userdata($data_session);
				redirect('pelamar/admin','refresh');

			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Periksa kembali login.');
				redirect($this->agent->referrer());
			}
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Periksa kembali login.');
			redirect($this->agent->referrer());
		}
	}

	public function action_login_pelamar_front() {
		$this->load->model('Pelamar_model');
		$m_pelamar = new Pelamar_model();

		$alamat_email = htmlspecialchars($this->input->post('email'));
		$password     = htmlspecialchars($this->input->post('password'));

		if (!empty($alamat_email) && !empty($password)) {
			$where = array('alamat_email' => $alamat_email, 'password' => md5($password));
			$check_valid = $m_pelamar->cek_login_pelamar('Tbl_Pelamar', $where)->num_rows();

			if ($check_valid > 0) {
				$data_pelamar = $m_pelamar->cek_login_pelamar('Tbl_Pelamar', $where)->row();
				$data_session = array(
					'kode_pelamar' => $data_pelamar->kode_pelamar,
					'alamat_email' => $data_pelamar->alamat_email,
					'nomor_telepon' => $data_pelamar->nomor_telepon,
					'nama_pelamar' => $data_pelamar->nama_pelamar,
					'img_profile' => $data_pelamar->img_profile,
					'status_pelamar' => 'login_pelamar'
				);
				$this->session->set_userdata($data_session);
				redirect($this->agent->referrer());

			} else {
				$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Periksa kembali login.');
				redirect($this->agent->referrer());
			}
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Periksa kembali login.');
			redirect($this->agent->referrer());
		}
	}

	public function action_reset_password_pelamar() {

		$alamat_email 		= $this->input->post('email');
		$nomor_telepon 		= $this->input->post('nomor_telepon');

		$sql = "SELECT * FROM Tbl_Pelamar WHERE alamat_email = '$alamat_email' AND nomor_telepon = '$nomor_telepon'";
		$valid_check_data = $this->db->query($sql)->num_rows();

		if ($valid_check_data > 0) {
			$data_row = $this->db->query($sql)->row();

			redirect('pelamar/reset/'.$data_row->nomor_telepon.'/'.$data_row->kode_pelamar,'refresh');
		} else {
			$this->session->set_flashdata('error_data', '<strong>Data ini tidak tersedia pada sistem kami</strong>');
			redirect($this->agent->referrer());
		}
	}

	public function valid_reset_password_pelamar($nomor_telepon, $kode_pelamar) {
		$data['nomor_telepon'] = $nomor_telepon;
		$data['kode_pelamar'] = $kode_pelamar;
		$this->load->view('login_reg/pelamar/input_data_password', $data);
	}

	public function action_update_password_pelamar() {
		$this->load->model('Pelamar_model');
		$m_pelamar = new Pelamar_model();

		$kode_pelamar = $this->input->post('kode_pelamar');
		$nomor_telepon = $this->input->post('nomor_telepon');

		$sql = "SELECT * FROM Tbl_Pelamar WHERE kode_pelamar = '$kode_pelamar' AND nomor_telepon = '$nomor_telepon'";

		$valid_gate_1 = $this->db->query($sql)->num_rows();

		if (!empty($this->input->post('kode_pelamar'))) {
			
			if ($valid_gate_1 > 0) {
			
				if ($this->input->post('password') === $this->input->post('confirm_password')) {
					$data = array(
						'Password' => md5($this->input->post('password')),
						'password_text' => $this->input->post('password')
					);
					$m_pelamar->update_data_password('Tbl_Pelamar', $kode_pelamar, $data);
					$this->session->set_flashdata('sendmessage', '<strong>Password berhasil di perbaharui mohon login kembali</strong>');
					redirect('pelamar/login','refresh');
				} else {
					$this->session->set_flashdata('error_data', '<strong>Password tidak sama mohon ulang kembali</strong>');
					redirect($this->agent->referrer());
				}

			} else {
				$this->session->set_flashdata('error_data', '<strong>Data ini tidak tersedia pada sistem kami</strong>');
				redirect($this->agent->referrer());
			}

		} else {
			$this->session->set_flashdata('error_data', '<strong>Data ini tidak tersedia pada sistem kami</strong>');
			redirect($this->agent->referrer());
		}

	}
		


}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */