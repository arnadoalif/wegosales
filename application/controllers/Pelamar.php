<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelamar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('status_pelamar') != "login_pelamar"){
			redirect('pelamar/login','refresh');
		}
	}

	public function index()
	{
		$this->load->view('pelamar/page/home_view');
	}

	public function view_profile() {
		$this->load->model('Pelamar_model');
		$this->load->model('Infolain_model');
		$this->load->model('Keterampilan_model');
		$this->load->model('Pendidikan_model');
		$this->load->model('pengalaman_model');
		$this->load->model('Querycv_model');

		$m_pelamar     = new Pelamar_model();
		$m_infolainnya = new Infolain_model();
		$m_keterampilan = new Keterampilan_model();
		$m_pendidikan = new Pendidikan_model();
		$m_pengalaman = new Pengalaman_model();
		$m_querycv    = new Querycv_model();

		if (empty($this->session->userdata('kode_pelamar'))) {
			redirect('pelamar/login');
		} else {
			$data['data_cv_profile'] = $m_querycv->view_all_profile_cv($this->session->userdata('kode_pelamar'))->row();
			$data['data_profil'] = $m_pelamar->view_data_pelamar_by_id_pelamar('Tbl_Pelamar', $this->session->userdata('kode_pelamar'))->result();
			$data['data_info']   = $m_infolainnya->view_all_info_lainnya_by_kode_pelamar('Tbl_Info_Tambahan', $this->session->userdata('kode_pelamar'))->result();
			$data['data_keterampilan'] =  $m_keterampilan->view_all_keterampilan('Tbl_Keterampilan', $this->session->userdata('kode_pelamar'))->result();
			$data['data_pendidikan'] = $m_pendidikan->view_data_pendidikan_by_pelamar('Tbl_Riwayat_Pendidikan', $this->session->userdata('kode_pelamar'))->result();
			$data['data_pengalaman'] = $m_pengalaman->view_all_data_pengalaman_by_pelamar('Tbl_Pengalaman_Kerja', $this->session->userdata('kode_pelamar'))->result();

            $this->load->view('pelamar/page/pelamar_profile', $data);
		}
	}

	public function action_ubah_avatar() {
		$this->load->model('Pelamar_model');
		$m_pelamar = new Pelamar_model();

		if ($_FILES['img_avatar']['error'] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Gambar tidak boleh kosong </strong>');
			redirect($this->agent->referrer());
		} else {
			if ($this->session->userdata('img_profile') == 'default_pelamar.png') {

				$kode_pelamar = $this->session->userdata('kode_pelamar');
				$nama_pelamar = $this->session->userdata('nama_pelamar');

				$name_img_avatar 	 	    = $_FILES['img_avatar']['name'];
				$tmp_name_img_avatar 		= $_FILES['img_avatar']['tmp_name'];
				$error_img_avatar    		= $_FILES['img_avatar']['error'];
				$type_img_avatar    		= $_FILES['img_avatar']['type'];
				$file_name_foto_extension 	= pathinfo($name_img_avatar, PATHINFO_EXTENSION);
				$file_name_foto             = str_replace('-','_', $kode_pelamar).'_'.str_replace(' ','_', $nama_pelamar).'.'.$file_name_foto_extension;
				
				$config['upload_path'] = './assets_storage/img_avatar/';
				$config['allowed_types'] = 'jpg|png|JPEG|JPG|PNG';
				$config['max_size']  = '*';
				$config['file_name'] = $file_name_foto;
				$this->load->library('upload', $config);
			
				if ( ! $this->upload->do_upload('img_avatar')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Gagal saat mengunggah photo</strong>');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
					$data_update_avatar = array(
						'img_profile' => $file_name_foto
					);

					$this->session->set_userdata('img_profile', $file_name_foto);
					$m_pelamar->update_data_profile('Tbl_Pelamar', $kode_pelamar, $data_update_avatar);
					$this->session->set_flashdata('sendmessage', '<strong>Foto berhasil di ubah </strong>');
					redirect($this->agent->referrer());
				}

			} else {

				if (file_exists('./assets_storage/img_avatar/'.$this->session->userdata('img_profile'))) {

					$location_file = './assets_storage/img_avatar/'.$this->session->userdata('img_profile');
					unlink($location_file);

                    $kode_pelamar = $this->session->userdata('kode_pelamar');
					$nama_pelamar = $this->session->userdata('nama_pelamar');

					$name_img_avatar 	 	    = $_FILES['img_avatar']['name'];
					$tmp_name_img_avatar 		= $_FILES['img_avatar']['tmp_name'];
					$error_img_avatar    		= $_FILES['img_avatar']['error'];
					$type_img_avatar    		= $_FILES['img_avatar']['type'];
					$file_name_foto_extension 	= pathinfo($name_img_avatar, PATHINFO_EXTENSION);
					$file_name_foto             = str_replace('-','_', $kode_pelamar).'_'.str_replace(' ','_', $nama_pelamar).'.'.$file_name_foto_extension;
					
					$config['upload_path'] = './assets_storage/img_avatar/';
					$config['allowed_types'] = 'jpg|png|JPEG|JPG|PNG';
					$config['max_size']  = '*';
					$config['file_name'] = $file_name_foto;
					$this->load->library('upload', $config);
				
					if ( ! $this->upload->do_upload('img_avatar')){
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('error_data', '<strong>Gagal saat mengunggah photo</strong>');
						redirect($this->agent->referrer());
					}
					else{
						$data = array('upload_data' => $this->upload->data());
						$data_update_avatar = array(
							'img_profile' => $file_name_foto
						);
						$this->session->set_userdata('img_profile', $file_name_foto);
						$m_pelamar->update_data_profile('Tbl_Pelamar', $kode_pelamar, $data_update_avatar);
						$this->session->set_flashdata('sendmessage', '<strong>Foto berhasil di ubah </strong>');
						redirect($this->agent->referrer());
					}

				} else {
					$kode_pelamar = $this->session->userdata('kode_pelamar');
					$nama_pelamar = $this->session->userdata('nama_pelamar');

					$name_img_avatar 	 	    = $_FILES['img_avatar']['name'];
					$tmp_name_img_avatar 		= $_FILES['img_avatar']['tmp_name'];
					$error_img_avatar    		= $_FILES['img_avatar']['error'];
					$type_img_avatar    		= $_FILES['img_avatar']['type'];
					$file_name_foto_extension 	= pathinfo($name_img_avatar, PATHINFO_EXTENSION);
					$file_name_foto             = str_replace('-','_', $kode_pelamar).'_'.str_replace(' ','_', $nama_pelamar).'.'.$file_name_foto_extension;
					
					$config['upload_path'] = './assets_storage/img_avatar/';
					$config['allowed_types'] = 'jpg|png|JPEG|JPG|PNG';
					$config['max_size']  = '*';
					$config['file_name'] = $file_name_foto;
					$this->load->library('upload', $config);
				
					if ( ! $this->upload->do_upload('img_avatar')){
						$error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('error_data', '<strong>Gagal saat mengunggah photo</strong>');
						redirect($this->agent->referrer());
					}
					else{
						$data = array('upload_data' => $this->upload->data());
						$data_update_avatar = array(
							'img_profile' => $file_name_foto
						);
						$this->session->set_userdata('img_profile', $file_name_foto);
						$m_pelamar->update_data_profile('Tbl_Pelamar', $kode_pelamar, $data_update_avatar);
						$this->session->set_flashdata('sendmessage', '<strong>Foto berhasil di ubah </strong>');
						redirect($this->agent->referrer());
					}
				}

			}
		}
	}

	public function action_upload_resume() {
		$this->load->model('Pelamar_model');
		$m_pelamar = new Pelamar_model();

		if ($_FILES['file_resume']['error'] > 0) {
			$this->session->set_flashdata('error_data', '<strong>Resume tidak boleh kosong </strong>');
			redirect($this->agent->referrer());
		} else {
			$kode_pelamar = $this->session->userdata('kode_pelamar');
			$nama_pelamar = $this->session->userdata('nama_pelamar');
			
			$nama_file_resume  = $_FILES['file_resume']['name'];
			$tmp_file_resume   = $_FILES['file_resume']['tmp_name'];
			$error_file_resume = $_FILES['file_resume']['error'];
			$type_file_resume  = $_FILES['file_resume']['type'];
			$file_extension_resume 	= pathinfo($nama_file_resume, PATHINFO_EXTENSION);
			$file_name_resume       = str_replace('-','_', $kode_pelamar).'_resume_'.str_replace(' ','_', $nama_pelamar).'.'.$file_extension_resume;
			$valid = $this->db->query("SELECT * FROM Tbl_Pelamar WHERE file_resume IS NOT NULL AND kode_pelamar = '$kode_pelamar'")->num_rows();

			if ($valid > 0) {
				$kode_pelamar = $this->session->userdata('kode_pelamar');
				$data_file = $this->db->query("SELECT file_resume FROM Tbl_Pelamar WHERE kode_pelamar = '$kode_pelamar'")->row();

				if (file_exists("./assets_storage/file_resume/".$data_file->file_resume)) {
					$location_file = "./assets_storage/file_resume/".$data_file->file_resume;
					unlink($location_file);
				}

				$config['upload_path'] = './assets_storage/file_resume/';
				$config['allowed_types'] = 'pdf';
				$config['max_size']  = '1024';
				$config['file_name'] = $file_name_resume;
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('file_resume')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Gagal mengunggah resume file max 1mb</strong>');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
					$data_resume = array(
						'file_resume' => $file_name_resume
					);

					$m_pelamar->update_data_profile('Tbl_Pelamar', $kode_pelamar, $data_resume);
					$this->session->set_flashdata('sendmessage', '<strong>Resume berhasil di tambahakan </strong>');
					redirect($this->agent->referrer());
				}

			} else {

				$config['upload_path'] = './assets_storage/file_resume/';
				$config['allowed_types'] = 'pdf';
				$config['max_size']  = '1024';
				$config['file_name'] = $file_name_resume;
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('file_resume')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Gagal mengunggah resume file max 1mb</strong>');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
					$data_resume = array(
						'file_resume' => $file_name_resume
					);

					$m_pelamar->update_data_profile('Tbl_Pelamar', $kode_pelamar, $data_resume);
					$this->session->set_flashdata('sendmessage', '<strong>Resume berhasil di tambahakan </strong>');
					redirect($this->agent->referrer());
				}

			}
			
		}
		
	}

	public function action_delete_resume($nama_resume) {
		$this->load->model('Pelamar_model');
		$m_pelamar = new Pelamar_model();

		$kode_pelamar = $this->session->userdata('kode_pelamar');
		$file_location = "./assets_storage/file_resume/".$nama_resume;
		if (file_exists($file_location)) {
			$valid = $this->db->query("SELECT * FROM Tbl_Pelamar WHERE file_resume = '$nama_resume' AND kode_pelamar = '$kode_pelamar'")->num_rows();
			if ($valid > 0) {
				unlink($file_location);
				$data_resume = array('file_resume' => '');
				$m_pelamar->update_data_profile('Tbl_Pelamar', $kode_pelamar, $data_resume);
				$this->session->set_flashdata('sendmessage', '<strong>Resume berhasil di hapus </strong>');
				redirect($this->agent->referrer());
			} else {
				redirect($this->agent->referrer());
			}
		} else {
			redirect($this->agent->referrer());
		}
	}

	public function action_update_profile() {
		$this->load->model('Pelamar_model');
		$m_pelamar = new Pelamar_model();

		$kode_pelamar = $this->session->userdata('kode_pelamar');

		$nama_pelamar 	= htmlspecialchars($this->input->post('nama_pelamar'));
		$tanggal_lahir 	= htmlspecialchars($this->input->post('tanggal'));
		$bulan_lahir 	= htmlspecialchars($this->input->post('bulan'));
		$tahun_lahir 	= htmlspecialchars($this->input->post('tahun'));
		$nomor_telepon  = htmlspecialchars($this->input->post('nomor_telepon'));
		$alamat1         = htmlspecialchars($this->input->post('alamat1'));
		$alamat2         = htmlspecialchars($this->input->post('alamat2'));
		$jenis_kelamin  = htmlspecialchars($this->input->post('jenis_kelamin'));
		$kota           = htmlspecialchars($this->input->post('kota'));
		$kode_pos       = htmlspecialchars($this->input->post('kode_pos'));
		$propinsi 		= htmlspecialchars($this->input->post('propinsi'));
		$identitas 		= htmlspecialchars($this->input->post('identitas'));
		$nomor_identitas = htmlspecialchars($this->input->post('nomor_identitas'));

		if (!empty($kode_pelamar)) {
			$data_profil = array(
				'nama_pelamar' 	=> $nama_pelamar,
				'tanggal' 		=> $tanggal_lahir,
				'bulan' 		=> $bulan_lahir,
				'tahun' 		=> $tahun_lahir,
				'nomor_telepon' => $nomor_telepon,
				'jenis_kelamin' => $jenis_kelamin,
				'alamat_1' 		=> $alamat1,
				'alamat_2' 		=> $alamat2,
				'kota' 			=> $kota,
				'kode_pos' 		=> $kode_pos,
				'provinsi'      => $propinsi,
				'identitas' 	=> $identitas,
				'nomor_identitas' => $nomor_identitas
			);
			$this->session->set_userdata('nama_pelamar', $nama_pelamar);
			$this->session->set_userdata('nomor_telepon', $nomor_telepon);
			$m_pelamar->update_data_profile('Tbl_Pelamar', $kode_pelamar , $data_profil);
			$this->session->set_flashdata('sendmessage', '<strong>Profil berhasil diubah </strong>');
			redirect($this->agent->referrer());
		} else{
			redirect('pelamar/login','refresh');
		}
	}

	public function action_update_info_lainnya() {
		$this->load->model('Infolain_model');
		$m_infolainnya = new Infolain_model();

		$kode_pelamar = $this->session->userdata('kode_pelamar');

		$mata_uang = htmlspecialchars($this->input->post('mata_uang'));
		$gaji      = htmlspecialchars($this->input->post('gaji'));
		$lokasi_kerja1 = htmlspecialchars($this->input->post('lokasi_kerja1'));
		$lokasi_kerja2 = htmlspecialchars($this->input->post('lokasi_kerja2'));
		$lokasi_kerja3 = htmlspecialchars($this->input->post('lokasi_kerja3'));
		$informasi_lainnya = htmlspecialchars($this->input->post('informasi_lainnya'));

	   $valid = $this->db->query("SELECT * FROM Tbl_Info_Tambahan WHERE kode_pelamar = '$kode_pelamar'")->num_rows();
	   if ($valid > 0) {
	   		$data_info_lainnya = array(
	   			'kode_pelamar' => $kode_pelamar,
	   			'mata_uang' => $mata_uang,
	   			'gaji' => $gaji,
	   			'lokasi_kerja_1' => $lokasi_kerja1,
	   			'lokasi_kerja_2' => $lokasi_kerja2,
	   			'lokasi_kerja_3' => $lokasi_kerja3,
	   			'informasi_lainnya' => $informasi_lainnya
	   		);
	   		$m_infolainnya->update_data_info_lainnya('Tbl_Info_Tambahan', $kode_pelamar, $data_info_lainnya);
	   		$this->session->set_flashdata('sendmessage', '<strong>Info berhasil ditambahkan </strong>');
			redirect($this->agent->referrer());
	   } else {
	   		$data_info_lainnya = array(
	   			'kode_pelamar' => $kode_pelamar,
	   			'mata_uang' => $mata_uang,
	   			'gaji' => $gaji,
	   			'lokasi_kerja_1' => $lokasi_kerja1,
	   			'lokasi_kerja_2' => $lokasi_kerja2,
	   			'lokasi_kerja_3' => $lokasi_kerja3,
	   			'informasi_lainnya' => $informasi_lainnya
	   		);
	   		$m_infolainnya->insert_data_info_lainnya('Tbl_Info_Tambahan', $data_info_lainnya);
	   		$this->session->set_flashdata('sendmessage', '<strong>Info berhasil ditambahkan </strong>');
			redirect($this->agent->referrer());
	   }
	}

	public function action_update_keterampilan_bahasa() {
		$this->load->model('Keterampilan_model');
		$m_keterampilan = new Keterampilan_model();

		$kode_pelamar = $this->session->userdata('kode_pelamar');

		$valid = $this->db->query("SELECT * FROM Tbl_Keterampilan WHERE kode_pelamar = '$kode_pelamar'")->num_rows();
		if ($valid > 0) {
			$data_bahasa = array('kuasai_bahasa' => $this->input->post('bahasa'));
			$m_keterampilan->update_data_keterampilan('Tbl_Keterampilan', $kode_pelamar, $data_bahasa);
			$this->session->set_flashdata('sendmessage', '<strong>Keterampilan bahasa berhasil ditambahkan </strong>');
			redirect($this->agent->referrer());
		} else {
			$data_bahasa = array('kode_pelamar' => $kode_pelamar, 'kuasai_bahasa' => $this->input->post('bahasa'));
			$m_keterampilan->insert_data_keterampilan('Tbl_Keterampilan', $data_bahasa);
			$this->session->set_flashdata('sendmessage', '<strong>Keterampilan bahasa berhasil ditambahkan </strong>');
			redirect($this->agent->referrer());
		}
	}

	public function action_update_keterampilan_skill() {
		$this->load->model('Keterampilan_model');
		$m_keterampilan = new Keterampilan_model();

		$kode_pelamar = $this->session->userdata('kode_pelamar');

		$valid = $this->db->query("SELECT * FROM Tbl_Keterampilan WHERE kode_pelamar = '$kode_pelamar'")->num_rows();
		if ($valid > 0) {
			$data_bahasa = array('keterampilan' => $this->input->post('keterampilan_skill'));
			$m_keterampilan->update_data_keterampilan('Tbl_Keterampilan', $kode_pelamar, $data_bahasa);
			$this->session->set_flashdata('sendmessage', '<strong>Keterampilan berhasil ditambahkan </strong>');
			redirect($this->agent->referrer());
		} else {
			$data_bahasa = array('kode_pelamar' => $kode_pelamar, 'keterampilan' => $this->input->post('keterampilan_skill'));
			$m_keterampilan->insert_data_keterampilan('Tbl_Keterampilan', $data_bahasa);
			$this->session->set_flashdata('sendmessage', '<strong>Keterampilan berhasil ditambahkan </strong>');
			redirect($this->agent->referrer());
		}
	}

	public function action_insert_data_pendidikan() {
		$this->load->model('Pendidikan_model');
		$m_pendidikan = new Pendidikan_model();

		$kode_pelamar   = $this->session->userdata('kode_pelamar');
		$nama_universitas = htmlspecialchars($this->input->post('nama_universitas'));
		$bulan_wisuda     = htmlspecialchars($this->input->post('bulan_wisuda'));
		$tahun_lulusan    = htmlspecialchars($this->input->post('tahun_lulusan'));
		$kualifikasi      = htmlspecialchars($this->input->post('kualifikasi'));
		$lokasi           = htmlspecialchars($this->input->post('lokasi'));
		$jurusan          = htmlspecialchars($this->input->post('jurusan'));
		$grade            = htmlspecialchars($this->input->post('grade'));
		if ($grade == 'IPK') {
			$nilai_asli  = htmlspecialchars($this->input->post('nilai_asli'));
			$nilai_dari  = htmlspecialchars($this->input->post('nilai_dari'));
		} else {
			$nilai_asli = '';
			$nilai_dari = '';
		}

		$informasi_tambahan = htmlspecialchars($this->input->post('informasi_tambahan'));
		$data_pendidikan = array(
				'kode_pelamar'	  => $kode_pelamar,
				'nama_universitas' => $nama_universitas,
				'bulan_wisuda'     => $bulan_wisuda,
				'tahun_lulusan'    => $tahun_lulusan,
				'kualifikasi'      => $kualifikasi,
				'lokasi'           => $lokasi,
				'jurusan'		   => $jurusan,
				'grade'			   => $grade,
				'nilai_asli'	  => $nilai_asli,
				'nilai_dari'	   => $nilai_dari,
				'informasi_tambahan' => $informasi_tambahan
			);

		$m_pendidikan->insert_data_pendidikan_by_pelamar('Tbl_Riwayat_Pendidikan', $data_pendidikan);
		$this->session->set_flashdata('sendmessage', '<strong>Pendidikan berhasil ditambahkan </strong>');
		redirect($this->agent->referrer());

	}

	public function action_update_data_pendidikan() {

		$this->load->model('Pendidikan_model');
		$m_pendidikan = new Pendidikan_model();

		$kode_pelamar   = $this->session->userdata('kode_pelamar');
		$kode_pendidikan  = htmlspecialchars($this->input->post('kode_pendidikan'));
		$nama_universitas = htmlspecialchars($this->input->post('nama_universitas'));
		$bulan_wisuda     = htmlspecialchars($this->input->post('bulan_wisuda'));
		$tahun_lulusan    = htmlspecialchars($this->input->post('tahun_lulusan'));
		$kualifikasi      = htmlspecialchars($this->input->post('kualifikasi'));
		$lokasi           = htmlspecialchars($this->input->post('lokasi'));
		$jurusan          = htmlspecialchars($this->input->post('jurusan'));
		$grade            = htmlspecialchars($this->input->post('grade'));
		if ($grade == 'IPK') {
			$nilai_asli  = htmlspecialchars($this->input->post('nilai_asli'));
			$nilai_dari  = htmlspecialchars($this->input->post('nilai_dari'));
		} else {
			$nilai_asli = '';
			$nilai_dari = '';
		}
		$informasi_tambahan = htmlspecialchars($this->input->post('informasi_tambahan'));

		$valid = $this->db->query("SELECT * FROM Tbl_Riwayat_Pendidikan WHERE kode_pelamar ='$kode_pelamar' AND kode_pendidikan = '$kode_pendidikan'")->num_rows();

		if ($valid > 0) {

			$data_pendidikan = array(
				'nama_universitas' => $nama_universitas,
				'bulan_wisuda'     => $bulan_wisuda,
				'tahun_lulusan'    => $tahun_lulusan,
				'kualifikasi'      => $kualifikasi,
				'lokasi'           => $lokasi,
				'jurusan'		   => $jurusan,
				'grade'			   => $grade,
				'nilai_asli'	  => $nilai_asli,
				'nilai_dari'	   => $nilai_dari,
				'informasi_tambahan' => $informasi_tambahan
			);

			$m_pendidikan->update_data_pendidikan_by_pelamar('Tbl_Riwayat_Pendidikan', $kode_pendidikan, $kode_pelamar, $data_pendidikan);
			$this->session->set_flashdata('sendmessage', '<strong>Pendidikan berhasil diubah </strong>');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Gagal saat mengubah data pendidikan</strong>');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_data_pendidikan($kode_key) {
		$this->load->model('Pendidikan_model');
		$m_pendidikan = new Pendidikan_model();

		$kode_pelamar = $this->session->userdata('kode_pelamar');
		$valid = $this->db->query("SELECT * FROM Tbl_Riwayat_Pendidikan WHERE kode_pelamar = '$kode_pelamar'")->num_rows();
		if ($valid > 0) {

			$valid_lev_2 = $this->db->query("SELECT * FROM Tbl_Riwayat_Pendidikan WHERE kode_pelamar = '$kode_pelamar' AND kode_pendidikan = '$kode_key'")->num_rows();
			if ($valid_lev_2 > 0) {
				$m_pendidikan->delete_data_pendidikan_by_pelamar('Tbl_Riwayat_Pendidikan', $kode_key, $kode_pelamar);
				$this->session->set_flashdata('sendmessage', '<strong>Pendidikan berhasil dihapus </strong>');
				redirect($this->agent->referrer());
			} else {
              $this->session->set_flashdata('error_data', '<strong>Gagal saat menghapus data pendidikan</strong>');
			  redirect($this->agent->referrer());
			}

		} else {
          	$this->session->set_flashdata('error_data', '<strong>Gagal saat menghapus data pendidikan</strong>');
			redirect($this->agent->referrer());
		}
	}

	public function action_insert_data_pengalaman() {
		$this->load->model('Pengalaman_model');
		$m_pengalaman = new Pengalaman_model();

		$kode_pelamar = $this->session->userdata('kode_pelamar');
		$posisi_kerja 		= htmlspecialchars($this->input->post('posisi_kerja'));
		$nama_perusahaan 	= htmlspecialchars($this->input->post('nama_perusahaan'));
		$bulan_start_kerja 	= htmlspecialchars($this->input->post('bulan_start_kerja'));
		$tahun_start_kerja 	= htmlspecialchars($this->input->post('tahun_start_kerja'));

		if (empty($this->input->post('masih_kerja'))) {
			$masih_kerja = '';
			$bulan_start_selesai = htmlspecialchars($this->input->post('bulan_start_selesai'));
			$tahun_start_selesai = htmlspecialchars($this->input->post('tahun_start_selesai'));
		} else {
			$masih_kerja = htmlspecialchars($this->input->post('masih_kerja'));
			$bulan_start_selesai = '';
			$tahun_start_selesai = '';
		}

		$spesialisasi 		= htmlspecialchars($this->input->post('spesialisasi'));
		$bidang_pekerjaan 	= htmlspecialchars($this->input->post('bidang_pekerjaan'));
		$negara 			= htmlspecialchars($this->input->post('negara'));
		$industri 			= htmlspecialchars($this->input->post('industri'));
		$jabatan 			= htmlspecialchars($this->input->post('jabatan'));
		$mata_uang          = htmlspecialchars($this->input->post('mata_uang'));
		$gaji 				= htmlspecialchars($this->input->post('gaji'));
		$keterangan_kerja 	= htmlspecialchars($this->input->post('keterangan_kerja'));

		$data_pengalaman = array(
			'kode_pelamar' 		=> $kode_pelamar,
			'posisi_kerja' 		=> $posisi_kerja,
			'nama_perusahaan' 	=> $nama_perusahaan,
			'bulan_start_kerja' => $bulan_start_kerja,
			'tahun_start_kerja' => $tahun_start_kerja,
			'masih_kerja' 		=> $masih_kerja,
			'bulan_start_selesai' => $bulan_start_selesai,
			'tahun_start_selesai' => $tahun_start_selesai,
			'spesialisasi' 		=> $spesialisasi,
			'bidang_pekerjaan' 	=> $bidang_pekerjaan,
			'negara' 			=> $negara,
			'industri' 			=> $industri,
			'jabatan' 			=> $jabatan,
			'mata_uang' 		=> $mata_uang,
			'gaji' 				=> $gaji,
			'keterangan_kerja'  => $keterangan_kerja
		);

		$m_pengalaman->insert_data_pengalaman_by_pelamar('Tbl_Pengalaman_Kerja', $data_pengalaman);
		$this->session->set_flashdata('sendmessage', '<strong>Pengalaman berhasil ditambahkan </strong>');
		redirect($this->agent->referrer());
		
	}

	public function action_update_pengalaman() {

		$this->load->model('Pengalaman_model');
		$m_pengalaman = new Pengalaman_model();

		$kode_pelamar = $this->session->userdata('kode_pelamar');
		$kode_pengalaman    = htmlspecialchars($this->input->post('kode_pengalaman'));
		$posisi_kerja 		= htmlspecialchars($this->input->post('posisi_kerja'));
		$nama_perusahaan 	= htmlspecialchars($this->input->post('nama_perusahaan'));
		$bulan_start_kerja 	= htmlspecialchars($this->input->post('bulan_start_kerja'));
		$tahun_start_kerja 	= htmlspecialchars($this->input->post('tahun_start_kerja'));

		if (empty($this->input->post('masih_kerja_update'))) {
			$masih_kerja = '';
			$bulan_start_selesai = htmlspecialchars($this->input->post('bulan_start_selesai'));
			$tahun_start_selesai = htmlspecialchars($this->input->post('tahun_start_selesai'));
		} else {
			$masih_kerja = htmlspecialchars($this->input->post('masih_kerja_update'));
			$bulan_start_selesai = '';
			$tahun_start_selesai = '';
		}

		$spesialisasi 		= htmlspecialchars($this->input->post('spesialisasi'));
		$bidang_pekerjaan 	= htmlspecialchars($this->input->post('bidang_pekerjaan'));
		$negara 			= htmlspecialchars($this->input->post('negara'));
		$industri 			= htmlspecialchars($this->input->post('industri'));
		$jabatan 			= htmlspecialchars($this->input->post('jabatan'));
		$mata_uang          = htmlspecialchars($this->input->post('mata_uang'));
		$gaji 				= htmlspecialchars($this->input->post('gaji'));
		$keterangan_kerja 	= htmlspecialchars($this->input->post('keterangan_kerja'));

		$valid = $this->db->query("SELECT * FROM Tbl_Pengalaman_Kerja WHERE kode_pengalaman = '$kode_pengalaman' AND kode_pelamar = '$kode_pelamar'")->num_rows();

		if ($valid > 0) {
			$data_pengalaman = array(
				'posisi_kerja' 		=> $posisi_kerja,
				'nama_perusahaan' 	=> $nama_perusahaan,
				'bulan_start_kerja' => $bulan_start_kerja,
				'tahun_start_kerja' => $tahun_start_kerja,
				'masih_kerja' 		=> $masih_kerja,
				'bulan_start_selesai' => $bulan_start_selesai,
				'tahun_start_selesai' => $tahun_start_selesai,
				'spesialisasi' 		=> $spesialisasi,
				'bidang_pekerjaan' 	=> $bidang_pekerjaan,
				'negara' 			=> $negara,
				'industri' 			=> $industri,
				'jabatan' 			=> $jabatan,
				'mata_uang' 		=> $mata_uang,
				'gaji' 				=> $gaji,
				'keterangan_kerja'  => $keterangan_kerja
			);
			$m_pengalaman->update_data_pengalaman_by_pelamar('Tbl_Pengalaman_Kerja', $kode_pengalaman, $kode_pelamar, $data_pengalaman);
			$this->session->set_flashdata('sendmessage', '<strong>Pengalaman berhasil diperbaharui </strong>');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Gagal saat perbaharui data Pengalaman</strong>');
			redirect($this->agent->referrer());
		}
		
	}

	public function action_delete_pengalaman($kode_key) {
		$this->load->model('Pengalaman_model');
		$m_pengalaman = new Pengalaman_model();

		$kode_pelamar = $this->session->userdata('kode_pelamar');
		$valid = $this->db->query("SELECT * FROM Tbl_Pengalaman_Kerja WHERE kode_pelamar = '$kode_pelamar'")->num_rows();
		if ($valid > 0) {

			$valid_lev_2 = $this->db->query("SELECT * FROM Tbl_Pengalaman_Kerja WHERE kode_pelamar = '$kode_pelamar' AND kode_pengalaman = '$kode_key'")->num_rows();
			if ($valid_lev_2 > 0) {
				$m_pengalaman->delete_data_pengalaman_by_pelamar('Tbl_Pengalaman_Kerja', $kode_key, $kode_pelamar);
				$this->session->set_flashdata('sendmessage', '<strong>Pengalaman berhasil dihapus </strong>');
				redirect($this->agent->referrer());
			} else {
              $this->session->set_flashdata('error_data', '<strong>Gagal saat menghapus data Pengalaman</strong>');
			  redirect($this->agent->referrer());
			}

		} else {
          	$this->session->set_flashdata('error_data', '<strong>Gagal saat menghapus data Pengalaman</strong>');
			redirect($this->agent->referrer());
		}
	}

	public function download_cv_pelamar($kode_pelamar) {
		$this->load->model('Pelamar_model');
		$m_pelamar = new Pelamar_model();

		$valid = $this->db->query("SELECT * FROM Tbl_Pelamar WHERE kode_pelamar = '$kode_pelamar'")->num_rows();

		if($valid > 0) {
			$data['data_pelamar'] =	$m_pelamar->view_data_pelamar_by_id_pelamar('Tbl_Pelamar', $kode_pelamar)->result();
			$this->load->view('pelamar/page/convert_pdf_file_view', $data);
		} else {
			redirect('pelamar/admin','refresh');
		}
		
	}



}

/* End of file Pelamar.php */
/* Location: ./application/controllers/Pelamar.php */