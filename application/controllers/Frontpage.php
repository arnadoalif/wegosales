<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontpage extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$this->load->view('frontpage/page/home_pelamar_view');
	}

	public function front_perusahaan() {
		$this->load->view('frontpage/page/home_perusahaan_view');
	}

	public function front_cari_lowongan($kode_provinsi) {
		$this->load->model('Lowongan_model');
		$this->load->model('Lib_model');
		$m_lowongan = new Lowongan_model();
		$m_lin      = new Lib_model();

		$nama_provinsi = $this->db->query("SELECT * FROM Tbl_Provinsi WHERE kode_provinsi = '$kode_provinsi'")->row();
		$data['data_lowongan'] = $m_lowongan->view_lowongan_by_kode_provinsi($nama_provinsi->nama_provinsi)->result();
		$data['nama_provinsi']	= $nama_provinsi->nama_provinsi;
		$data['kode_provinsi'] = $kode_provinsi;
		$this->load->view('frontpage/page/cari_lowongan_view', $data);
	}

	public function frontpage_lowongan_detail($kode_provinsi, $slug_lowongan) {
		$this->load->model('Querydetail_model');
		$m_query_detail = new Querydetail_model();
		$sql = "SELECT * FROM Tbl_Provinsi WHERE kode_provinsi = '$kode_provinsi'";
		$valid_kode_provinsi = $this->db->query($sql)->num_rows();
		if ($valid_kode_provinsi > 0) {
			$valid = $m_query_detail->view_data_perusahaan_lowongan($slug_lowongan)->num_rows();
			if ($valid > 0) {
				$nama_provinsi = $this->db->query($sql)->row();
				$data['data_lowongan'] = $m_query_detail->view_data_perusahaan_lowongan($slug_lowongan)->result();
				$data['nama_provinsi'] = $nama_provinsi->nama_provinsi;
				$this->load->view('frontpage/page/detail_lowongan_view', $data);
			} else {
				redirect('pilih-wilayah','refresh');
			}
		} else {
			redirect('pilih-wilayah','refresh');
		}
		
	}

	public function front_peluang_usaha() {
		$this->load->view('frontpage/page/home_peluang_usaha_view');
	}

	public function job() {
		$this->load->view('frontpage/page/cari_lowongan_rev_view');
	}

	public function frontpage_provinsi_view() {
		$this->load->model('Lib_model');
		$m_lib = new Lib_model();

		$data['data_provinsi'] = $m_lib->view_provinsi()->result();
		$this->load->view('frontpage/page/lowongan_provinsi_view', $data);
	}

	public function front_kontak() {
		$this->load->view('frontpage/page/kotak_view');
	}

	public function action_summit_lowongan() {
		$this->load->model('Lib_model');
		$this->load->model('Querydetail_model');

		$m_model = new Lib_model();
		$m_query_detail = new Querydetail_model();

		$kode_pelamar       = $this->session->userdata('kode_pelamar');
		$email_pelamar       = $this->session->userdata('alamat_email');
		$apikey 			= htmlspecialchars($this->input->post('apikey'));
		$kode_lowongan 		= htmlspecialchars($this->input->post('kode_lowongan'));
		$kode_perusahaan 	= htmlspecialchars($this->input->post('kode_perusahaan'));
		$deskripsi_promosi 	= htmlspecialchars($this->input->post('message'));
		$nama_provinsi      = htmlspecialchars($this->input->post('nama_provinsi'));


		$kode_pelamar = $this->session->userdata('kode_pelamar');
        $tambahan = "select * from Tbl_Info_Tambahan where kode_pelamar    = '$kode_pelamar'";
        $keterampilan = "select * from Tbl_Keterampilan where kode_pelamar  = '$kode_pelamar'";
        $pengalaman = "select * from Tbl_Pengalaman_Kerja where kode_pelamar = '$kode_pelamar'";
        $pendidiakan = "select * from Tbl_Riwayat_Pendidikan where kode_pelamar = '$kode_pelamar'";

        $h_tambahan     = $this->db->query($tambahan)->row();
        $h_keterampilan = $this->db->query($keterampilan)->row();
        $h_pengalaman   = $this->db->query($pengalaman)->row();
        $h_pendidikan   = $this->db->query($pendidiakan)->row();

		if (empty($h_tambahan) && empty($h_keterampilan) && empty($h_pengalaman) && empty($h_pendidiakan)) {
			$this->session->set_flashdata('error_data', "<strong>Lamaran tidak bisa di proses lengkapi data diri anda terlebih dahulu <a href=". base_url("pelamar/admin/profil")." title='Lengkapi Profil'>Lengkapi Profil</a> </strong>");
			redirect($this->agent->referrer());
		} else {
			$valid_apply = $this->db->query("SELECT * FROM Tbl_Apply WHERE kode_lowongan = '$kode_lowongan' AND kode_pelamar = '$kode_pelamar'")->num_rows();
			if ($valid_apply > 0) {
				$data_apply = array(
					'email_pelamar' => $email_pelamar,
					'deskripsi_promosi' => $deskripsi_promosi
				);
				$m_query_detail->update_apply_lowongan('Tbl_Apply', $kode_lowongan, $kode_pelamar, $data_apply);
				$this->session->set_flashdata('sendmessage', '<strong>Anda sebelumnya sudah terdaftar sistem akan mengupdate masukan data yang terakhir kali </strong>');
				redirect($this->agent->referrer());

			} else {
				$data_apply = array(
					'api_key' 				=> $apikey,
					'kode_lowongan' 		=> $kode_lowongan,
					'kode_perusahaan' 		=> $kode_perusahaan,
					'kode_pelamar' 			=> $kode_pelamar,
					'email_pelamar' 		=> $email_pelamar,
					'deskripsi_promosi'		=> $deskripsi_promosi,
					'wilayah_daftar'  		=> $nama_provinsi,
					'tanggal_daftar' 		=> date("Y-m-d H:i:s")
				);
				$m_query_detail->insert_apply_lowongan('Tbl_Apply', $data_apply);
				redirect('thanks-apply','refresh');
			}
		}
	}

	public function thanks_apply_view() {
		$this->load->view('frontpage/page/thanks_apply_view');
	}

	public function mantenance_view() {
		$this->load->view('503_view');
	}

}

/* End of file Frontpage.php */
/* Location: ./application/controllers/Frontpage.php */