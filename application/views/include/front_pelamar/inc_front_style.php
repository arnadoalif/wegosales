<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="situs online yang menyediakan layanan pencari lowongan kerja, tau untuk mencari pekerjaan dengan bidang yang di atur dalam sistem wegosales untuk mencari kriteria pekerja dengan standar yang telah di tentukan">
    <meta name="author" content="wegosales">
    <meta name="keywords" itemprop="keywords" content="info lowongan, lowongan kerja terbaru, loker 2018, peluang kerja, info kerja, cari lowongan kerja, lowongan kerja jogja, loker kerja, cari kerja, lowongan kerja 2018, situs lowongan kerja, situs loker">
    <link rel="canonical" href="http://www.wegosales.com/" />
    <meta name="robots" content="follow,index" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets_front/images/logo_icon.png') ?>">
    <title>Wegosales.com | Situs Lowongan Kerja Indonesia</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets_front/bootstrap/css/bootstrap.min.css'); ?> " rel="stylesheet">
    <!-- owl slider CSS -->
    <link href="<?php echo base_url('assets_front/assets/owl.carousel/owl.carousel.min.css'); ?> " rel="stylesheet">
    <link href="<?php echo base_url('assets_front/assets/owl.carousel/owl.theme.default.css'); ?> " rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets_front/css/style.css'); ?> " rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->