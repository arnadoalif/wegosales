<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->


<script src="<?php echo base_url('assets_front/js/jquery.min.js'); ?> "></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo base_url('assets_front/bootstrap/js/popper.min.js'); ?> "></script>
<script src="<?php echo base_url('assets_front/bootstrap/js/bootstrap.min.js'); ?> "></script>
<!--Wave Effects -->
<script src="<?php echo base_url('assets_front/js/waves.js'); ?> "></script>
<!--stickey kit -->
<script src="<?php echo base_url('assets_front/js/sticky-kit.min.js'); ?> "></script>
<!-- jQuery for carousel -->
<script src="<?php echo base_url('assets_front/assets/owl.carousel/owl.carousel.min.js'); ?> "></script>
<!--Custom JavaScript -->
<script src="<?php echo base_url('assets_front/js/custom.min.js'); ?> "></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115062658-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115062658-1');
</script>
