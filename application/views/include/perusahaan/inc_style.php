<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets_front/images/logo_icon.png') ?>">
<title>Wegosales.com Admin Perusahaan</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">

<!-- summernotes CSS -->
<link href="<?php echo base_url('assets/plugins/summernote/dist/summernote.css'); ?>" rel="stylesheet" />

<!--alerts CSS -->
<link href="<?php echo base_url('assets/plugins/sweetalert/sweetalert.css'); ?>" rel="stylesheet" type="text/css">

<!-- page CSS -->
<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/plugins/select2/dist/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/plugins/switchery/dist/switchery.min.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/plugins/bootstrap-select/bootstrap-select.min.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/plugins/multiselect/css/multi-select.css'); ?>" rel="stylesheet" type="text/css" />

<!-- morris CSS -->
<link href="<?php echo base_url('assets/plugins/morrisjs/morris.css'); ?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url('assets_admin/css/style.css'); ?>" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="<?php echo base_url('assets_admin/css/colors/blue.css'); ?>" id="theme" rel="stylesheet">
<style type="text/css">
        .topbar {
            background: #b80000;
        }

        .sidebar-nav ul li a.active, .sidebar-nav ul li a:hover {
		    color: #8d6c2c;
		}

		.sidebar-nav>ul>li.active>a {
		    color: #8d6c2c;
		    font-weight: 500;
		    border-left: 3px solid #8d6c2c;
		}
		
		.sidebar-nav > ul > li.active > a {
		    color: #8d6c2c;
		    border-color: #8d6c2c;
		}
		
		.sidebar-nav>ul>li.active>a {
		    color: #1976d2;
		    font-weight: 500;
		    border-left: 3px solid #8d6c2c;
		}

		.text-themecolor {
		    color: #8d6c2c !important;
		}

		.sidebar-nav>ul>li.active>a {
		    color: #8d6c2c;
		    font-weight: 500;
		    border-left: 3px solid #8d6c2c;
		}

		.sidebar-nav ul li a.active i, .sidebar-nav ul li a:hover i {
		    color: #8d6c2c;
		}

		.comment-text p {
    		   max-height: 100px;
    		   width: 100%;
    		   overflow: hidden;
		}
    </style>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->