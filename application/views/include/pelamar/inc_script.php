<!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/popper.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url('assets_pelamar/js/jquery.slimscroll.js'); ?> "></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url('assets_pelamar/js/waves.js'); ?> "></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url('assets_pelamar/js/sidebarmenu.js'); ?> "></script>
    <!--stickey kit -->
    <script src="<?php echo base_url('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/sparkline/jquery.sparkline.min.js'); ?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url('assets_pelamar/js/custom.min.js'); ?> "></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/skycons/skycons.js'); ?>"></script>
    <!-- chartist chart -->
    <script src="<?php echo base_url('assets/plugins/chartist-js/dist/chartist.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets_pelamar/js/dashboard3.js'); ?> "></script>

    <script src="<?php echo base_url('assets/plugins/switchery/dist/switchery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/select2/dist/js/select2.full.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-select/bootstrap-select.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js'); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/multiselect/js/jquery.multi-select.js'); ?>"></script>

    <!-- Sweet-Alert  -->
    <script src="<?php echo base_url('assets/plugins/sweetalert/sweetalert.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/sweetalert/jquery.sweet-alert.custom.js'); ?>"></script>

    <!-- Calendar JavaScript -->
    <script src="<?php echo base_url('assets/plugins/calendar/jquery-ui.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/moment/moment.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/calendar/dist/fullcalendar.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/calendar/dist/cal-init.js'); ?>"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/styleswitcher/jQuery.style.switcher.js'); ?>"></script>