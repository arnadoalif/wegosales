<!DOCTYPE html>
<html lang="en">

 <?php require_once(APPPATH .'views/include/superroot/inc_style.php'); ?>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('superroot/header'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('superroot/left_sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Dashboard</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                <!-- Row -->
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="fa fa-building text-info"></i></h2>
                                    <h3 class=""><?php echo $total_perusahan; ?></h3>
                                    <h6 class="card-subtitle">Company</h6></div>
                                <div class="col-12">
                                    <!-- <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="fa fa-user text-success"></i></h2>
                                    <h3 class=""><?php echo $total_pelamar ?></h3>
                                    <h6 class="card-subtitle">Member</h6></div>
                                <div class="col-12">
                                    <!-- <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="fa fa-briefcase text-purple"></i></h2>
                                    <h3 class=""><?php echo $total_apply ?></h3>
                                    <h6 class="card-subtitle">Job Apply</h6></div>
                                <div class="col-12">
                                    <!-- <div class="progress">
                                        <div class="progress-bar bg-primary" role="progressbar" style="width: 56%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-buffer text-warning"></i></h2>
                                    <h3 class=""><?php echo $total_lowongan ?></h3>
                                    <h6 class="card-subtitle">Vacancy</h6></div>
                                <div class="col-12">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover earning-box">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Company</th>
                                                <th>Email</th>
                                                <th>Website</th>
                                                <th>Country</th>
                                                <th>Vacancy / Loker</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($data_perusahan as $dt_perusahaan): ?>
                                                
                                                <tr>
                                                    <td style="width:50px;">
                                                        <a href="#modal-id<?php echo $dt_perusahaan->Kode_Perusahaan ?>"  data-toggle="modal" href='#modal-id' title="">
                                                            <span class="round" style="background: none;"><img src="<?php echo base_url('assets_storage/img_perusahaan/'.$dt_perusahaan->Logo_perusahaan) ?>" alt="user" width="50"></span></td>
                                                        </a>
                                                    <td>
                                                        <a href="#modal-id<?php echo $dt_perusahaan->Kode_Perusahaan ?>"  data-toggle="modal" href='#modal-id' title="">
                                                            <h6><?php echo $dt_perusahaan->Nama_Perusahaan ?></h6><small class="text-muted"><?php echo $dt_perusahaan->Nomor_Telepon .'<br>'.$dt_perusahaan->Nomor_Tlp_kantor ?></small>
                                                        </a>
                                                    </td>
                                                    <td><span class="label label-success"><?php echo $dt_perusahaan->Alamat_Email ?></span></td>
                                                    <td><?php echo $dt_perusahaan->Website ?></td>
                                                    <td><?php echo $dt_perusahaan->Negara ?></td>
                                                    <td>
                                                        <?php 
                                                            $this->load->model('Querydetail_model');
                                                            $m_query_detail = new Querydetail_model();
                                                            $total_lowongan = $m_query_detail->view_lowongan_by_perusahaan($dt_perusahaan->Kode_Perusahaan)->num_rows();
                                                         ?>
                                                         <a class="btn btn-info" href="<?php echo base_url('super/root/detail/'.$dt_perusahaan->Kode_Perusahaan); ?>" role="button"><?php echo $total_lowongan .' Loker'; ?></a>
                                                    </td>
                                                </tr>

                                                <div class="modal fade" id="modal-id<?php echo $dt_perusahaan->Kode_Perusahaan ?>">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title"><?php echo $dt_perusahaan->Nama_Perusahaan ?></h4>
                                                            </div>
                                                            <div class="modal-body">

                                                                <div class="col-xs-12 col-lg-12">
                            
                                                                    <div class="ribbon-wrapper card">
                                                                        <div class="ribbon ribbon-info">Gambaran Perusahaan</div>

                                                                        <p class="ribbon-content">

                                                                            <div class="row">
                                                                                <div class="col-xs-12 col-lg-6">
                                                                                    <small class="text-muted">Alamat Email</small>
                                                                                    <h6><?php echo $dt_perusahaan->Alamat_Email ?></h6>
                                                                                </div>
                                                                                <div class="col-xs-12 col-lg-6">
                                                                                    <small class="text-muted">Industri </small>
                                                                                    <h6><?php echo $dt_perusahaan->Industri ?></h6>
                                                                                </div>
                                                                                <div class="col-xs-12 col-lg-6">
                                                                                    <small class="text-muted">Nomor Kantor</small>
                                                                                    <h6><?php echo $dt_perusahaan->Nomor_Tlp_kantor ?></h6>
                                                                                </div>
                                                                                <div class="col-xs-12 col-lg-6">
                                                                                    <small class="text-muted">Website </small>
                                                                                    <?php if (!empty($dt_perusahaan->Website)): ?>
                                                                                        <h6><a href="<?php echo $dt_perusahaan->Website ?>" target="_blank" title="<?php $dt_perusahaan->Nama_Perusahaan ?>"><?php echo $dt_perusahaan->Website ?></a></h6>
                                                                                    <?php else: ?>
                                                                                        -
                                                                                    <?php endif ?>
                                                                                </div>
                                                                                
                                                                            </div>

                                                                        </p>
                                                                    </div>

                                                                    <div class="ribbon-wrapper card">
                                                                        <div class="ribbon ribbon-info">Deskripsi Perusahaan</div>
                                                                        <p class="ribbon-content"><?php echo $dt_perusahaan->Deskripsi_perusahaan ?></p>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php endforeach ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php $this->load->view('superroot/footer'); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php require_once(APPPATH .'views/include/superroot/inc_script.php'); ?>
</body>

</html>