<!DOCTYPE html>
<html lang="en">

 <?php require_once(APPPATH .'views/include/superroot/inc_style.php'); ?>
 <style type="text/css">
     .zoom {      
        -webkit-transition: all 0.35s ease-in-out;    
        -moz-transition: all 0.35s ease-in-out;    
        transition: all 0.35s ease-in-out;     
        cursor: -webkit-zoom-in;      
        cursor: -moz-zoom-in;      
        cursor: zoom-in;  
    }     

        .zoom:hover,  
        .zoom:active,   
        .zoom:focus {
        /**adjust scale to desired size, 
        add browser prefixes**/
        -ms-transform: scale(2.5);    
        -moz-transform: scale(2.5);  
        -webkit-transform: scale(2.5);  
        -o-transform: scale(2.5);  
        transform: scale(2.5);    
        position:relative;      
        z-index:100;  
    }

        /**To keep upscaled images visible on mobile, 
        increase left & right margins a bit**/  
        @media only screen and (max-width: 768px) {   
        ul.gallery {      
        margin-left: 15vw;       
        margin-right: 15vw;
    }

        /**TIP: Easy escape for touch screens,
        give gallery's parent container a cursor: pointer.**/
        .DivName {cursor: pointer}
    }

    .modal-lg {
        max-width: 1025px;
    }

    .comment-text {
        padding: 15px 15px 15px 20px;
        width: 100%;
    }
 </style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('superroot/header'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('superroot/left_sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Data Pelamar</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('super/root'); ?>">Home</a></li>
                        <li class="breadcrumb-item active">Data Pelamar</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Email Address</th>
                                                <th>Phone Number</th>
                                                <th>Gender</th>
                                                <th>Province</th>
                                                <th>Create At</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email Address</th>
                                                <th>Phone Number</th>
                                                <th>Gender</th>
                                                <th>Province</th>
                                                <th>Create At</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php foreach ($data_pelamar as $dt_pelamar): ?>
                                                
                                            <tr>
                                                <td>
                                                    <img src="<?php echo base_url('assets_storage/img_avatar/'.$dt_pelamar->img_profile); ?>" class="img-thumbnail zoom" alt="Image" width="50">
                                                </td>
                                                <td><a data-toggle="modal" href='#modal-id<?php echo $dt_pelamar->kode_pelamar ?>' title=""> <?php echo $dt_pelamar->nama_pelamar ?> </a></td>
                                                <td><?php echo $dt_pelamar->alamat_email ?></td>
                                                <td><?php echo $dt_pelamar->nomor_telepon ?></td>
                                                <td><?php echo $dt_pelamar->jenis_kelamin ?></td>
                                                <td><?php echo $dt_pelamar->provinsi ?></td>
                                                <td><?php echo $dt_pelamar->tanggal_daftar ?></td>
                                            </tr>

                                            <div class="modal fade" id="modal-id<?php echo $dt_pelamar->kode_pelamar ?>">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title"><?php echo $dt_pelamar->nama_pelamar ?></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            
                                                            <?php 
                                                                $info_lainnya = $this->db->query("SELECT * FROM Tbl_Info_Tambahan WHERE kode_pelamar = '$dt_pelamar->kode_pelamar'")->row();
                                                                $info_pengalaman =$this->db->query("SELECT TOP 1 * FROM Tbl_Pengalaman_Kerja WHERE kode_pelamar = '$dt_pelamar->kode_pelamar' ORDER BY kode_pengalaman DESC")->result();
                                                                $info_pendidikan = $this->db->query("SELECT kualifikasi, tahun_lulusan from Tbl_Riwayat_Pendidikan where kode_pelamar = '$dt_pelamar->kode_pelamar' order by kode_pendidikan desc")->row();

                                                                $det_pengalaman =$this->db->query("SELECT * FROM Tbl_Pengalaman_Kerja WHERE kode_pelamar = '$dt_pelamar->kode_pelamar' ORDER BY kode_pengalaman DESC")->result();
                                                                $det_pendidikan = $this->db->query("SELECT * from Tbl_Riwayat_Pendidikan where kode_pelamar = '$dt_pelamar->kode_pelamar' order by kode_pendidikan DESC")->result();
                                                                $det_keterampilan = $this->db->query("SELECT * from Tbl_Keterampilan where kode_pelamar = '$dt_pelamar->kode_pelamar'")->result();
                                                                $det_info_lainnya = $this->db->query("SELECT * FROM Tbl_Info_Tambahan WHERE kode_pelamar = '$dt_pelamar->kode_pelamar'")->result();
                                                              ?>

                                                            <div class="card">
                                                                <div class="card-body">

                                                                  <div class="row">                       
                                                                    <!-- Comment Row -->
                                                                    <div class="d-flex flex-row comment-row">
                                                                        <div class="p-2">
                                                                            <img src="<?php echo base_url('assets_storage/img_avatar/'.$dt_pelamar->img_profile); ?>" class=" img-rounded" width="90" />
                                                                        </div>
                                                                        <div class="comment-text ">
                                                                            <h5><?php echo $dt_pelamar->nama_pelamar ?></h5>   
                                                                            <div class="comment-footer">
                                                                                <div class="comment-footer">
                                                                                    <?php foreach ($info_pengalaman as $f_penggalaman): ?>
                                                                                        <span class="date"><strong><?php echo $f_penggalaman->posisi_kerja ?></strong></span><br>
                                                                                        <label><?php echo $f_penggalaman->nama_perusahaan ?></label>
                                                                                    <?php endforeach ?>
                                                                                    
                                                                                    <br>
                                                                                    <span class="" style="font-size: 15px;"> <i class="fa fa-phone"></i> <?php echo $dt_pelamar->nomor_telepon ?> | <i class="fa fa-envelope"></i> <?php echo $dt_pelamar->alamat_email ?> | <i class="fa fa-dollar"></i> 
                                                                                        <?php if (!empty($info_lainnya->mata_uang) && !empty($info_lainnya->gaji)): ?>
                                                                                            <?php echo $info_lainnya->mata_uang ?> - <?php echo number_format($info_lainnya->gaji); ?>
                                                                                        <?php else: ?>
                                                                                            <span class="label label-danger">Not Set</span> 
                                                                                        <?php endif ?>
                                                                                        
                                                                                          | <i class="fa fa-map-marker"></i> <?php echo $dt_pelamar->kota.', ',$dt_pelamar->provinsi ?></span> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- End Comment Row -->
                                                                  </div>

                                                                    <h4 class="font-medium m-t-30">Pengalaman</h4>
                                                                      <?php foreach ($det_pengalaman as $dt_pengalaman): ?>
                                                                        <!-- Comment Row -->
                                                                          <div class="d-flex flex-row comment-row">
                                                                              <div class="p-2">
                                                                                  <p><?php echo $dt_pengalaman->bulan_start_kerja ?>, <?php echo $dt_pengalaman->tahun_start_kerja ?></p> - 
                                                                                  <p>
                                                                                      <?php if (empty($dt_pengalaman->masih_kerja)): ?>
                                                                                          <?php echo $dt_pengalaman->bulan_start_selesai ?>, <?php echo $dt_pengalaman->tahun_start_selesai ?>
                                                                                      <?php else: ?>
                                                                                          Sampai Sekarang
                                                                                      <?php endif ?>
                                                                                  </p>
                                                                              </div>
                                                                              <div class="comment-text w-90">
                                                                                  <h5><?php echo $dt_pengalaman->posisi_kerja ?></h5>
                                                                                  <div class="comment-footer">
                                                                                      <span class="date"> <i class="fa fa-building"></i> <?php echo $dt_pengalaman->nama_perusahaan ?> | <?php echo $dt_pengalaman->negara ?></span>

                                                                                        <div class="row">
                                                                                            <div class="col-xs-12 col-lg-6">
                                                                                                <small class="text-muted">Industri</small>
                                                                                                <h6><?php echo $dt_pengalaman->industri ?></h6>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-lg-6">
                                                                                                <small class="text-muted">Spesialisasi</small>
                                                                                                <h6><?php echo $dt_pengalaman->spesialisasi ?></h6>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-lg-6">
                                                                                                <small class="text-muted">Bidang Pekerjaan</small>
                                                                                                <h6><?php echo $dt_pengalaman->bidang_pekerjaan ?></h6>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-lg-6">
                                                                                                <small class="text-muted">Jabatan</small>
                                                                                                <h6><?php echo $dt_pengalaman->jabatan ?></h6>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-lg-6">
                                                                                                <small class="text-muted">Gaji Perbulan</small>
                                                                                                <h6><?php echo $dt_pengalaman->mata_uang ?> - <?php echo number_format($dt_pengalaman->gaji); ?></h6>
                                                                                            </div>
                                                                                        </div>

                                                                                  </div>
                                                                                  
                                                                                  <p class="m-b-5 m-t-10"><?php echo $dt_pengalaman->keterangan_kerja ?></p>
                                                                              </div>
                                                                          </div>
                                                                          <hr>
                                                                        <!-- End Comment Row -->
                                                                        <?php endforeach ?>
                                                                      <hr>

                                                                    <h4 class="font-medium m-t-30">Pendidikan</h4>
                                                                      <?php foreach ($det_pendidikan as $dt_pendidikan): ?>                            
                                                                        <!-- Comment Row -->
                                                                        <div class="d-flex flex-row comment-row">
                                                                            <div class="p-2">
                                                                                <p><?php echo $dt_pendidikan->bulan_wisuda ?> , <?php echo $dt_pendidikan->tahun_lulusan ?></p>
                                                                            </div>
                                                                            <div class="comment-text w-90">
                                                                                <h5><?php echo $dt_pendidikan->nama_universitas ?></h5>
                                                                                <div class="comment-footer">
                                                                                    <span class="date"><?php echo $dt_pendidikan->kualifikasi ?> di <?php echo $dt_pendidikan->jurusan ?> | <?php echo $dt_pendidikan->lokasi ?></span>
                                                                                    
                                                                                </div>
                                                                                
                                                                                <div class="row">
                                                                                    <div class="col-xs-12 col-lg-6">
                                                                                        <small class="text-muted">Jurusan</small>
                                                                                        <h6><?php echo $dt_pendidikan->jurusan ?></h6>
                                                                                    </div>

                                                                                    <?php if ($dt_pendidikan->grade == "IPK"): ?>
                                                                                        <div class="col-xs-12 col-lg-6">
                                                                                            <small class="text-muted"><?php echo $dt_pendidikan->grade ?> </small>
                                                                                            <h6><?php echo $dt_pendidikan->nilai_asli ?> / <?php echo $dt_pendidikan->nilai_dari ?></h6>
                                                                                        </div>
                                                                                    <?php else: ?>
                                                                                        <div class="col-xs-12 col-lg-6">
                                                                                            <small class="text-muted">Hasil Studi</small>
                                                                                            <?php if ($dt_pendidikan->grade == "Masih Belajar"): ?>
                                                                                                <h6><span class="label label-info"><?php echo $dt_pendidikan->grade ?></span> </h6>
                                                                                            <?php else: ?>
                                                                                                <h6><span class="label label-danger"><?php echo $dt_pendidikan->grade ?></span> </h6>
                                                                                            <?php endif ?>
                                                                                        </div>
                                                                                        
                                                                                    <?php endif ?>
                                    
                                                                                </div>

                                                                                <p class="m-b-5 m-t-10"><?php echo $dt_pendidikan->informasi_tambahan ?></p>
                                                                            </div>
                                                                        </div>
                                                                        <!-- End Comment Row -->
                                                                        <?php endforeach ?>
                                                                      <hr>

                                                                      <h4 class="font-medium m-t-30">Keterampilan</h4>
                                                                        <div class="form-group row">
                                                                            <label for="keterampilan" class="col-sm-3 control-label">Keterampilan</label>
                                                                            <?php foreach ($det_keterampilan as $dt_keterampilan): ?>
                                                                            <div class="col-sm-9">
                                                                               <?php echo $dt_keterampilan->keterampilan ?>
                                                                            </div>
                                                                            <?php endforeach ?>
                                                                        </div>
                                                                      <hr>

                                                                      <h4 class="font-medium m-t-30">Bahasa</h4>
                                                                        <div class="form-group row">
                                                                            <label for="bahasa" class="col-sm-3 control-label">Bahasa</label>
                                                                            <div class="col-sm-9">
                                                                            <?php foreach ($det_keterampilan as $dt_keterampilan): ?>
                                                                                <?php echo $dt_keterampilan->kuasai_bahasa ?>
                                                                            <?php endforeach ?>
                                                                            </div>
                                                                        </div>
                                                                      <hr>

                                                                      <h4 class="font-medium m-t-30">Info Lain</h4>
                                                                        <?php foreach ($det_info_lainnya as $dt_info): ?>
                                                                            <div class="row">
                                                                                <div class="col-xs-12 col-lg-6">
                                                                                    <small class="text-muted">Gaji yang diharapkan</small>
                                                                                    <h6><?php echo $dt_info->mata_uang .' - '. number_format($dt_info->gaji); ?></h6>
                                                                                </div>
                                                                                <div class="col-xs-12 col-lg-6">
                                                                                    <small class="text-muted">Lokasi kerja yang diharapkan</small>
                                                                                    <h6><?php echo $dt_info->lokasi_kerja_1 .", ". $dt_info->lokasi_kerja_2 .", ". $dt_info->lokasi_kerja_3?></h6>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-lg-12">
                                                                                <small class="text-muted">Informasi lainnya</small>
                                                                                <h6><?php echo $dt_info->informasi_lainnya ?></h6>
                                                                            </div>

                                                                          <?php endforeach ?>
                                                                      <hr>

                                                                      <h4 class="font-medium m-t-30">Profil Saya</h4>
                                                                        <div class="card-body">
                                                                            <div class="row">
                                                                                <div class="col-xs-12 col-lg-12">
                                                                                    <small class="text-muted">Jenis Kelamin</small>
                                                                                    <h6>
                                                                                        <?php if (!empty($dt_apply->jenis_kelamin)): ?>
                                                                                            <?php echo $dt_apply->jenis_kelamin ?>
                                                                                        <?php else: ?> <span class="label label-danger">Not Set</span>  <?php endif ?>
                                                                                    </h6>
                                                                                </div>
                                                                                <div class="col-xs-12 col-lg-12">
                                                                                    <small class="text-muted">Alamat 1</small>
                                                                                    <h6>
                                                                                        <?php if (!empty($dt_apply->alamat_1)): ?>
                                                                                            <?php echo $dt_apply->alamat_1 ?>
                                                                                        <?php else: ?> <span class="label label-danger">Not Set</span>  <?php endif ?>
                                                                                    </h6>
                                                                                </div>

                                                                                <div class="col-xs-12 col-lg-12">
                                                                                    <small class="text-muted">Alamat 2</small>
                                                                                    <h6>
                                                                                        <?php if (!empty($dt_apply->alamat_2)): ?>
                                                                                          <?php echo $dt_apply->alamat_2 ?>
                                                                                        <?php else: ?>
                                                                                          <span class="label label-danger">Not Set</span> 
                                                                                        <?php endif ?>
                                                                                    </h6>
                                                                                </div>

                                                                                <div class="col-xs-12 col-lg-12">
                                                                                    <small class="text-muted">Tanggal Lahir</small>
                                                                                    <h6>
                                                                                        <?php if (!empty($dt_apply->tanggal) && !empty($dt_apply->bulan) && !empty($dt_apply->tahun)): ?>
                                                                                            <?php echo $dt_apply->tanggal.' '. $dt_apply->bulan.' '. $dt_apply->tahun ?>
                                                                                        <?php else: ?>
                                                                                            <span class="label label-danger">Not Set</span> 
                                                                                        <?php endif ?>
                                                                                        </h6>
                                                                                </div>

                                                                                <?php if (!empty($dt_apply->identitas)): ?>
                                                                                    <div class="col-xs-12 col-lg-12">
                                                                                        <small class="text-muted">Identitas</small>
                                                                                        <h6><?php echo $dt_apply->identitas ?></h6>
                                                                                    </div>
                                                                                <?php endif ?>

                                                                                <?php if (!empty($dt_apply->nomor_identitas)): ?>
                                                                                    <div class="col-xs-12 col-lg-12">
                                                                                        <small class="text-muted">Nomor Identitas</small>
                                                                                        <h6><?php echo $dt_apply->nomor_identitas ?></h6>
                                                                                    </div>
                                                                                <?php endif ?>
                                                                            </div>
                                                                      </div>
                                                                      <hr>
      
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php endforeach ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php $this->load->view('superroot/footer'); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php require_once(APPPATH .'views/include/superroot/inc_script.php'); ?>
    <!-- This is data table -->
    <script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
     <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
            $(document).ready(function() {
                var table = $('#example').DataTable({
                    "columnDefs": [{
                        "visible": false,
                        "targets": 2
                    }],
                    "order": [
                        [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
                // Order by the grouping
                $('#example tbody').on('click', 'tr.group', function() {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                        table.order([2, 'desc']).draw();
                    } else {
                        table.order([2, 'asc']).draw();
                    }
                });
            });
        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    </script>
</body>

</html>