<!DOCTYPE html>
<html lang="en">

 <?php require_once(APPPATH .'views/include/superroot/inc_style.php'); ?>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('superroot/header'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('superroot/left_sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Loker Detail's</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('super/root'); ?>">Home</a></li>
                        <li class="breadcrumb-item active">Loker Detail's</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card"> 
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover earning-box">
                                        <thead>
                                            <tr>
                                                <th>Loker Name</th>
                                                <th>Job Type</th>
                                                <th>Position</th>
                                                <th>Create at</th>
                                                <th>Close</th>
                                                <th>Status</th>
                                                <th>Job Apply</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($data_detail_lowongan as $dt_detail_lowongan): ?>
                                                <?php if ($dt_detail_lowongan->status_sekarang == "Close"): ?>
                                                    <tr style="background: red; color: #fff;">
                                                        <td>
                                                            <a href="#modal-id<?php echo $dt_detail_lowongan->kode_lowongan ?>" title="<?php echo $dt_detail_lowongan->nama_lowongan ?>" data-toggle="modal"><h6 style="color: #fff;"><?php echo $dt_detail_lowongan->nama_lowongan; ?></h6></a>
                                                        </td>
                                                <?php else: ?>
                                                    <tr>
                                                        <td>
                                                            <a href="#modal-id<?php echo $dt_detail_lowongan->kode_lowongan ?>" title="<?php echo $dt_detail_lowongan->nama_lowongan ?>" data-toggle="modal"><h6 style="color: #8f9cac;"><?php echo $dt_detail_lowongan->nama_lowongan; ?></h6></a>
                                                        </td>
                                                <?php endif ?>
                                                        
                                                        <td><?php echo $dt_detail_lowongan->pekerjaan_jenis ?></td>
                                                        <td><?php echo $dt_detail_lowongan->posisi_spesialis ?></td>
                                                        <td><?php echo $dt_detail_lowongan->tgl_insert ?></td>
                                                        <td><?php echo $dt_detail_lowongan->tanggal_tutup ?></td>
                                                        <td><?php echo $dt_detail_lowongan->status_sekarang ?></td>
                                                        <td>
                                                            <?php 
                                                                $this->load->model('Lowongan_model');
                                                                $m_lowongan = new Lowongan_model();
                                                                $data_pelamar = $m_lowongan->count_lowongan_posisi('Tbl_Apply', $dt_detail_lowongan->kode_lowongan)->result();
                                                             ?>
                                                         <?php foreach ($data_pelamar as $count_pelamar): ?>
                                                             <?php if (!empty($count_pelamar->tot_pegawai)): ?>
                                                                <a data-toggle="modal" class="btn btn-info" href="#detail-id<?php echo $dt_detail_lowongan->Kode_Perusahaan ?>" role="button"><?php echo $count_pelamar->tot_pegawai ?></a> 
                                                             <?php else: ?>
                                                                 0
                                                             <?php endif ?>
                                                         <?php endforeach ?>
                                                        </td>
                                                    </tr>


                                                    <div class="modal fade" id="modal-id<?php echo $dt_detail_lowongan->kode_lowongan ?>">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title"><?php echo $dt_detail_lowongan->nama_lowongan; ?> </h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    
                                                                    <div class="row">
                                                                        <div class="col-md-4 col-lg-4">
                                                                             <a href=""><img src="<?php echo base_url('assets_storage/img_iklan/'.$dt_detail_lowongan->cover_iklan) ?>" alt="user" class="img-responsive" "></a>
                                                                        </div>
                                                                        <div class="col-md-8 col-lg-8">
                                                                            <div class="ribbon-wrapper card">
                                                                                <div class="ribbon ribbon-info">Deskripsi Lowongan</div>
                                                                                <p class="ribbon-content">
                                                                                    <div class="row">
                                                                                        <div class="col-xs-12 col-lg-6">
                                                                                            <small class="text-muted">Nama Lowongan</small>
                                                                                            <h6><i class="fa fa-briefcase"></i> <?php echo $dt_detail_lowongan->nama_lowongan ?></h6>
                                                                                        </div>
                                                                                        <div class="col-xs-12 col-lg-6">
                                                                                            <small class="text-muted">Nama Perusahaan </small>
                                                                                            <h6><i class="fa fa-building"></i> <?php echo $dt_detail_lowongan->Nama_Perusahaan ?></h6>
                                                                                        </div>
                                                                                        <div class="col-xs-12 col-lg-6">
                                                                                            <small class="text-muted">Gaji</small>
                                                                                            <h6> <i class="fa fa-dollar"></i>
                                                                                                Min
                                                                                                <?php if (!empty($dt_detail_lowongan->salary_min)): ?>
                                                                                                    <?php echo "Rp ".number_format($dt_detail_lowongan->salary_min) ?>
                                                                                                <?php else: ?>
                                                                                                    0
                                                                                                <?php endif ?>
                                                                                                
                                                                                                <?php if (!empty($dt_detail_lowongan->salary_max)): ?>

                                                                                                    <?php echo "- Max Rp ".number_format($dt_detail_lowongan->salary_max) ?>
                                                                                                <?php else: ?>
                                                                                                    
                                                                                                <?php endif ?>
                                                                                            </h6>
                                                                                        </div>
                                                                                        <div class="col-xs-12 col-lg-6">
                                                                                            <small class="text-muted">Alamat Perusahaan </small>
                                                                                            <h6>
                                                                                              <i class="fa fa-map-marker"></i> <?php echo $dt_detail_lowongan->Alamat_perusahaan ?>
                                                                                              <?php if (!empty($dt_detail_lowongan->Negara)): ?>
                                                                                                  <label><?php echo ", ".$dt_detail_lowongan->Negara ?></label>
                                                                                              <?php endif ?>
                                                                                            </h6>
                                                                                        </div>
                                                                                        <div class="col-xs-12 col-lg-6">
                                                                                            <small class="text-muted">Tutup Lowongan </small>
                                                                                            <h6 style="color: red;"><i class="fa fa-calendar"></i> <?php echo date("d/m/Y", strtotime($dt_detail_lowongan->tanggal_tutup)); ?></h6>
                                                                                        </div>
                                                                                    </div>
                                                                                       
                                                                                </p>
                                                                            </div>

                                                                            <div class="ribbon-wrapper card">
                                                                                <div class="ribbon ribbon-info">Deskripsi Kerjaan</div>
                                                                                <p class="ribbon-content">
                                                                                    <div class="row">
                                                                                        <div class="col-xs-12 col-lg-6">
                                                                                            <small class="text-muted">Jenis Pekerjaan</small>
                                                                                            <h6><?php echo $dt_detail_lowongan->pekerjaan_jenis ?></h6>
                                                                                        </div>
                                                                                        <div class="col-xs-12 col-lg-6">
                                                                                            <small class="text-muted">Posisi Spesialis </small>
                                                                                            <h6><?php echo $dt_detail_lowongan->posisi_spesialis ?></h6>
                                                                                        </div>
                                                                                        <div class="col-xs-12 col-lg-6">
                                                                                            <small class="text-muted">Keahlian</small>
                                                                                            <h6><?php echo $dt_detail_lowongan->keahlian ?></h6>
                                                                                        </div>
                                                                                        <div class="col-xs-12 col-lg-6">
                                                                                            <small class="text-muted">Tingkat Pendidikan </small>
                                                                                            <h6><?php echo $dt_detail_lowongan->tingkat_pendidikan ?></h6>
                                                                                        </div>
                                                                                        <div class="col-xs-12 col-lg-6">
                                                                                            <small class="text-muted">Bidang Studi </small>
                                                                                            <h6><?php echo $dt_detail_lowongan->bidang_studi ?></h6>
                                                                                        </div>
                                                                                    </div>
                                                                                    <hr>

                                                                                    <?php echo $dt_detail_lowongan->deskripsi_pekerjaan ?>        
                                                                                </p>
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="modal fade" id="detail-id<?php echo $dt_detail_lowongan->Kode_Perusahaan ?>">
                                                      <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title"><?php echo $dt_detail_lowongan->Nama_Perusahaan ?></h4>
                                                          </div>
                                                          <div class="modal-body">
                                                            
                                                            <?php 

                                                                $this->load->model('Querydetail_model');
                                                                $m_query_detail = new Querydetail_model();
                                                                $data_pelamar = $m_query_detail->view_data_pelamar_by_super_root($dt_detail_lowongan->Kode_Perusahaan)->result();
                                                             ?>

                                                            <div class="row">
                                                               <?php foreach ($data_pelamar as $dt_pelamar): ?>
                                                              
                                                                  <div class="col-md-6 col-lg-6">
                                                                    <div class="card card-body">
                                                                      <div class="row">
                                                                          <div class="col-md-4 col-lg-3 text-center">
                                                                              <a href=""><img src="<?php echo base_url('assets_storage/img_avatar/'.$dt_pelamar->img_profile); ?>" alt="user" class="img-thumbnail img-responsive"></a>
                                                                          </div>
                                                                          <div class="col-md-8 col-lg-9">
                                                                              <h3 class="box-title m-b-0"><?php echo $dt_pelamar->nama_pelamar ?></h3> <small><?php echo $dt_pelamar->nama_lowongan ?></small>
                                                                          </div>
                                                                      </div>
                                                                    </div>
                                                                  </div>
                                                                
                                                              <?php endforeach ?>
                                                            </div>
                                                            

                                                          </div>
                                                          <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                
                                            <?php endforeach ?>                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php $this->load->view('superroot/footer'); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php require_once(APPPATH .'views/include/superroot/inc_script.php'); ?>
</body>

</html>