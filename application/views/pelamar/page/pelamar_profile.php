﻿<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once(APPPATH .'views/include/pelamar/inc_style.php'); ?>
    <style type="text/css">
        input[type='text'] {
            border-radius: 0px;
        }

        .form-control {
            border-radius: 0px;
        }

        .label-costume {
            font-size: 12px;
            margin: 0 auto;
            position: absolute;
            margin-top: -20px;
            margin-left: 95mm;
        }

        legend {
            font-size: 20px;
        }

        .btn-info, .btn-info.disabled {
            background: #b80000;
            border: 1px solid #8d6c2c;
        }

        .profile-tab li a.nav-link:hover, .customtab li a.nav-link:hover {
            color: #8d6c2c;
        }
        
        .profile-tab li a.nav-link.active, .customtab li a.nav-link.active {
            border-bottom: 2px solid #8d6c2c;
            color: #8d6c2c;
        }

        
        .customtab li a.nav-link:hover, .profile-tab li a.nav-link:hover {
            color: #8d6c2c;
        }
        
        .customtab li a.nav-link.active, .profile-tab li a.nav-link.active {
            border-bottom: 2px solid #8d6c2c;
            color: #8d6c2c;
        }
        
        .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
            color: #495057;
            background-color: #fff;
            border-color: #ddd #ddd #fff;
        }

        .profile-tab li a.nav-link.active, .customtab li a.nav-link.active {
            border-bottom: 2px solid #b80000;
            color: #b80000;
        }

        .label-info {
            background-color: #b80000;
        }

        a {
            color: #b80000;border-spacing: 
        }

        @media (min-width: 1025px) and (max-width: 1280px) {
          .hidden-lg {
            display: none;
          }
        }

        @media (min-width: 1281px) {
            .hidden-lg {
                display: none;
              }
        }


        @media (min-width: 320px) and (max-width: 480px) {
            .hidden-xs {
                display: none;
            }

            .font-sm {
                font-weight: 600;
            }

        .container-fluid {
                padding: 0 0px 25px 0px;
            }

        .card-no-border .card {
                border: 0px;
                 border-radius: 0px; 
                 box-shadow: none; 
                padding: 6px;
            }
            .card {
                 box-shadow: none; 
                border-radius: 4px;
            }
        }
    </style>
</head>

<body class="fix-header card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('pelamar/header'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('pelamar/left_sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- <div class="row page-titles m-b-0">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Dashboard3</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard3</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div> -->
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->

                <?php if (isset($_SESSION['sendmessage'])): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['sendmessage'] ?>
                    </div>
                <?php endif ?>
                <?php if (isset($_SESSION['error_data'])): ?>
                    <div class="alert alert-warning" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                    </div>
                <?php endif ?>

                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-3 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> <img src="<?php echo base_url('assets_storage/img_avatar/'.$this->session->userdata('img_profile')); ?>" class="img-thumbnail" width="150" />
                                    <h4 class="card-title m-t-10"><?php echo $this->session->userdata('nama_pelamar') ?></h4>
                                    
                                </center>

                                <a class="btn btn-block btn-info" data-toggle="modal" href='#upload-cv' role="button"><i class="fa fa-upload" style="color: #fff;"></i> Upload Resume</a>
                                <a class="btn btn-block btn-info" data-toggle="modal" href='#upload-avatar' role="button"><i class="fa fa-picture-o" style="color: #fff;"></i> Ubah Foto</a>

                                <?php 
                                    $kode_pelamar = $this->session->userdata('kode_pelamar');
                                    $tambahan = "select * from Tbl_Info_Tambahan where kode_pelamar    = '$kode_pelamar'";
                                    $keterampilan = "select * from Tbl_Keterampilan where kode_pelamar  = '$kode_pelamar'";
                                    $pengalaman = "select * from Tbl_Pengalaman_Kerja where kode_pelamar = '$kode_pelamar'";
                                    $pendidiakan = "select * from Tbl_Riwayat_Pendidikan where kode_pelamar = '$kode_pelamar'";

                                    $h_tambahan     = $this->db->query($tambahan)->row();
                                    $h_keterampilan = $this->db->query($keterampilan)->row();
                                    $h_pengalaman   = $this->db->query($pengalaman)->row();
                                    $h_pendidikan   = $this->db->query($pendidiakan)->row();
                                 ?>

                                <?php if ((empty($h_pengalaman) === true) || (empty($h_pendidikan) === true) || (empty($h_tambahan) === true) || (empty($h_keterampilan) === true)  ): ?>
                                <?php else: ?>
                                    <a class="btn btn-block btn-info hidden-xs" data-toggle="modal" href='#print' role="button" style="color: #fff;"><i class="fa fa-print"></i> Print CV</a>
                                <?php endif ?>
                                
                                
                                <div class="modal fade" id="upload-cv">
                                    <div class="modal-dialog modal-lg">
                                        <form action="<?php echo base_url('pelamar/action_upload_resume') ?>" method="POST" enctype="multipart/form-data">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Resume yang diunggah</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <input type="file" name="file_resume" class="form-control" value="">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Upload Resume</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="modal fade" id="upload-avatar">
                                    <div class="modal-dialog modal-lg">
                                        <form action="<?php echo base_url('pelamar/action_ubah_avatar') ?>" method="POST" enctype="multipart/form-data">
                                            
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Ubah tampilan profil</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <input type="file" name="img_avatar" class="form-control" value="">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Ubah avatar</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>

                                <div class="modal fade" id="print">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <a class="btn btn-default" target="_blank" href="<?php echo base_url('pelamar/admin/download-cv/'.$this->session->userdata('kode_pelamar')); ?>" role="button"><i class="fa fa-print"></i> PRINT</a>
                                                <!-- <button type="button" class="btn btn-primary" onclick="PrintMe('divid')"> <i class="fa fa-print"></i> PRINT </button> -->
                                            </div>
                                            <div class="modal-body" id="divid">
                                                <div class="d-flex flex-row comment-row">
                                                    <div class="p-2"><span class=""><img src="<?php echo base_url('assets_storage/img_avatar/'.$this->session->userdata('img_profile')); ?>" alt="user" width="100"></span></div>
                                                    <div class="comment-text w-100">
                                                        <h5><?php echo $data_cv_profile->nama_pelamar ?> 
                                                            <?php 
                                                                $this->load->model('Lib_model');
                                                                $m_lib = new Lib_model();

                                                                $result = $m_lib->calculate_age($this->session->userdata('kode_pelamar')); 
                                                                echo ("/ ".$result);
                                                            ?>
                                                        </h5>
                                                        <div class="comment-footer">
                                                            <span class="date"><?php echo $data_cv_profile->posisi_kerja ?></span><br>
                                                            <label><?php echo $data_cv_profile->nama_perusahaan ?></label>
                                                            <br>
                                                            <span class="" style="font-size: 15px;"> <i class="fa fa-phone"></i> <?php echo $data_cv_profile->nomor_telepon ?> | <i class="fa fa-envelope"></i> <?php echo $data_cv_profile->alamat_email ?> | <i class="fa fa-dollar"></i> <?php echo $data_cv_profile->mata_uang.' '.number_format($data_cv_profile->gaji) ?> | <i class="fa fa-map-marker"></i> <?php echo $data_cv_profile->kota.', ',$data_cv_profile->provinsi ?></span> 
                                                            
                                                        </div>
                                                        <p class="m-b-5 m-t-10"></p>
                                                    </div>
                                                </div>
                                                
                                                <hr>
                                                <fieldset>
                                                    <legend><i class="fa fa-briefcase"></i> Pengalaman</legend>
                                                
                                                    <?php foreach ($data_pengalaman as $dt_pengalaman): ?>
                                                    <!-- Comment Row -->
                                                        <div class="d-flex flex-row comment-row">
                                                            <div class="p-2">
                                                                <p><?php echo $dt_pengalaman->bulan_start_kerja ?>, <?php echo $dt_pengalaman->tahun_start_kerja ?></p> - 
                                                                <p>
                                                                    <?php if (empty($dt_pengalaman->masih_kerja)): ?>
                                                                        <?php echo $dt_pengalaman->bulan_start_selesai ?>, <?php echo $dt_pengalaman->tahun_start_selesai ?>
                                                                    <?php else: ?>
                                                                        Sampai Sekarang
                                                                    <?php endif ?>
                                                                </p>
                                                            </div>
                                                            <div class="comment-text w-90">
                                                                <h5><?php echo $dt_pengalaman->posisi_kerja ?></h5>
                                                                <div class="comment-footer">
                                                                    <span class="date"> <i class="fa fa-building"></i> <?php echo $dt_pengalaman->nama_perusahaan ?> | <?php echo $dt_pengalaman->negara ?></span>
                                                                    <span class="action-icons">
                                                                        <a data-toggle="modal" href='#edit_data_pengalaman<?php echo $dt_pengalaman->kode_pengalaman ?>'><i class="mdi mdi-pencil-circle"></i></a>
                                                                        <a href="<?php echo base_url('pelamar/admin/remove-pengalaman/'.$dt_pengalaman->kode_pengalaman); ?>"><i class="fa fa-trash"></i></a>   
                                                                    </span>
                                                                </div>
                                                                <table class="table">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Industri</td>
                                                                            <td><?php echo $dt_pengalaman->industri ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Spesialisasi</td>
                                                                            <td><?php echo $dt_pengalaman->spesialisasi ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Bidang Pekerjaan</td>
                                                                            <td><?php echo $dt_pengalaman->bidang_pekerjaan ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Jabatan</td>
                                                                            <td><?php echo $dt_pengalaman->jabatan ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Gaji Perbulan</td>
                                                                            <td><?php echo $dt_pengalaman->mata_uang ?> - <?php echo number_format($dt_pengalaman->gaji); ?></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <p class="m-b-5 m-t-10"><?php echo $dt_pengalaman->keterangan_kerja ?></p>
                                                            </div>
                                                        </div>
                                                    <!-- End Comment Row -->
                                                    <?php endforeach ?>

                                                </fieldset>

                                                <hr>
                                                <fieldset>
                                                    <legend><i class="fa fa-graduation-cap"></i> Pendidikan</legend>

                                                    <?php foreach ($data_pendidikan as $dt_pendidikan): ?>
                                                    
                                                    <!-- Comment Row -->
                                                    <div class="d-flex flex-row comment-row">
                                                        <div class="p-2">
                                                            <p><?php echo $dt_pendidikan->bulan_wisuda ?> , <?php echo $dt_pendidikan->tahun_lulusan ?></p>
                                                        </div>
                                                        <div class="comment-text w-90">
                                                            <h5><?php echo $dt_pendidikan->nama_universitas ?></h5>
                                                            <div class="comment-footer">
                                                                <span class="date"><?php echo $dt_pendidikan->kualifikasi ?> di <?php echo $dt_pendidikan->jurusan ?> | <?php echo $dt_pendidikan->lokasi ?></span>
                                                                <span class="action-icons">
                                                                    <a data-toggle="modal" href='#edit_data_pendidikan<?php echo $dt_pendidikan->kode_pendidikan ?>'><i class="mdi mdi-pencil-circle"></i></a>
                                                                    <a href="<?php echo base_url('pelamar/admin/remove-pendidikan/'.$dt_pendidikan->kode_pendidikan) ?>"><i class="fa fa-trash"></i></a>   
                                                                </span>
                                                            </div>
                                                            <table class="table">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Jurusan</td>
                                                                        <td><?php echo $dt_pendidikan->jurusan ?></td>
                                                                    </tr>
                                                                    <?php if ($dt_pendidikan->grade == "IPK"): ?>
                                                                        <tr>
                                                                            <td><?php echo $dt_pendidikan->grade ?></td>
                                                                            <td><?php echo $dt_pendidikan->nilai_asli ?> / <?php echo $dt_pendidikan->nilai_dari ?></td>
                                                                        </tr>
                                                                    <?php else: ?>
                                                                        <tr>
                                                                            <td>Hasil Studi</td>
                                                                            <td>
                                                                                <?php if ($dt_pendidikan->grade == "Masih Belajar"): ?>
                                                                                    <span class="label label-info"><?php echo $dt_pendidikan->grade ?></span> 
                                                                                <?php else: ?>
                                                                                    <span class="label label-danger"><?php echo $dt_pendidikan->grade ?></span> 
                                                                                <?php endif ?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php endif ?>
                                                                    
                                                                    
                                                                </tbody>
                                                            </table>
                                                            <p class="m-b-5 m-t-10"><?php echo $dt_pendidikan->informasi_tambahan ?></p>
                                                        </div>
                                                    </div>
                                                    <!-- End Comment Row -->
                                                    <?php endforeach ?>

                                                </fieldset>

                                                <hr>
                                                <fieldset>
                                                    <legend><i class="fa fa-code"></i> Keterampilan</legend>
                                                    <div class="form-group row">
                                                        <label for="keterampilan" class="col-sm-3 control-label">Keterampilan</label>
                                                        <?php foreach ($data_keterampilan as $dt_keterampilan): ?>
                                                        <div class="col-sm-9">
                                                           <?php echo $dt_keterampilan->keterampilan ?>
                                                        </div>
                                                        <?php endforeach ?>
                                                    </div>
                                                </fieldset>

                                                <hr>
                                                <fieldset>
                                                    <legend><i class="fa fa-comments-o"></i> Bahasa</legend>
                                                    <div class="form-group row">
                                                        <label for="bahasa" class="col-sm-3 control-label">Bahasa</label>
                                                        <div class="col-sm-9">
                                                        <?php foreach ($data_keterampilan as $dt_keterampilan): ?>
                                                            <?php echo $dt_keterampilan->kuasai_bahasa ?>
                                                        <?php endforeach ?>
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                <hr>
                                                <fieldset>
                                                    <legend><i class="fa fa-align-justify"></i> Info Gaji</legend>
                                                    <?php foreach ($data_info as $dt_info): ?>
                                            
                                                    <table class="table table-hover">
                                                        <tbody>
                                                            <tr>
                                                                <td>Gaji yang diharapkan</td>
                                                                <td><?php echo $dt_info->mata_uang .' - '. number_format($dt_info->gaji); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Lokasi kerja yang diharapkan</td>
                                                                <td><?php echo $dt_info->lokasi_kerja_1 .", ". $dt_info->lokasi_kerja_2 .", ". $dt_info->lokasi_kerja_3?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Informasi lainnya</td>
                                                                <td><?php echo $dt_info->informasi_lainnya ?> </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <?php endforeach ?>
                                                </fieldset>

                                                <hr>
                                                <fieldset>
                                                    <legend><i class="fa fa-user"></i> Profil Saya</legend>
                                                    <?php foreach ($data_profil as $dt_profile): ?>
                                        
                                                    <div class="card-body">
                                                        <table class="table table-hover">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Jenis Kelamin</td>
                                                                    <td>
                                                                        <?php if (!empty($dt_profile->jenis_kelamin)): ?>
                                                                            <?php echo $dt_profile->jenis_kelamin ?>
                                                                        <?php else: ?> <i>Not Set</i> <?php endif ?>
                                                                    </td>
                                                                </tr> 
                                                                <tr>
                                                                    <td>Alamat 1</td>
                                                                    <td>
                                                                        <?php if (!empty($dt_profile->alamat_1)): ?>
                                                                            <?php echo $dt_profile->alamat_1 ?>
                                                                        <?php else: ?> <i>Not set</i> <?php endif ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Alamat 2</td>
                                                                    <td>
                                                                        <?php if (!empty($dt_profile->alamat_2)): ?>
                                                                            <?php echo $dt_profile->alamat_2 ?>
                                                                        <?php else: ?>
                                                                            <i>Not set</i>
                                                                        <?php endif ?>
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td>Tanggal Lahir</td>
                                                                    <td>
                                                                        <?php echo $dt_profile->tanggal.' '. $dt_profile->bulan.' '. $dt_profile->tahun ?>
                                                                    </td>
                                                                </tr>

                                                                <?php if (!empty($dt_profile->identitas)): ?>
                                                                    <tr>
                                                                        <td>Identitas</td>
                                                                        <td><?php echo $dt_profile->identitas ?></td>
                                                                    </tr>
                                                                <?php endif ?>
                                                                
                                                                <?php if (!empty($dt_profile->nomor_identitas)): ?>
                                                                    <tr>
                                                                        <td>Nomor Identitas</td>
                                                                        <td><?php echo $dt_profile->nomor_identitas ?></td>
                                                                    </tr>
                                                                <?php endif ?>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <?php endforeach ?>
                                                </fieldset>
                                            </div>

                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div>
                                <hr> </div>
                            <div class="card-body"> 
                                <small class="text-muted">Email address </small>
                                <h6><?php echo $this->session->userdata('alamat_email'); ?></h6> 
                                <small class="text-muted">Nomor Telepon</small>
                                <h6><?php echo $this->session->userdata('nomor_telepon'); ?></h6>
                                <small class="text-muted">Umur</small>
                                <h6>
                                    <?php 
                                        $this->load->model('Lib_model');
                                        $m_lib = new Lib_model();

                                        $result = $m_lib->calculate_age($this->session->userdata('kode_pelamar')); 
                                        echo ($result);
                                    ?>
                                </h6> 
                                
                            </div>
                        </div>
                    </div>
                    <!-- Column -->



                    <!-- Column -->
                    <div class="col-lg-9 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist" id="myTab">
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#pengalaman" role="tab"><i class="fa fa-briefcase"></i> Pengalaman</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#pendidikan" role="tab"><i class="fa fa-graduation-cap"></i> Pendidikan</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#keterampilan" role="tab"><i class="fa fa-code"></i> Keterampilan</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#bahasa" role="tab"><i class="fa fa-comments-o"></i> Bahasa</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#infolain" role="tab"><i class="fa fa-align-justify"></i> Info Gaji</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profilsaya" role="tab"><i class="fa fa-user"></i> Profil Saya</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">

                               <div class="tab-pane active" id="pengalaman" role="tabpanel">
                                    <div class="card-body">
                                        <h3><i class="fa fa-briefcase"></i> Pengalaman 
                                            <div class="pull-right hidden-xs">
                                                <a data-toggle="modal" href='#tambah_pengalaman'><button class="btn btn-xs btn-circle btn-info"> <i class="fa fa-plus"></i> </button></a>
                                            </div>
                                        </h3>

                                        <div class="card">
                                            <div class="card-body">
                                                <!-- <h4 class="card-title">Recent Comments</h4> -->
                                                <h6 class="card-subtitle">Tambahkan jenjang pendidikan yang tertinggi</h6> 
                                                <a class="btn btn-block btn-info hidden-lg" data-toggle="modal" href='#tambah_pengalaman' role="button"><i class="fa fa-plus"></i> Tambah Pengalaman</a>
                                            </div>
                                            <!-- ============================================================== -->
                                            <!-- Comment widgets -->
                                            <!-- ============================================================== -->
                                            <div class="row">
                                                <?php foreach ($data_pengalaman as $dt_pengalaman): ?>
                                                    
                                                
                                                <div class="col-xs-12 col-lg-3">
                                                    <div class="p-2">
                                                        <p><?php echo $dt_pengalaman->bulan_start_kerja ?>, <?php echo $dt_pengalaman->tahun_start_kerja ?> - 
                                                        
                                                            <?php if (empty($dt_pengalaman->masih_kerja)): ?>
                                                                <?php echo $dt_pengalaman->bulan_start_selesai ?>, <?php echo $dt_pengalaman->tahun_start_selesai ?>
                                                            <?php else: ?>
                                                                Sampai Sekarang
                                                            <?php endif ?>
                                                        </p>
                                                    </div>
                                                   
                                                </div>

                                                <div class="col-xs-12 col-lg-9">
                                                    <h5><?php echo $dt_pengalaman->posisi_kerja ?></h5>
                                                    <div class="comment-footer">
                                                        <span class="date"> <i class="fa fa-building"></i> <?php echo $dt_pengalaman->nama_perusahaan ?> | <?php echo $dt_pengalaman->negara ?></span>
                                                       <!--  <span class="action-icons"> -->
                                                            <a data-toggle="modal" href='#edit_data_pengalaman<?php echo $dt_pengalaman->kode_pengalaman ?>'><i class="mdi mdi-pencil-circle"></i></a>
                                                            
                                                            <a id="hapus_pengalaman" href="<?php echo base_url('pelamar/admin/remove-pengalaman/'.$dt_pengalaman->kode_pengalaman); ?>"><i class="fa fa-trash"></i></a>   
                                                        <!-- </span> -->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-4 col-sm-4">
                                                            <strong class="font-sm">Industri </strong> 
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8">
                                                            <?php echo $dt_pengalaman->industri ?>
                                                        </div>

                                                        <div class="col-xs-4 col-sm-4">
                                                            <strong class="font-sm">Spesialisasi </strong> 
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8">
                                                            <?php echo $dt_pengalaman->spesialisasi ?>
                                                        </div>

                                                        <div class="col-xs-4 col-sm-4">
                                                            <strong class="font-sm">Bidang Pekerjaan </strong> 
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8">
                                                            <?php echo $dt_pengalaman->bidang_pekerjaan ?>
                                                        </div>

                                                        <div class="col-xs-4 col-sm-4">
                                                            <strong class="font-sm">Jabatan </strong> 
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8">
                                                            <?php echo $dt_pengalaman->jabatan ?>
                                                        </div>

                                                        <div class="col-xs-4 col-sm-4">
                                                            <strong class="font-sm">Gaji Perbulan </strong> 
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8">
                                                            <?php echo $dt_pengalaman->mata_uang ?> - <?php echo number_format($dt_pengalaman->gaji); ?>
                                                        </div>

                                                    </div>

                                                    <p class="m-b-5 m-t-10"><?php echo $dt_pengalaman->keterangan_kerja ?></p>
                                                    <hr>
                                                </div>

                                                <div class="modal fade" id="edit_data_pengalaman<?php echo $dt_pengalaman->kode_pengalaman ?>">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title"><?php echo $dt_pengalaman->posisi_kerja; ?></h4>
                                                            </div>
                                                            <form class="form-horizontal p-t-20" method="POST" action="<?php echo base_url('pelamar/action_update_pengalaman'); ?>">
                                                                <input type="hidden" name="kode_pengalaman" id="inputKode_" class="form-control" value="<?php echo $dt_pengalaman->kode_pengalaman ?>">
                                                                <div class="modal-body">
                                                                    <div class="form-group row">
                                                                        <label for="posisi_kerja" class="col-sm-3 control-label">Posisi *</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" name="posisi_kerja" id="inputPosisi" class="form-control" value="<?php echo $dt_pengalaman->posisi_kerja ?>" required="required">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="nama_perusahaan" class="col-sm-3 control-label">Nama Perusahaan *</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" name="nama_perusahaan" id="inputNamaperusahaan" class="form-control" value="<?php echo $dt_pengalaman->nama_perusahaan ?> " required="required">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="lama_berkerja" class="col-sm-3 control-label">Lama Berkerja</label>
                                                                        <div class="col-sm-3">
                                                                            <select name="bulan_start_kerja" id="inputBulan" class="form-control" required="required">
                                                                                <?php if (!empty($dt_pengalaman->bulan_start_kerja)): ?>
                                                                                    <option value="<?php echo $dt_pengalaman->bulan_start_kerja ?>" selected><?php echo $dt_pengalaman->bulan_start_kerja ?></option>
                                                                                <?php else: ?>
                                                                                    <option value="" selected>Bulan</option>
                                                                                <?php endif ?>
                                                                                <option value="Januari">Januari</option>
                                                                                <option value="Februari">Februari</option>
                                                                                <option value="Maret">Maret</option>
                                                                                <option value="April">April</option>
                                                                                <option value="Mei">Mei</option>
                                                                                <option value="Juni">Juni</option>
                                                                                <option value="Juli">Juli</option>
                                                                                <option value="Agustus">Agustus</option>
                                                                                <option value="September">September</option>
                                                                                <option value="Oktober">Oktober</option>
                                                                                <option value="November">November</option>
                                                                                <option value="Desember">Desember</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <select name="tahun_start_kerja" id="inputTahun" class="form-control" required="required">
                                                                                <?php if (!empty($dt_pengalaman->tahun_start_kerja)): ?>
                                                                                    <option value="<?php echo $dt_pengalaman->tahun_start_kerja ?>" selected><?php echo $dt_pengalaman->tahun_start_kerja ?></option>
                                                                                <?php else: ?>
                                                                                    <option value="" selected>Tahun</option>
                                                                                <?php endif ?>
                                                                                
                                                                                <?php $year = date('Y'); $min = $year - 60; $max = $year; ?>
                                                                                <?php for ($i=$max; $i>=$min; $i-- ): ?>
                                                                                     <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                                                <?php endfor ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <label class="label-costume">sampai</label>

                                                                    <div class="form-group row">
                                                                        <label for="lama_berkerja" class="col-sm-3 control-label"></label>

                                                                        <div class="col-sm-3">
                                                                            <?php if (!empty($dt_pengalaman->masih_kerja)): ?>
                                                                                <select name="bulan_start_selesai" disabled class="form-control bulan_start_selesai_update" required="required">
                                                                            <?php else: ?>
                                                                                <select name="bulan_start_selesai" class="form-control bulan_start_selesai_update" required="required">
                                                                            <?php endif ?>
                                                                            
                                                                                <?php if (!empty($dt_pengalaman->bulan_start_selesai)): ?>
                                                                                    <option value="<?php echo $dt_pengalaman->bulan_start_selesai ?>" selected><?php echo $dt_pengalaman->bulan_start_selesai ?></option>
                                                                                <?php else: ?>
                                                                                    <option value="" selected>Bulan</option>
                                                                                <?php endif ?>
                                                                                <option value="Januari">Januari</option>
                                                                                <option value="Februari">Februari</option>
                                                                                <option value="Maret">Maret</option>
                                                                                <option value="April">April</option>
                                                                                <option value="Mei">Mei</option>
                                                                                <option value="Juni">Juni</option>
                                                                                <option value="Juli">Juli</option>
                                                                                <option value="Agustus">Agustus</option>
                                                                                <option value="September">September</option>
                                                                                <option value="Oktober">Oktober</option>
                                                                                <option value="November">November</option>
                                                                                <option value="Desember">Desember</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <?php if (!empty($dt_pengalaman->masih_kerja)): ?>
                                                                                 <select name="tahun_start_selesai" disabled class="form-control tahun_start_selesai_update" required="required">
                                                                            <?php else: ?>
                                                                                 <select name="tahun_start_selesai" class="form-control tahun_start_selesai_update" required="required">
                                                                            <?php endif ?>

                                                                            <?php if (!empty($dt_pengalaman->tahun_start_selesai)): ?>
                                                                                <option value="<?php echo $dt_pengalaman->tahun_start_selesai ?>" selected><?php echo $dt_pengalaman->tahun_start_selesai ?></option>
                                                                            <?php endif ?>
                                                                               
                                                                                <option value="" selected>Tahun</option>
                                                                                
                                                                                <?php $year = date('Y'); $min = $year - 60; $max = $year; ?>
                                                                                <?php for ($i=$max; $i>=$min; $i-- ): ?>
                                                                                     <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                                                <?php endfor ?>
                                                                            </select>
                                                                        </div>

                                                                        <div class="col-sm-3">
                                                                            <?php if (!empty($dt_pengalaman->masih_kerja)): ?>
                                                                                <div class="checkbox checkbox-info">
                                                                                    <input type="checkbox" name="masih_kerja_update" checked id="masih_kerja_update<?php echo $dt_pengalaman->kode_pengalaman ?>" class="filled-in" value="sampai_sekarang">
                                                                                    <label for="masih_kerja_update<?php echo $dt_pengalaman->kode_pengalaman ?>" class=""> Sekarang </label>
                                                                                </div>
                                                                            <?php else: ?>
                                                                                <div class="checkbox checkbox-info">
                                                                                    <input type="checkbox" name="masih_kerja_update" id="masih_kerja_update<?php echo $dt_pengalaman->kode_pengalaman ?> " class="filled-in" value="sampai_sekarang">
                                                                                    <label for="masih_kerja_update<?php echo $dt_pengalaman->kode_pengalaman ?> " class=""> Sekarang </label>
                                                                                </div>
                                                                            <?php endif ?>
                                                                            
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="spesialisasi" class="col-sm-3 control-label">Spesialisasi</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="spesialisasi" id="inputspesialisasi" class="form-control" required="required">
                                                                                <?php if (!empty($dt_pengalaman->spesialisasi)): ?>
                                                                                    <option value="<?php echo $dt_pengalaman->spesialisasi ?>" selected><?php echo $dt_pengalaman->spesialisasi ?></option>
                                                                                <?php else: ?>
                                                                                    <option value="" selected>Pilih Spesialisasi</option>
                                                                                <?php endif ?>
                                                                                  
                                                                                  <option value="Arsitek/Desain Interior"> Arsitek/Desain Interior </option>
                                                                                  <option value="Bioteknologi"> Bioteknologi </option>
                                                                                  <option value="Clerical/Staf Admin"> Clerical/Staf Admin </option>
                                                                                  <option value="Customer Service"> Customer Service </option>
                                                                                  <option value="Engineering - Mechanical/Otomotif"> Engineering - Mechanical/Otomotif </option>
                                                                                  <option value="Geologi/Geofisika"> Geologi/Geofisika </option>
                                                                                  <option value="Hiburan/Seni Panggung"> Hiburan/Seni Panggung </option>
                                                                                  <option value="Hotel/Pariwisata"> Hotel/Pariwisata </option>
                                                                                  <option value="HRD"> HRD </option>
                                                                                  <option value="Hukum/Pelayanan Legal"> Hukum/Pelayanan Legal </option>
                                                                                  <option value="Ilmu Komunikasi"> Ilmu Komunikasi </option>
                                                                                  <option value="Ilmu Pasti /Statistik"> Ilmu Pasti /Statistik </option>
                                                                                  <option value="Ilmu Sains dan Labrotarium"> Ilmu Sains dan Labrotarium </option>
                                                                                  <option value="IT/Komputer - Jaringan/Sistem/Sistem Database"> IT/Komputer - Jaringan/Sistem/Sistem Database </option>
                                                                                  <option value="IT/Komputer - Perangkat Keras"> IT/Komputer - Perangkat Keras </option>
                                                                                  <option value="IT/Komputer - Perangkat Lunak"> IT/Komputer - Perangkat Lunak </option>
                                                                                  <option value="Jurnalis/Editor"> Jurnalis/Editor </option>
                                                                                  <option value="Keamanan/Angkatan Bersenjata/Proctevtive Service"> Keamanan/Angkatan Bersenjata/Proctevtive Service </option>
                                                                                  <option value="Kesehatan - Dokter/Diagnnosa"> Kesehatan - Dokter/Diagnnosa </option>
                                                                                  <option value="Kesehatan - Farmasi"> Kesehatan - Farmasi </option>
                                                                                  <option value="Kesehatan - Suster &amp; Asisten Medis"> Kesehatan - Suster &amp; Asisten Medis </option>
                                                                                  <option value="Keuangan - Audit/Pajak"> Keuangan - Audit/Pajak </option>
                                                                                  <option value="Keuangan - Keuangan Perusahaan/Investasi/Perbankan Retail"> Keuangan - Keuangan Perusahaan/Investasi/Perbankan Retail </option>
                                                                                  <option value="Keuangan - Umum/Akuntansi Pembiayaan"> Keuangan - Umum/Akuntansi Pembiayaan </option>
                                                                                  <option value="Kimia"> Kimia </option>
                                                                                  <option value="Kontrol dan Proses design"> Kontrol dan Proses design </option>
                                                                                  <option value="Logistik / Supply Chain"> Logistik / Supply Chain </option>
                                                                                  <option value="Makanan/Minuman/Pelayanan Restoran"> Makanan/Minuman/Pelayanan Restoran </option>
                                                                                  <option value="Manufaktur/Operasi Produksi"> Manufaktur/Operasi Produksi </option>
                                                                                  <option value="Merchandising"> Merchandising </option>
                                                                                  <option value="Pekerjaan Umum (Pengurus Rumah,Sopir,Pengantar Barang)"> Pekerjaan Umum (Pengurus Rumah,Sopir,Pengantar Barang) </option>
                                                                                  <option value="Pelatihan &amp; Pengembangan"> Pelatihan &amp; Pengembangan </option>
                                                                                  <option value="Pelayanan Sosial &amp; Konseling"> Pelayanan Sosial &amp; Konseling </option>
                                                                                  <option value="Pemasaran/Pengembangan Bisnis"> Pemasaran/Pengembangan Bisnis </option>
                                                                                  <option value="Pembelian/Inventaris/Manajemen Barang &amp; Pergudangan"> Pembelian/Inventaris/Manajemen Barang &amp; Pergudangan </option>
                                                                                  <option value="Pemeliharaan (Fasilitas &amp; Mesin)"> Pemeliharaan (Fasilitas &amp; Mesin) </option>
                                                                                  <option value="Pendidikan"> Pendidikan </option>
                                                                                  <option value="Penerbangan"> Penerbangan </option>
                                                                                  <option value="Penerbitan/Percetakan"> Penerbitan/Percetakan </option>
                                                                                  <option value="Penjualan - Corporate"> Penjualan - Corporate </option>
                                                                                  <option value="Penjualan - Engineering/Teknikal/IT"> Penjualan - Engineering/Teknikal/IT </option>
                                                                                  <option value="Penjualan - Keuangan ( Asuransi, Saham, Etc )"> Penjualan - Keuangan ( Asuransi, Saham, Etc ) </option>
                                                                                  <option value="Penjualan - Retail / Umum"> Penjualan - Retail / Umum </option>
                                                                                  <option value="Perawatan pribadi/Kecantikan/Pelayanan Fitnes"> Perawatan pribadi/Kecantikan/Pelayanan Fitnes </option>
                                                                                  <option value="Perbankan/Pelayanan Keuangan"> Perbankan/Pelayanan Keuangan </option>
                                                                                  <option value="Periklanan/Perencanaan Media"> Periklanan/Perencanaan Media </option>
                                                                                  <option value="Pertanian/Perhutanan/Perikanan"> Pertanian/Perhutanan/Perikanan </option>
                                                                                  <option value="Properti/Real Estate"> Properti/Real Estate </option>
                                                                                  <option value="Quality Control"> Quality Control </option>
                                                                                  <option value="Sales - Telesales / Telemarketing"> Sales - Telesales / Telemarketing </option>
                                                                                  <option value="Sekretaris/ Personal Assistant"> Sekretaris/ Personal Assistant </option>
                                                                                  <option value="Seni/Kreatif/Desain Grafis"> Seni/Kreatif/Desain Grafis </option>
                                                                                  <option value="Strategi korporasi / Top Management"> Strategi korporasi / Top Management </option>
                                                                                  <option value="Survei Kuantitas"> Survei Kuantitas </option>
                                                                                  <option value="Teknik - Elektrikal"> Teknik - Elektrikal </option>
                                                                                  <option value="Teknik - Elektronik/Komunikasi"> Teknik - Elektronik/Komunikasi </option>
                                                                                  <option value="Teknik - Industri"> Teknik - Industri </option>
                                                                                  <option value="Teknik - Kimia"> Teknik - Kimia </option>
                                                                                  <option value="Teknik - Lain-lain"> Teknik - Lain-lain </option>
                                                                                  <option value="Teknik - Lingkungan/Kesehatan/Keamanan"> Teknik - Lingkungan/Kesehatan/Keamanan </option>
                                                                                  <option value="Teknik - Minyak/Gas"> Teknik - Minyak/Gas </option>
                                                                                  <option value="Teknik - Sipil/Konstruksi/Struktural"> Teknik - Sipil/Konstruksi/Struktural </option>
                                                                                  <option value="Teknikal &amp; Helpdesk"> Teknikal &amp; Helpdesk </option>
                                                                                  <option value="Teknologi Makanan/Ahli Gizi"> Teknologi Makanan/Ahli Gizi </option>
                                                                                  <option value="Lainnya">Lainnya</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="bidang_pekerjaan" class="col-sm-3 control-label">Bidang Pekerjaan</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" name="bidang_pekerjaan" id="input" class="form-control" value="<?php echo $dt_pengalaman->bidang_pekerjaan ?>" placeholder="Bidang Pekerjaan">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="negara" class="col-sm-3 control-label">Negara</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="negara" id="inputnegara" class="form-control negara" required="required">
                                                                                <?php if (!empty($dt_pengalaman->negara)): ?>
                                                                                    <option value="<?php echo $dt_pengalaman->negara ?>" selected><?php echo $dt_pengalaman->negara ?></option>
                                                                                <?php else: ?>
                                                                                    <option value="" selected>Pilih Negara</option>
                                                                                <?php endif ?>
                                                                                
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="industri" class="col-sm-3 control-label">Industri</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="industri" id="inputindustri" class="form-control" required="required">
                                                                                <?php if (!empty($dt_pengalaman->industri)): ?>
                                                                                     <option value="<?php echo $dt_pengalaman->industri ?>" selected><?php echo $dt_pengalaman->industri ?></option>
                                                                                <?php else: ?>
                                                                                     <option value="" selected>Pilih Industri</option>
                                                                                <?php endif ?>
                                                                               
                                                                                <option value="Agrikultural / Perkebunan / Peternakan Unggas / Perikanan"> Agrikultural / Perkebunan / Peternakan Unggas / Perikanan </option>
                                                                                 <option value="Akuntansi / Audit / Pelayanan Pajak"> Akuntansi / Audit / Pelayanan Pajak </option>
                                                                                 <option value="Asuransi"> Asuransi </option>
                                                                                 <option value="Automobil / Mesin Tambahan Automotif / Kendaraan" > Automobil / Mesin Tambahan Automotif / Kendaraan </option>
                                                                                 <option value="Bahan Kimia / Pupuk / Pestisida" > Bahan Kimia / Pupuk / Pestisida </option>
                                                                                 <option value="BioTeknologi / Farmasi / Riset klinik"> BioTeknologi / Farmasi / Riset klinik </option>
                                                                                 <option value="Call Center / IT-Enabled Services / BPO"> Call Center / IT-Enabled Services / BPO </option>
                                                                                 <option value="Elektrikal &amp; Elektronik"> Elektrikal &amp; Elektronik </option>
                                                                                 <option value="Hiburan / Media"> Hiburan / Media </option>
                                                                                 <option value="Hotel / Pariwisata"> Hotel / Pariwisata </option>
                                                                                 <option value="Hukum / Legal"> Hukum / Legal </option>
                                                                                 <option value="Ilmu Pengetahuan &amp; Teknologi "> Ilmu Pengetahuan &amp; Teknologi  </option>
                                                                                 <option value="Industri Berat / Mesin / Peralatan"> Industri Berat / Mesin / Peralatan </option>
                                                                                 <option value="Jual Beli Saham / Sekuritas"> Jual Beli Saham / Sekuritas </option>
                                                                                 <option value="Jurnalisme"> Jurnalisme </option>
                                                                                 <option value="Kayu / Fiber / Kertas"> Kayu / Fiber / Kertas </option>
                                                                                 <option value="Keamanan / Penegak hukum"> Keamanan / Penegak hukum </option>
                                                                                 <option value="Kelautan / Aquakultur"> Kelautan / Aquakultur </option>
                                                                                 <option value="Kesehatan / Medis "> Kesehatan / Medis  </option>
                                                                                 <option value="Komputer / Teknik Informatika (Perangkat Keras)" > Komputer / Teknik Informatika (Perangkat Keras) </option>
                                                                                 <option value="Komputer / Teknik Informatika (Perangkat Lunak)"> Komputer / Teknik Informatika (Perangkat Lunak) </option>
                                                                                 <option value="Konstruksi / Bangunan / Teknik"> Konstruksi / Bangunan / Teknik </option>
                                                                                 <option value="Konsultasi (Bisnis &amp; Manajemen)"> Konsultasi (Bisnis &amp; Manajemen) </option>
                                                                                 <option value="Konsultasi (IT, Ilmu Pengetahuan, Teknis &amp; Teknikal)"> Konsultasi (IT, Ilmu Pengetahuan, Teknis &amp; Teknikal) </option>
                                                                                 <option value="Layanan Umum / Tenaga Penggerak"> Layanan Umum / Tenaga Penggerak </option>
                                                                                 <option value="Lingkungan / Kesehatan / Keamanan"> Lingkungan / Kesehatan / Keamanan </option>
                                                                                 <option value="Luar Angkasa / Aviasi / Pesawat Terbang" > Luar Angkasa / Aviasi / Pesawat Terbang </option>
                                                                                 <option value="Makanan &amp; Minuman / Katering / Rumah Makan"> Makanan &amp; Minuman / Katering / Rumah Makan </option>
                                                                                 <option value="Manajemen / Konsulting HR"> Manajemen / Konsulting HR </option>
                                                                                 <option value="Manufaktur / Produksi"> Manufaktur / Produksi </option>
                                                                                 <option value="Minyak / Gas / Petroleum"> Minyak / Gas / Petroleum </option>
                                                                                 <option value="Olahraga"> Olahraga </option>
                                                                                 <option value="Organisasi Nirlaba / Pelayanan Sosial / LSM"> Organisasi Nirlaba / Pelayanan Sosial / LSM </option>
                                                                                 <option value="Pakaian"> Pakaian </option>
                                                                                 <option value="Pameran / Manajemen acara / PIKP"> Pameran / Manajemen acara / PIKP </option>
                                                                                 <option value="Pelayanan Arsitek / Desain Interior"> Pelayanan Arsitek / Desain Interior </option>
                                                                                 <option value="Pelayanan Perbaikan &amp; Pemeliharaan"> Pelayanan Perbaikan &amp; Pemeliharaan </option>
                                                                                 <option value="Pemerintahan / Pertahanan"> Pemerintahan / Pertahanan </option>
                                                                                 <option value="Pendidikan "> Pendidikan  </option>
                                                                                 <option value="Perawatan / Kecantikan / Fitnes"> Perawatan / Kecantikan / Fitnes </option>
                                                                                 <option value="Perbankan / Pelayanan Keuangan"> Perbankan / Pelayanan Keuangan </option>
                                                                                 <option value="Percetakan / Penerbitan"> Percetakan / Penerbitan </option>
                                                                                 <option value="Periklanan / Marketing / Promosi / Hubungan Masyarakat"> Periklanan / Marketing / Promosi / Hubungan Masyarakat </option>
                                                                                 <option value="Permata / Perhiasan"> Permata / Perhiasan </option>
                                                                                 <option value="Perpustakaan / Museum"> Perpustakaan / Museum </option>
                                                                                 <option value="Pertambangan"> Pertambangan </option>
                                                                                 <option value="Polymer / Plastik / Karet / Ban"> Polymer / Plastik / Karet / Ban </option>
                                                                                 <option value="Produk Konsumen / Barang konsumen yang bergerak cepat"> Produk Konsumen / Barang konsumen yang bergerak cepat </option>
                                                                                 <option value="Properti / Real Estate"> Properti / Real Estate </option>
                                                                                 <option value="R&amp;D"> R&amp;D </option>
                                                                                 <option value="Retail / Merchandise"> Retail / Merchandise </option>
                                                                                 <option value="Semikonduktor / Fabrikasi"> Semikonduktor / Fabrikasi </option>
                                                                                 <option value="Seni / Desain / Fashion" > Seni / Desain / Fashion </option>
                                                                                 <option value="Tekstil  / Garment"> Tekstil  / Garment </option>
                                                                                 <option value="Telekomunikasi"> Telekomunikasi </option>
                                                                                 <option value="Tembakau"> Tembakau </option>
                                                                                 <option value="Transportasi / Logistik"> Transportasi / Logistik </option>
                                                                                 <option value="Travel / Pariwisata"> Travel / Pariwisata </option>
                                                                                 <option value="Umum &amp; Grosir"> Umum &amp; Grosir </option>
                                                                                 <option value="Lainnya">Lainnya</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="jabatan" class="col-sm-3 control-label">Jabatan</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="jabatan" id="inputJabatan" class="form-control" required="required">
                                                                                <?php if (!empty($dt_pengalaman->jabatan)): ?>
                                                                                     <option value="<?php echo $dt_pengalaman->jabatan ?>" selected><?php echo $dt_pengalaman->jabatan ?></option>
                                                                                <?php else: ?>
                                                                                     <option value="" selected>Pilih Jabatan</option>
                                                                                <?php endif ?>
                                                                               
                                                                                <option value="CEO/GM/Direktur/Manajer Senior"> CEO/GM/Direktur/Manajer Senior </option>
                                                                                <option value="Manajer/Asisten Manajer"> Manajer/Asisten Manajer </option>
                                                                                <option value="Supervisor/Koordinator"> Supervisor/Koordinator </option>
                                                                                <option value="Pegawai (non-manajemen &amp; non-supervisor)"> Pegawai (non-manajemen &amp; non-supervisor) </option>
                                                                                <option value="Lulusan baru/Pengalaman kerja kurang dari 1 tahun"> Lulusan baru/Pengalaman kerja kurang dari 1 tahun </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="exampleInputuname3" class="col-sm-3 control-label">Gaji bulanan</label>
                                                                        <div class="col-sm-3">
                                                                            <select name="mata_uang" id="inputMata_uang" class="form-control" required="required">
                                                                                <?php if (!empty($dt_pengalaman->mata_uang)): ?>
                                                                                    <option value="<?php echo $dt_pengalaman->mata_uang ?>" selected><?php echo $dt_pengalaman->mata_uang ?></option>
                                                                                <?php else: ?>
                                                                                    <option value="" selected>Pilih Mata Uang</option>
                                                                                <?php endif ?>
                                                                                 
                                                                                <option value="MYR">MYR</option>
                                                                                <option value="SGD">SGD</option>
                                                                                <option value="PHP">PHP</option>
                                                                                <option value="USD">USD</option>
                                                                                <option value="INR">INR</option>
                                                                                <option value="AUD">AUD</option>
                                                                                <option value="THB">THB</option>
                                                                                <option value="IDR">IDR</option>
                                                                                <option value="HKD">HKD</option>
                                                                                <option value="EUR">EUR</option>
                                                                                <option value="CNY">CNY</option>
                                                                                <option value="JPY">JPY</option>
                                                                                <option value="GBP">GBP</option>
                                                                                <option value="VND">VND</option>
                                                                                <option value="BDT">BDT</option>
                                                                                <option value="NZD">NZD</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="number" name="gaji" id="input" class="form-control" value="<?php echo $dt_pengalaman->gaji ?>" required="required" placeholder="Nominal Gaji" onkeypress="return isNumberKey(event);">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="keterangan_kerja" class="col-sm-3 control-label">Keterangan Kerja</label>
                                                                        <div class="col-sm-9">
                                                                            <textarea name="keterangan_kerja" id="inputInf" class="form-control" rows="7" required="required"><?php echo $dt_pengalaman->keterangan_kerja ?> </textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary">Tambah</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php endforeach ?>

                                                <div class="modal fade" id="tambah_pengalaman">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <!-- <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">Modal title</h4>
                                                            </div> -->
                                                            <form class="form-horizontal p-t-20" action="<?php echo base_url('pelamar/action_insert_data_pengalaman'); ?>" method="POST">
                                                                <div class="modal-body">
                                                                    <div class="form-group row">
                                                                        <label for="posisi_kerja" class="col-sm-3 control-label">Posisi *</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" name="posisi_kerja" id="inputPosisi" class="form-control" value="" required="required">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="nama_perusahaan" class="col-sm-3 control-label">Nama Perusahaan *</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" name="nama_perusahaan" id="inputNamaperusahaan" class="form-control" value="" required="required">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="lama_berkerja" class="col-sm-3 control-label">Lama Berkerja</label>
                                                                        <div class="col-sm-3">
                                                                            <select name="bulan_start_kerja" id="inputBulan" class="form-control" required="required">
                                                                                <option value="" selected>Bulan</option>
                                                                                <option value="Januari">Januari</option>
                                                                                <option value="Februari">Februari</option>
                                                                                <option value="Maret">Maret</option>
                                                                                <option value="April">April</option>
                                                                                <option value="Mei">Mei</option>
                                                                                <option value="Juni">Juni</option>
                                                                                <option value="Juli">Juli</option>
                                                                                <option value="Agustus">Agustus</option>
                                                                                <option value="September">September</option>
                                                                                <option value="Oktober">Oktober</option>
                                                                                <option value="November">November</option>
                                                                                <option value="Desember">Desember</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <select name="tahun_start_kerja" id="inputTahun" class="form-control" required="required">
                                                                                <option value="" selected>Tahun</option>
                                                                                <?php $year = date('Y'); $min = $year - 60; $max = $year; ?>
                                                                                <?php for ($i=$max; $i>=$min; $i-- ): ?>
                                                                                     <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                                                <?php endfor ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <label class="label-costume">sampai</label>
                                                                    <div class="form-group row">
                                                                        <label for="lama_berkerja" class="col-sm-3 control-label"></label>
                                                                        <div class="col-sm-3">
                                                                            <select name="bulan_start_selesai" id="bulan_start_selesai" class="form-control" required="required">
                                                                                <option value="" selected>Bulan</option>
                                                                                <option value="Januari">Januari</option>
                                                                                <option value="Februari">Februari</option>
                                                                                <option value="Maret">Maret</option>
                                                                                <option value="April">April</option>
                                                                                <option value="Mei">Mei</option>
                                                                                <option value="Juni">Juni</option>
                                                                                <option value="Juli">Juli</option>
                                                                                <option value="Agustus">Agustus</option>
                                                                                <option value="September">September</option>
                                                                                <option value="Oktober">Oktober</option>
                                                                                <option value="November">November</option>
                                                                                <option value="Desember">Desember</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <select name="tahun_start_selesai" id="tahun_start_selesai" class="form-control" required="required">
                                                                                <option value="" selected>Tahun</option>
                                                                                <?php $year = date('Y'); $min = $year - 60; $max = $year; ?>
                                                                                <?php for ($i=$max; $i>=$min; $i-- ): ?>
                                                                                     <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                                                <?php endfor ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="checkbox checkbox-info">
                                                                                <input type="checkbox" name="masih_kerja" id="sampai_sekarang" class="filled-in" value="sampai_sekarang">
                                                                                <label for="sampai_sekarang" class=""> Sekarang </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="spesialisasi" class="col-sm-3 control-label">Spesialisasi</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="spesialisasi" id="inputspesialisasi" class="form-control" required="required">
                                                                             <option value="" selected>Pilih Spesialisasi</option>
                                                                              <option value="Arsitek/Desain Interior"> Arsitek/Desain Interior </option>
                                                                              <option value="Bioteknologi"> Bioteknologi </option>
                                                                              <option value="Clerical/Staf Admin"> Clerical/Staf Admin </option>
                                                                              <option value="Customer Service"> Customer Service </option>
                                                                              <option value="Engineering - Mechanical/Otomotif"> Engineering - Mechanical/Otomotif </option>
                                                                              <option value="Geologi/Geofisika"> Geologi/Geofisika </option>
                                                                              <option value="Hiburan/Seni Panggung"> Hiburan/Seni Panggung </option>
                                                                              <option value="Hotel/Pariwisata"> Hotel/Pariwisata </option>
                                                                              <option value="HRD"> HRD </option>
                                                                              <option value="Hukum/Pelayanan Legal"> Hukum/Pelayanan Legal </option>
                                                                              <option value="Ilmu Komunikasi"> Ilmu Komunikasi </option>
                                                                              <option value="Ilmu Pasti /Statistik"> Ilmu Pasti /Statistik </option>
                                                                              <option value="Ilmu Sains dan Labrotarium"> Ilmu Sains dan Labrotarium </option>
                                                                              <option value="IT/Komputer - Jaringan/Sistem/Sistem Database"> IT/Komputer - Jaringan/Sistem/Sistem Database </option>
                                                                              <option value="IT/Komputer - Perangkat Keras"> IT/Komputer - Perangkat Keras </option>
                                                                              <option value="IT/Komputer - Perangkat Lunak"> IT/Komputer - Perangkat Lunak </option>
                                                                              <option value="Jurnalis/Editor"> Jurnalis/Editor </option>
                                                                              <option value="Keamanan/Angkatan Bersenjata/Proctevtive Service"> Keamanan/Angkatan Bersenjata/Proctevtive Service </option>
                                                                              <option value="Kesehatan - Dokter/Diagnnosa"> Kesehatan - Dokter/Diagnnosa </option>
                                                                              <option value="Kesehatan - Farmasi"> Kesehatan - Farmasi </option>
                                                                              <option value="Kesehatan - Suster &amp; Asisten Medis"> Kesehatan - Suster &amp; Asisten Medis </option>
                                                                              <option value="Keuangan - Audit/Pajak"> Keuangan - Audit/Pajak </option>
                                                                              <option value="Keuangan - Keuangan Perusahaan/Investasi/Perbankan Retail"> Keuangan - Keuangan Perusahaan/Investasi/Perbankan Retail </option>
                                                                              <option value="Keuangan - Umum/Akuntansi Pembiayaan"> Keuangan - Umum/Akuntansi Pembiayaan </option>
                                                                              <option value="Kimia"> Kimia </option>
                                                                              <option value="Kontrol dan Proses design"> Kontrol dan Proses design </option>
                                                                              <option value="Logistik / Supply Chain"> Logistik / Supply Chain </option>
                                                                              <option value="Makanan/Minuman/Pelayanan Restoran"> Makanan/Minuman/Pelayanan Restoran </option>
                                                                              <option value="Manufaktur/Operasi Produksi"> Manufaktur/Operasi Produksi </option>
                                                                              <option value="Merchandising"> Merchandising </option>
                                                                              <option value="Pekerjaan Umum (Pengurus Rumah,Sopir,Pengantar Barang)"> Pekerjaan Umum (Pengurus Rumah,Sopir,Pengantar Barang) </option>
                                                                              <option value="Pelatihan &amp; Pengembangan"> Pelatihan &amp; Pengembangan </option>
                                                                              <option value="Pelayanan Sosial &amp; Konseling"> Pelayanan Sosial &amp; Konseling </option>
                                                                              <option value="Pemasaran/Pengembangan Bisnis"> Pemasaran/Pengembangan Bisnis </option>
                                                                              <option value="Pembelian/Inventaris/Manajemen Barang &amp; Pergudangan"> Pembelian/Inventaris/Manajemen Barang &amp; Pergudangan </option>
                                                                              <option value="Pemeliharaan (Fasilitas &amp; Mesin)"> Pemeliharaan (Fasilitas &amp; Mesin) </option>
                                                                              <option value="Pendidikan"> Pendidikan </option>
                                                                              <option value="Penerbangan"> Penerbangan </option>
                                                                              <option value="Penerbitan/Percetakan"> Penerbitan/Percetakan </option>
                                                                              <option value="Penjualan - Corporate"> Penjualan - Corporate </option>
                                                                              <option value="Penjualan - Engineering/Teknikal/IT"> Penjualan - Engineering/Teknikal/IT </option>
                                                                              <option value="Penjualan - Keuangan ( Asuransi, Saham, Etc )"> Penjualan - Keuangan ( Asuransi, Saham, Etc ) </option>
                                                                              <option value="Penjualan - Retail / Umum"> Penjualan - Retail / Umum </option>
                                                                              <option value="Perawatan pribadi/Kecantikan/Pelayanan Fitnes"> Perawatan pribadi/Kecantikan/Pelayanan Fitnes </option>
                                                                              <option value="Perbankan/Pelayanan Keuangan"> Perbankan/Pelayanan Keuangan </option>
                                                                              <option value="Periklanan/Perencanaan Media"> Periklanan/Perencanaan Media </option>
                                                                              <option value="Pertanian/Perhutanan/Perikanan"> Pertanian/Perhutanan/Perikanan </option>
                                                                              <option value="Properti/Real Estate"> Properti/Real Estate </option>
                                                                              <option value="Quality Control"> Quality Control </option>
                                                                              <option value="Sales - Telesales / Telemarketing"> Sales - Telesales / Telemarketing </option>
                                                                              <option value="Sekretaris/ Personal Assistant"> Sekretaris/ Personal Assistant </option>
                                                                              <option value="Seni/Kreatif/Desain Grafis"> Seni/Kreatif/Desain Grafis </option>
                                                                              <option value="Strategi korporasi / Top Management"> Strategi korporasi / Top Management </option>
                                                                              <option value="Survei Kuantitas"> Survei Kuantitas </option>
                                                                              <option value="Teknik - Elektrikal"> Teknik - Elektrikal </option>
                                                                              <option value="Teknik - Elektronik/Komunikasi"> Teknik - Elektronik/Komunikasi </option>
                                                                              <option value="Teknik - Industri"> Teknik - Industri </option>
                                                                              <option value="Teknik - Kimia"> Teknik - Kimia </option>
                                                                              <option value="Teknik - Lain-lain"> Teknik - Lain-lain </option>
                                                                              <option value="Teknik - Lingkungan/Kesehatan/Keamanan"> Teknik - Lingkungan/Kesehatan/Keamanan </option>
                                                                              <option value="Teknik - Minyak/Gas"> Teknik - Minyak/Gas </option>
                                                                              <option value="Teknik - Sipil/Konstruksi/Struktural"> Teknik - Sipil/Konstruksi/Struktural </option>
                                                                              <option value="Teknikal &amp; Helpdesk"> Teknikal &amp; Helpdesk </option>
                                                                              <option value="Teknologi Makanan/Ahli Gizi"> Teknologi Makanan/Ahli Gizi </option>
                                                                              <option value="Lainnya">Lainnya</option>
                                                                        </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="bidang_pekerjaan" class="col-sm-3 control-label">Bidang Pekerjaan</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" name="bidang_pekerjaan" id="input" class="form-control" value="" placeholder="Bidang Pekerjaan">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="negara" class="col-sm-3 control-label">Negara</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="negara" id="inputnegara" class="form-control negara" required="required">
                                                                                <option value="" selected>Pilih Negara</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="industri" class="col-sm-3 control-label">Industri</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="industri" id="inputindustri" class="form-control" required="required">
                                                                                <option value="" selected>Pilih Industri</option>
                                                                                <option value="Agrikultural / Perkebunan / Peternakan Unggas / Perikanan"> Agrikultural / Perkebunan / Peternakan Unggas / Perikanan </option>
                                                                                 <option value="Akuntansi / Audit / Pelayanan Pajak"> Akuntansi / Audit / Pelayanan Pajak </option>
                                                                                 <option value="Asuransi"> Asuransi </option>
                                                                                 <option value="Automobil / Mesin Tambahan Automotif / Kendaraan" > Automobil / Mesin Tambahan Automotif / Kendaraan </option>
                                                                                 <option value="Bahan Kimia / Pupuk / Pestisida" > Bahan Kimia / Pupuk / Pestisida </option>
                                                                                 <option value="BioTeknologi / Farmasi / Riset klinik"> BioTeknologi / Farmasi / Riset klinik </option>
                                                                                 <option value="Call Center / IT-Enabled Services / BPO"> Call Center / IT-Enabled Services / BPO </option>
                                                                                 <option value="Elektrikal &amp; Elektronik"> Elektrikal &amp; Elektronik </option>
                                                                                 <option value="Hiburan / Media"> Hiburan / Media </option>
                                                                                 <option value="Hotel / Pariwisata"> Hotel / Pariwisata </option>
                                                                                 <option value="Hukum / Legal"> Hukum / Legal </option>
                                                                                 <option value="Ilmu Pengetahuan &amp; Teknologi "> Ilmu Pengetahuan &amp; Teknologi  </option>
                                                                                 <option value="Industri Berat / Mesin / Peralatan"> Industri Berat / Mesin / Peralatan </option>
                                                                                 <option value="Jual Beli Saham / Sekuritas"> Jual Beli Saham / Sekuritas </option>
                                                                                 <option value="Jurnalisme"> Jurnalisme </option>
                                                                                 <option value="Kayu / Fiber / Kertas"> Kayu / Fiber / Kertas </option>
                                                                                 <option value="Keamanan / Penegak hukum"> Keamanan / Penegak hukum </option>
                                                                                 <option value="Kelautan / Aquakultur"> Kelautan / Aquakultur </option>
                                                                                 <option value="Kesehatan / Medis "> Kesehatan / Medis  </option>
                                                                                 <option value="Komputer / Teknik Informatika (Perangkat Keras)" > Komputer / Teknik Informatika (Perangkat Keras) </option>
                                                                                 <option value="Komputer / Teknik Informatika (Perangkat Lunak)"> Komputer / Teknik Informatika (Perangkat Lunak) </option>
                                                                                 <option value="Konstruksi / Bangunan / Teknik"> Konstruksi / Bangunan / Teknik </option>
                                                                                 <option value="Konsultasi (Bisnis &amp; Manajemen)"> Konsultasi (Bisnis &amp; Manajemen) </option>
                                                                                 <option value="Konsultasi (IT, Ilmu Pengetahuan, Teknis &amp; Teknikal)"> Konsultasi (IT, Ilmu Pengetahuan, Teknis &amp; Teknikal) </option>
                                                                                 <option value="Layanan Umum / Tenaga Penggerak"> Layanan Umum / Tenaga Penggerak </option>
                                                                                 <option value="Lingkungan / Kesehatan / Keamanan"> Lingkungan / Kesehatan / Keamanan </option>
                                                                                 <option value="Luar Angkasa / Aviasi / Pesawat Terbang" > Luar Angkasa / Aviasi / Pesawat Terbang </option>
                                                                                 <option value="Makanan &amp; Minuman / Katering / Rumah Makan"> Makanan &amp; Minuman / Katering / Rumah Makan </option>
                                                                                 <option value="Manajemen / Konsulting HR"> Manajemen / Konsulting HR </option>
                                                                                 <option value="Manufaktur / Produksi"> Manufaktur / Produksi </option>
                                                                                 <option value="Minyak / Gas / Petroleum"> Minyak / Gas / Petroleum </option>
                                                                                 <option value="Olahraga"> Olahraga </option>
                                                                                 <option value="Organisasi Nirlaba / Pelayanan Sosial / LSM"> Organisasi Nirlaba / Pelayanan Sosial / LSM </option>
                                                                                 <option value="Pakaian"> Pakaian </option>
                                                                                 <option value="Pameran / Manajemen acara / PIKP"> Pameran / Manajemen acara / PIKP </option>
                                                                                 <option value="Pelayanan Arsitek / Desain Interior"> Pelayanan Arsitek / Desain Interior </option>
                                                                                 <option value="Pelayanan Perbaikan &amp; Pemeliharaan"> Pelayanan Perbaikan &amp; Pemeliharaan </option>
                                                                                 <option value="Pemerintahan / Pertahanan"> Pemerintahan / Pertahanan </option>
                                                                                 <option value="Pendidikan "> Pendidikan  </option>
                                                                                 <option value="Perawatan / Kecantikan / Fitnes"> Perawatan / Kecantikan / Fitnes </option>
                                                                                 <option value="Perbankan / Pelayanan Keuangan"> Perbankan / Pelayanan Keuangan </option>
                                                                                 <option value="Percetakan / Penerbitan"> Percetakan / Penerbitan </option>
                                                                                 <option value="Periklanan / Marketing / Promosi / Hubungan Masyarakat"> Periklanan / Marketing / Promosi / Hubungan Masyarakat </option>
                                                                                 <option value="Permata / Perhiasan"> Permata / Perhiasan </option>
                                                                                 <option value="Perpustakaan / Museum"> Perpustakaan / Museum </option>
                                                                                 <option value="Pertambangan"> Pertambangan </option>
                                                                                 <option value="Polymer / Plastik / Karet / Ban"> Polymer / Plastik / Karet / Ban </option>
                                                                                 <option value="Produk Konsumen / Barang konsumen yang bergerak cepat"> Produk Konsumen / Barang konsumen yang bergerak cepat </option>
                                                                                 <option value="Properti / Real Estate"> Properti / Real Estate </option>
                                                                                 <option value="R&amp;D"> R&amp;D </option>
                                                                                 <option value="Retail / Merchandise"> Retail / Merchandise </option>
                                                                                 <option value="Semikonduktor / Fabrikasi"> Semikonduktor / Fabrikasi </option>
                                                                                 <option value="Seni / Desain / Fashion" > Seni / Desain / Fashion </option>
                                                                                 <option value="Tekstil  / Garment"> Tekstil  / Garment </option>
                                                                                 <option value="Telekomunikasi"> Telekomunikasi </option>
                                                                                 <option value="Tembakau"> Tembakau </option>
                                                                                 <option value="Transportasi / Logistik"> Transportasi / Logistik </option>
                                                                                 <option value="Travel / Pariwisata"> Travel / Pariwisata </option>
                                                                                 <option value="Umum &amp; Grosir"> Umum &amp; Grosir </option>
                                                                                 <option value="Lainnya">Lainnya</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="jabatan" class="col-sm-3 control-label">Jabatan</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="jabatan" id="inputJabatan" class="form-control" required="required">
                                                                                <option value="" selected>Pilih Jabatan</option>
                                                                                <option value="CEO/GM/Direktur/Manajer Senior"> CEO/GM/Direktur/Manajer Senior </option>
                                                                                <option value="Manajer/Asisten Manajer"> Manajer/Asisten Manajer </option>
                                                                                <option value="Supervisor/Koordinator"> Supervisor/Koordinator </option>
                                                                                <option value="Pegawai (non-manajemen &amp; non-supervisor)"> Pegawai (non-manajemen &amp; non-supervisor) </option>
                                                                                <option value="Lulusan baru/Pengalaman kerja kurang dari 1 tahun"> Lulusan baru/Pengalaman kerja kurang dari 1 tahun </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="exampleInputuname3" class="col-sm-3 control-label">Gaji bulanan</label>
                                                                        <div class="col-sm-3">
                                                                            <select name="mata_uang" id="inputMata_uang" class="form-control" required="required">
                                                                                <option value="">Mata Uang</option>
                                                                                <option value="IDR" selected>IDR</option>
                                                                                <option value="MYR">MYR</option>
                                                                                <option value="SGD">SGD</option>
                                                                                <option value="PHP">PHP</option>
                                                                                <option value="USD">USD</option>
                                                                                <option value="INR">INR</option>
                                                                                <option value="AUD">AUD</option>
                                                                                <option value="THB">THB</option>
                                                                                <option value="HKD">HKD</option>
                                                                                <option value="EUR">EUR</option>
                                                                                <option value="CNY">CNY</option>
                                                                                <option value="JPY">JPY</option>
                                                                                <option value="GBP">GBP</option>
                                                                                <option value="VND">VND</option>
                                                                                <option value="BDT">BDT</option>
                                                                                <option value="NZD">NZD</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="number" name="gaji" id="input" class="form-control" value="" required="required" placeholder="Nominal Gaji" onkeypress="return isNumberKey(event);">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="keterangan_kerja" class="col-sm-3 control-label">Keterangan Kerja</label>
                                                                        <div class="col-sm-9">
                                                                            <textarea name="keterangan_kerja" id="inputInf" class="form-control" rows="7" required="required"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary">Tambah</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                               </div>

                                <div class="tab-pane " id="pendidikan" role="tabpanel">
                                    <div class="card-body">
                                        <?php if (count($data_pendidikan) == 2): ?>
                                            <h3><i class="fa fa-graduation-cap"></i> Pendidikan</h3>
                                        <?php else: ?>
                                            <h3><i class="fa fa-graduation-cap"></i> Pendidikan <div class="pull-right hidden-xs"><a data-toggle="modal" href='#tambah_pendidikan'><button class="btn btn-xs btn-circle btn-info"> <i class="fa fa-plus"></i> </button></a></div></h3>
                                        <?php endif ?>
                                        

                                        <div class="card">
                                            <div class="card-body">
                                                <h6 class="card-subtitle">Tambahkan 2 jenjang pendidikan yang tertinggi</h6> 
                                                <a class="btn btn-block btn-info hidden-lg" data-toggle="modal" href='#tambah_pendidikan' role="button"><i class="fa fa-plus"></i> Tambah Pendidikan</a>
                                            </div>

                                            <!-- ============================================================== -->
                                            <!-- Comment widgets -->
                                            <!-- ============================================================== -->
                                            <div class="row">
                                                <?php foreach ($data_pendidikan as $dt_pendidikan): ?>
                                                    
                                                
                                                <div class="col-xs-12 col-lg-3">
                                                    <div class="p-2">
                                                        <p><?php echo $dt_pendidikan->bulan_wisuda ?> , <?php echo $dt_pendidikan->tahun_lulusan ?></p>
                                                    </div>
                                                   
                                                </div>

                                                <div class="col-xs-12 col-lg-9">
                                                     <h5><?php echo $dt_pendidikan->nama_universitas ?></h5>
                                                    <div class="comment-footer">
                                                        <span class="date"><?php echo $dt_pendidikan->kualifikasi ?> di <?php echo $dt_pendidikan->jurusan ?> | <?php echo $dt_pendidikan->lokasi ?></span>
                                                        <!-- <span class="action-icons"> -->
                                                            <a data-toggle="modal" href='#edit_data_pendidikan<?php echo $dt_pendidikan->kode_pendidikan ?>'><i class="mdi mdi-pencil-circle"></i></a>
                                                            <!-- <a href="<?php echo base_url('pelamar/admin/remove-pendidikan/'.$dt_pendidikan->kode_pendidikan) ?>"><i class="fa fa-trash"></i></a> -->   
                                                            <a id="hapus_pendidikan" href="<?php echo base_url('pelamar/admin/remove-pendidikan/'.$dt_pendidikan->kode_pendidikan) ?>"><i class="fa fa-trash"></i></a>   
                                                        <!-- </span> -->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-4 col-sm-4">
                                                            <strong class="font-sm">Jurusan </strong> 
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8">
                                                            <?php echo $dt_pendidikan->jurusan ?>
                                                        </div>

                                                        <?php if ($dt_pendidikan->grade == "IPK"): ?>
                                                            <div class="col-xs-4 col-sm-4">
                                                                <strong class="font-sm"><?php echo $dt_pendidikan->grade ?> </strong> 
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8">
                                                               <?php echo $dt_pendidikan->nilai_asli ?> / <?php echo $dt_pendidikan->nilai_dari ?>
                                                            </div>
                                                        <?php else: ?>
                                                            <div class="col-xs-4 col-sm-4">
                                                                <strong class="font-sm">Hasil Studi </strong> 
                                                            </div>
                                                            <div class="col-xs-8 col-sm-8">
                                                               <?php if ($dt_pendidikan->grade == "Masih Belajar"): ?>
                                                                    <span class="label label-info"><?php echo $dt_pendidikan->grade ?></span> 
                                                                <?php else: ?>
                                                                    <span class="label label-danger"><?php echo $dt_pendidikan->grade ?></span> 
                                                                <?php endif ?>
                                                            </div>
                                                        <?php endif ?>

                                                    </div>

                                                     <p class="m-b-5 m-t-10"><?php echo $dt_pendidikan->informasi_tambahan ?></p>
                                                    <hr>
                                                </div>

                                                <div class="modal fade" id="edit_data_pendidikan<?php echo $dt_pendidikan->kode_pendidikan ?>">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title"><?php echo $dt_pendidikan->nama_universitas ?></h4>
                                                            </div>
                                                            <form class="form-horizontal p-t-20" action="<?php echo base_url('pelamar/action_update_data_pendidikan'); ?>" method="POST">
                                                                <div class="modal-body">
                                                                    <div class="form-group row">
                                                                        <label for="nama_universitas" class="col-sm-3 control-label">Institusi/Universitas *</label>
                                                                        <input type="hidden" name="kode_pendidikan" id="inputKode_pen" class="form-control" value="<?php echo $dt_pendidikan->kode_pendidikan ?>">
                                                                        <div class="col-sm-9">
                                                                            <input type="text" name="nama_universitas" id="inputNamaUniversitas" class="form-control" value="<?php echo $dt_pendidikan->nama_universitas ?>" required="required">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="tgl_wisauda" class="col-sm-3 control-label">Bulan Wisuda</label>
                                                                        <div class="col-sm-3">
                                                                            <select name="bulan_wisuda" id="inputBulan" class="form-control" required="required">
                                                                                <?php if (!empty($dt_pendidikan->bulan_wisuda)): ?>
                                                                                    <option value="<?php echo $dt_pendidikan->bulan_wisuda ?>" selected><?php echo $dt_pendidikan->bulan_wisuda ?></option>
                                                                                <?php else: ?>
                                                                                    <option value="" selected>Bulan</option>
                                                                                <?php endif ?>
                                                                                
                                                                                <option value="Januari">Januari</option>
                                                                                <option value="Februari">Februari</option>
                                                                                <option value="Maret">Maret</option>
                                                                                <option value="April">April</option>
                                                                                <option value="Mei">Mei</option>
                                                                                <option value="Juni">Juni</option>
                                                                                <option value="Juli">Juli</option>
                                                                                <option value="Agustus">Agustus</option>
                                                                                <option value="September">September</option>
                                                                                <option value="Oktober">Oktober</option>
                                                                                <option value="November">November</option>
                                                                                <option value="Desember">Desember</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="number" name="tahun_lulusan" id="inputTahun" class="form-control" value="<?php echo $dt_pendidikan->tahun_lulusan ?>" placeholder="Tahun" onkeypress="return isNumberKey(event);">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="kualifikasi" class="col-sm-3 control-label">Kualifikasi</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="kualifikasi" id="inputKualifikasi" class="form-control" required="required">
                                                                                <?php if (!empty($dt_pendidikan->kualifikasi)): ?>
                                                                                    <option value="<?php echo $dt_pendidikan->kualifikasi ?>" selected><?php echo $dt_pendidikan->kualifikasi ?></option>
                                                                                <?php else: ?>
                                                                                    <option value="" selected>Pilih Kualifikasi</option>
                                                                                <?php endif ?>
                                                                                <option value="SMU/SMK/STM">SMU/SMK/STM</option>
                                                                                <option value="Diploma(D3)">Diploma (D3)</option>
                                                                                <option value="Sarjana(S1)">Sarjana (S1)</option>
                                                                                <option value="Magister(S2)">Magister (S2)</option>
                                                                                <option value="Doktor(S3)">Doktor (S3)</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="lokasi" class="col-sm-3 control-label">lokasi</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="lokasi" id="inputNegara" class="form-control .negara" required="required">
                                                                                <?php if (!empty($dt_pendidikan->lokasi)): ?>
                                                                                    <option value="<?php echo $dt_pendidikan->lokasi ?>" selected><?php echo $dt_pendidikan->lokasi ?></option>
                                                                                <?php else: ?>
                                                                                    <option value="" selected>Pilih lokasi</option>
                                                                                <?php endif ?>
                                                                                
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="jurusan" class="col-sm-3 control-label">Jurusan</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" name="jurusan" id="inputNamaUniversitas" class="form-control" value="<?php echo $dt_pendidikan->jurusan ?>" required="required">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="grade" class="col-sm-3 control-label">Nilai Akhir</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="grade" id="inputNilaiakhir" class="nilai_akhir_update form-control" required="required">
                                                                                <option value="" selected>Nilai Akhir</option>
                                                                                <option <?php echo $dt_pendidikan->grade == 'IPK' ? 'selected = "selected"': ''; ?> value="IPK">IPK</option>
                                                                                <option <?php echo $dt_pendidikan->grade == 'Selesai' ? 'selected = "selected"': ''; ?> value="Selesai">Selesai</option>
                                                                                <option <?php echo $dt_pendidikan->grade == 'Tidak Selesai' ? 'selected = "selected"': ''; ?> value="Tidak Selesai">Tidak Selesai</option>
                                                                                <option <?php echo $dt_pendidikan->grade == 'Masih Belajar' ? 'selected = "selected"': ''; ?> value="Masih Belajar">Masih Belajar</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="form-group row skor_nilai_update">
                                                                        <label for="skor" class="col-sm-3 control-label">Skor</label>
                                                                        <div class="col-sm-3">
                                                                            <input type="number" name="nilai_asli" id="input" class="form-control" value="<?php echo $dt_pendidikan->nilai_asli ?>" onkeypress="return isNumberKey(event);">
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="number" name="nilai_dari" id="input" class="form-control" value="<?php echo $dt_pendidikan->nilai_dari ?>" onkeypress="return isNumberKey(event);">
                                                                        </div>
                                                                    </div>
                                                                    

                                                                    <div class="form-group row">
                                                                        <label for="informasi_tambahan" class="col-sm-3 control-label">Informasi Tambahan</label>
                                                                        <div class="col-sm-9">
                                                                            <textarea name="informasi_tambahan" id="inputInf" class="form-control" rows="7" required="required">
                                                                                <?php echo $dt_pendidikan->informasi_tambahan ?>
                                                                            </textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary">Perbaharui</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php endforeach ?>

                                                <div class="modal fade" id="tambah_pendidikan">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <form class="form-horizontal p-t-20" action="<?php echo base_url('pelamar/action_insert_data_pendidikan'); ?>" method="POST">
                                                                <div class="modal-body">
                                                                    <div class="form-group row">
                                                                        <label for="nama_universitas" class="col-sm-3 control-label">Institusi/Universitas *</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" name="nama_universitas" id="inputNamaUniversitas" class="form-control" value="" required="required">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="tgl_wisauda" class="col-sm-3 control-label">Bulan Wisuda</label>
                                                                        <div class="col-sm-3">
                                                                            <select name="bulan_wisuda" id="inputBulan" class="form-control" required="required">
                                                                                <option value="" selected>Bulan</option>
                                                                                <option value="Januari">Januari</option>
                                                                                <option value="Februari">Februari</option>
                                                                                <option value="Maret">Maret</option>
                                                                                <option value="April">April</option>
                                                                                <option value="Mei">Mei</option>
                                                                                <option value="Juni">Juni</option>
                                                                                <option value="Juli">Juli</option>
                                                                                <option value="Agustus">Agustus</option>
                                                                                <option value="September">September</option>
                                                                                <option value="Oktober">Oktober</option>
                                                                                <option value="November">November</option>
                                                                                <option value="Desember">Desember</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="number" name="tahun_lulusan" id="inputTahun" class="form-control" onkeypress="return isNumberKey(event);" value="" placeholder="Tahun">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="kualifikasi" class="col-sm-3 control-label">Kualifikasi</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="kualifikasi" id="inputKualifikasi" class="form-control" required="required">
                                                                                <option value="" selected>Pilih Kualifikasi</option>
                                                                                <option value="SMU/SMK/STM">SMU/SMK/STM</option>
                                                                                <option value="Diploma(D3)">Diploma (D3)</option>
                                                                                <option value="Sarjana(S1)">Sarjana (S1)</option>
                                                                                <option value="Magister(S2)">Magister (S2)</option>
                                                                                <option value="Doktor(S3)">Doktor (S3)</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="lokasi" class="col-sm-3 control-label">lokasi</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="lokasi" id="inputNegara" class="form-control .negara" required="required">
                                                                                <option value="" selected>Pilih lokasi</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="jurusan" class="col-sm-3 control-label">Jurusan</label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" name="jurusan" id="inputNamaUniversitas" class="form-control" value="" required="required">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="grade" class="col-sm-3 control-label">Nilai Akhir</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="grade" id="inputNilaiakhir" class="nilai_akhir form-control" required="required">
                                                                                <option value="" selected>Nilai Akhir</option>
                                                                                <option value="IPK">IPK</option>
                                                                                <option value="Selesai">Selesai</option>
                                                                                <option value="Tidak Selesai">Tidak Selesai</option>
                                                                                <option value="Masih Belajar">Masih Belajar</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row skor_nilai">
                                                                        <label for="skor" class="col-sm-3 control-label">Skor</label>
                                                                        <div class="col-sm-3">
                                                                            <input type="number" name="nilai_asli" id="input" class="form-control" value="" onkeypress="return isNumberKey(event);">
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="number" name="nilai_dari" id="input" class="form-control" value="" onkeypress="return isNumberKey(event);">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="informasi_tambahan" class="col-sm-3 control-label">Informasi Tambahan</label>
                                                                        <div class="col-sm-9">
                                                                            <textarea name="informasi_tambahan" id="inputInf" class="form-control" rows="7" required="required"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary">Tambah</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>

                                        

                                    </div>
                                </div>

                                <div class="tab-pane " id="keterampilan" role="tabpanel">
                                    <div class="card-body">
                                        <h3><i class="fa fa-code"></i> Keterampilan</h3>
                                        <form action="<?php echo base_url('pelamar/action_update_keterampilan_skill'); ?>" method="POST" accept-charset="utf-8">

                                            <?php if (!empty($data_keterampilan)): ?>
                                                
                                                <div class="form-group row">
                                                    <label for="keterampilan" class="col-sm-3 control-label">Keterampilan</label>
                                                    <?php foreach ($data_keterampilan as $dt_keterampilan): ?>
                                                    <div class="col-sm-9">
                                                       <div class="tags-default">
                                                            <input type="text" name='keterampilan_skill' class="form-control" value="<?php echo $dt_keterampilan->keterampilan ?>" data-role="tagsinput" placeholder="Tambah keterampilan" />
                                                        </div>
                                                    </div>
                                                    <?php endforeach ?>
                                                </div>
                                                
                                            <?php else: ?>
                                                <div class="form-group row">
                                                    <label for="keterampilan" class="col-sm-3 control-label">Keterampilan</label>
                                                    <div class="col-sm-9">
                                                       <div class="tags-default">
                                                            <input type="text" name='keterampilan_skill' class="form-control" value="" data-role="tagsinput" placeholder="Tambah keterampilan" />
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif ?>

                                            <div class="col-sm-9">
                                               <div class="tags-default">
                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                </div>
                                            </div>
                                            
                                        </form>

                                    </div>
                                </div>

                                <div class="tab-pane" id="bahasa" role="tabpanel">
                                    <div class="card-body">
                                        <h3><i class="fa fa-comments-o"></i> Bahasa</h3>
                                        <form action="<?php echo base_url('pelamar/action_update_keterampilan_bahasa') ?>" method="POST" accept-charset="utf-8">
                                            
                                            <div class="form-group row">
                                                <label for="bahasa" class="col-sm-3 control-label">Bahasa</label>
                                                <div class="col-sm-9">
                                                <?php if (!empty($data_keterampilan)): ?>
                                                    <?php foreach ($data_keterampilan as $dt_keterampilan): ?>
                                                        <div class="tags-default">
                                                            <input type="text" name="bahasa" class="form-control" value="<?php echo $dt_keterampilan->kuasai_bahasa ?>" data-role="tagsinput" placeholder="Tambah Bahasa" />
                                                        </div>
                                                      <?php endforeach ?>
                                                <?php else: ?>
                                                    <div class="tags-default">
                                                        <input type="text" name="bahasa" class="form-control" value="" data-role="tagsinput" placeholder="Tambah Bahasa" />
                                                        <br>
                                                        <label class="text-danger"><i><span>pisahkan dengan tanda coma (,)</span></i></label>
                                                    </div>
                                                <?php endif ?>
                                                  
                                                </div>
                                                <div class="col-sm-9">
                                                    <br />
                                                    <br />
                                                   <button type="submit" class="btn btn-primary">Update</button>
                                                </div>
                                            </div>

                                        </form>


                                    </div>
                                </div>

                                <div class="tab-pane " id="infolain" role="tabpanel">
                                    <div class="card-body">
                                        <h3><i class="fa fa-align-justify"></i> Info Gaji
                                            <div class="pull-right hidden-xs">
                                                <a data-toggle="modal" href='#modal_info_lain'><button class="btn btn-sm btn-info"><i class="fa fa-pencil"></i> Ubah Data </button></a>
                                            </div>
                       <a class="btn btn-block btn-info hidden-lg" data-toggle="modal" href='#modal_info_lain' role="button"><i class="fa fa-pencil"></i> Ubah Data</a>
                                        </h3>
                                        <?php if (!empty($data_info)): ?>
                                            
                                            <?php foreach ($data_info as $dt_info): ?>
                                            
                                                <table class="table table-hover">
                                                    <tbody>
                                                        <tr>
                                                            <td>Gaji yang diharapkan</td>
                                                            <td><?php echo $dt_info->mata_uang .' - '. number_format($dt_info->gaji); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Lokasi kerja yang diharapkan</td>
                                                            <td><?php echo $dt_info->lokasi_kerja_1 .", ". $dt_info->lokasi_kerja_2 .", ". $dt_info->lokasi_kerja_3?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Informasi lainnya</td>
                                                            <td><?php echo $dt_info->informasi_lainnya ?> </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                                <div class="modal fade" id="modal_info_lain">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title"></h4>
                                                            </div>
                                                             <form class="form-horizontal p-t-20"  action="<?php echo base_url('pelamar/action_update_info_lainnya'); ?>" method="POST">
                                                                <div class="modal-body">
                                                                        <div class="form-group row">
                                                                            <label for="exampleInputuname3" class="col-sm-3 control-label">Gaji bulanan yang diharapkan</label>
                                                                            <div class="col-sm-3">
                                                                                <select name="mata_uang" id="inputMata_uang" class="form-control" required="required">
                                                                                    <?php if (!empty($dt_info->mata_uang)): ?>
                                                                                        <option value="<?php echo $dt_info->mata_uang ?>" selected><?php echo $dt_info->mata_uang ?></option>
                                                                                    <?php else: ?>
                                                                                        <option value="IDR" selected>IDR</option>
                                                                                    <?php endif ?>
                                                                                    <option value=""></option>
                                                                                    <option value="MYR">MYR</option>
                                                                                    <option value="SGD">SGD</option>
                                                                                    <option value="PHP">PHP</option>
                                                                                    <option value="USD">USD</option>
                                                                                    <option value="INR">INR</option>
                                                                                    <option value="AUD">AUD</option>
                                                                                    <option value="THB">THB</option>
                                                                                    <option value="HKD">HKD</option>
                                                                                    <option value="EUR">EUR</option>
                                                                                    <option value="CNY">CNY</option>
                                                                                    <option value="JPY">JPY</option>
                                                                                    <option value="GBP">GBP</option>
                                                                                    <option value="VND">VND</option>
                                                                                    <option value="BDT">BDT</option>
                                                                                    <option value="NZD">NZD</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <input type="number" name="gaji" id="input" class="form-control" value="<?php echo $dt_info->gaji?>" placeholder="Nominal Gaji" onkeypress="return isNumberKey(event);">
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group row">
                                                                            <label for="lokasi_kerja" class="col-sm-3 control-label">Lokasi kerja yang diharapkan <label class="text-danger">*</label></label>
                                                                            <div class="col-sm-9">
                                                                                <select name="lokasi_kerja1" id="" class="propinsi form-control">
                                                                                    <?php if (!empty($dt_info->lokasi_kerja_1)): ?>
                                                                                        <option value="<?php echo $dt_info->lokasi_kerja_1 ?>" selected><?php echo $dt_info->lokasi_kerja_1 ?></option>
                                                                                    <?php else: ?>
                                                                                        <option value="" selected>Lokasi 1</option>
                                                                                    <?php endif ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label for="lokasi_kerja" class="col-sm-3 control-label"></label>
                                                                            <div class="col-sm-9">
                                                                                <select name="lokasi_kerja2" id="" class="propinsi form-control">
                                                                                    <?php if (!empty($dt_info->lokasi_kerja_2)): ?>
                                                                                        <option value="<?php echo $dt_info->lokasi_kerja_2 ?>" selected><?php echo $dt_info->lokasi_kerja_2 ?></option>
                                                                                    <?php else: ?>
                                                                                        <option value="" selected>Lokasi 2</option>
                                                                                    <?php endif ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label for="lokasi_kerja" class="col-sm-3 control-label"></label>
                                                                            <div class="col-sm-9">
                                                                                <select name="lokasi_kerja3" id="" class="propinsi form-control">
                                                                                    <?php if (!empty($dt_info->lokasi_kerja_3)): ?>
                                                                                        <option value="<?php echo $dt_info->lokasi_kerja_3 ?>" selected><?php echo $dt_info->lokasi_kerja_3 ?></option>
                                                                                    <?php else: ?>
                                                                                        <option value="" selected>Lokasi 3</option>
                                                                                    <?php endif ?>
                                                                                    
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label for="informasi_lainnya" class="col-sm-3 control-label">Informasi Lainnya</label>
                                                                            <div class="col-sm-9">
                                                                                <textarea name="informasi_lainnya" id="inputInf" class="form-control" rows="7" > <?php echo $dt_info->informasi_lainnya; ?> </textarea>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php endforeach ?>

                                        <?php else: ?>
                                            
                                            <table class="table table-hover">
                                                <tbody>
                                                    <tr>
                                                        <td>Gaji yang diharapkan</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lokasi kerja yang diharapkan</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Informasi lainnya</td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <div class="modal fade" id="modal_info_lain">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title"></h4>
                                                        </div>
                                                         <form class="form-horizontal p-t-20"  action="<?php echo base_url('pelamar/action_update_info_lainnya'); ?>" method="POST">
                                                            <div class="modal-body">
                                                                    <div class="form-group row">
                                                                        <label for="exampleInputuname3" class="col-sm-3 control-label">Gaji bulanan yang diharapkan</label>
                                                                        <div class="col-sm-3">
                                                                            <select name="mata_uang" id="inputMata_uang" class="form-control" required="required">
                                                                                <option value=""></option>
                                                                                <option value="MYR">MYR</option>
                                                                                <option value="SGD">SGD</option>
                                                                                <option value="PHP">PHP</option>
                                                                                <option value="USD">USD</option>
                                                                                <option value="INR">INR</option>
                                                                                <option value="AUD">AUD</option>
                                                                                <option value="IDR" selected>IDR</option>
                                                                                <option value="THB">THB</option>
                                                                                <option value="HKD">HKD</option>
                                                                                <option value="EUR">EUR</option>
                                                                                <option value="CNY">CNY</option>
                                                                                <option value="JPY">JPY</option>
                                                                                <option value="GBP">GBP</option>
                                                                                <option value="VND">VND</option>
                                                                                <option value="BDT">BDT</option>
                                                                                <option value="NZD">NZD</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="number" name="gaji" id="input" class="form-control" value="" placeholder="Nominal Gaji" onkeypress="return isNumberKey(event);">
                                                                            <span class="font-13 ">Example : 3000000 (nominal angka)</span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="lokasi_kerja" class="col-sm-3 control-label">Lokasi kerja yang diharapkan *</label>
                                                                        <div class="col-sm-9">
                                                                            <select name="lokasi_kerja1" id="" class="propinsi form-control">
                                                                                <option value="" selected>Lokasi 1</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="lokasi_kerja" class="col-sm-3 control-label"></label>
                                                                        <div class="col-sm-9">
                                                                            <select name="lokasi_kerja2" id="" class="propinsi form-control">
                                                                                <option value="" selected>Lokasi 2</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="lokasi_kerja" class="col-sm-3 control-label"></label>
                                                                        <div class="col-sm-9">
                                                                            <select name="lokasi_kerja3" id="" class="propinsi form-control">
                                                                                <option value="" selected>Lokasi 3</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="informasi_lainnya" class="col-sm-3 control-label">Informasi Lainnya</label>
                                                                        <div class="col-sm-9">
                                                                            <textarea name="informasi_lainnya" id="inputInf" class="form-control" rows="7" ></textarea>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endif ?>
                                        
                                    </div>
                                </div>

                                <div class="tab-pane" id="profilsaya" role="tabpanel">
                                    <?php foreach ($data_profil as $dt_profile): ?>
                                        
                                        <div class="card-body">
                                            <h3><i class="fa fa-user"></i> Profil Saya
                                                <div class="pull-right hidden-xs">
                                                    <a data-toggle="modal" href='#modal-id'><button class="btn btn-sm btn-info"><i class="fa fa-pencil"></i> Ubah Profil </button></a>
                                                </div>
                        <a class="btn btn-block btn-info hidden-lg" data-toggle="modal" href='#modal-id' role="button"><i class="fa fa-pencil"></i> Ubah Profil</a>
                                            </h3>
                                            <table class="table table-hover">
                                                <tbody>
                                                    <tr>
                                                        <td>Nama</td>
                                                        <td><?php echo $dt_profile->nama_pelamar ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jenis Kelamin</td>
                                                        <td>
                                                            <?php if (!empty($dt_profile->jenis_kelamin)): ?>
                                                                <?php echo $dt_profile->jenis_kelamin ?>
                                                            <?php else: ?> <i>Not Set</i> <?php endif ?>
                                                        </td>
                                                    </tr> 
                                                    <tr>
                                                        <td>Nomor Telepon</td>
                                                        <td>
                                                            <?php if (!empty($dt_profile->nomor_telepon)): ?>
                                                                <?php echo $dt_profile->nomor_telepon ?>
                                                            <?php else: ?><i>Not Set</i><?php endif ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Email</td>
                                                        <td>
                                                            <?php if (!empty($dt_profile->alamat_email)): ?>
                                                                <?php echo $dt_profile->alamat_email ?>
                                                            <?php else: ?>
                                                                <i>Not Set</i>
                                                            <?php endif ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Alamat 1</td>
                                                        <td>
                                                            <?php if (!empty($dt_profile->alamat_1)): ?>
                                                                <?php echo $dt_profile->alamat_1 ?>
                                                            <?php else: ?> <i>Not set</i> <?php endif ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Alamat 2</td>
                                                        <td>
                                                            <?php if (!empty($dt_profile->alamat_2)): ?>
                                                                <?php echo $dt_profile->alamat_2 ?>
                                                            <?php else: ?>
                                                                <i>Not set</i>
                                                            <?php endif ?>
                                                        </td>
                                                    </tr>
                                                    <?php if (!empty($dt_profile->kota) && !empty($dt_profile->kode_pos)): ?>
                                                        <tr>
                                                            <td>Kota / Kode Pos</td>
                                                            <td><?php echo $dt_profile->kota ?>  <?php echo $dt_profile->kode_pos ?></td>
                                                        </tr>
                                                    <?php endif ?>
                                                    
                                                    <tr>
                                                        <td>Propinsi</td>
                                                        <td><?php echo $dt_profile->provinsi ?> </td>
                                                    </tr>
                                                    <!-- <tr>
                                                        <td>Negara</td>
                                                        <td><?php echo $dt_profile->negara ?></td>
                                                    </tr> -->
                                                     <tr>
                                                        <td>Tanggal Lahir</td>
                                                        <td>
                                                            <?php echo $dt_profile->tanggal.' '. $dt_profile->bulan.' '. $dt_profile->tahun ?>
                                                        </td>
                                                    </tr>
                                                    <!--  <tr>
                                                        <td>Warga Negara</td>
                                                        <td>
                                                            <?php if (!empty($dt_profile->warga_negara)): ?>
                                                                <?php echo $dt_profile->warga_negara ?>
                                                            <?php else: ?><?php endif ?>
                                                        </td>
                                                    </tr> -->

                                                    <?php if (!empty($dt_profile->file_resume)): ?>
                                                        <tr>
                                                            <td>File Resume</td>
                                                            <td> <a href="<?php echo base_url('assets_storage/file_resume/'.$dt_profile->file_resume) ?>" target="_blank"><i class="fa fa-eye"></i> Lihat Resume</a> 
                                                            <a id="hapus_resume" href="<?php echo base_url('pelamar/admin/remove-resume/'.$dt_profile->file_resume); ?>" title="" class="text-danger pull-right"><i class="fa fa-trash"></i><span class="hidden-xs">Hapus Resume</span></a></td>
                                                        </tr>
                                                    <?php endif ?>

                                                    <?php if (!empty($dt_profile->identitas)): ?>
                                                        <tr>
                                                            <td>Identitas</td>
                                                            <td><?php echo $dt_profile->identitas ?></td>
                                                        </tr>
                                                    <?php endif ?>
                                                    
                                                    <?php if (!empty($dt_profile->nomor_identitas)): ?>
                                                        <tr>
                                                            <td>Nomor Identitas</td>
                                                            <td><?php echo $dt_profile->nomor_identitas ?></td>
                                                        </tr>
                                                    <?php endif ?>
                                                    
                                                    
                                                </tbody>
                                            </table>
                                        </div>


                                        <div class="modal fade" id="modal-id">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title"><?php echo $dt_profile->nama_pelamar ?></h4>
                                                    </div>
                                                    <form action="<?php echo base_url('pelamar/action_update_profile'); ?>" method="POST">
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="nama_pelamar">Nama <label class="text-danger">*</label></label>
                                                                <input type="text" class="form-control" name="nama_pelamar" value="<?php echo $dt_profile->nama_pelamar ?>" id="nama_pelamar" required placeholder="Nama">
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6">
                                                                    <label for="tanggal_lahir">Tanggal Lahir <label class="text-danger">*</label></label>
                                                                    <div class="row" style="margin-top: -7px;">
                                                                        <div class="col-md-3 col-lg-3">
                                                                            <div class="form-group">
                                                                                <select name="tanggal" id="input" class="form-control" required="required">
                                                                                    <?php if (!empty($dt_profile->tanggal)): ?>
                                                                                        <option value="<?php echo $dt_profile->tanggal ?>" selected><?php echo $dt_profile->tanggal ?></option>
                                                                                    <?php else: ?>
                                                                                        <option value="" selected>Hari</option>
                                                                                    <?php endif ?>
                                                                                    
                                                                                    <?php for ($i=1; $i <= 31; $i++): ?>
                                                                                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                                                    <?php endfor ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-5 col-lg-5">
                                                                            <div class="form-group">
                                                                                <select name="bulan" id="input" class="form-control" required="required">
                                                                                    <?php if (!empty($dt_profile->bulan)): ?>
                                                                                        <option value="<?php echo $dt_profile->bulan ?>" selected><?php echo $dt_profile->bulan ?></option>
                                                                                    <?php else: ?>
                                                                                        <option value="" selected>Bulan</option>
                                                                                    <?php endif ?>
                                                                                    <option value="Januari">Januari</option>
                                                                                    <option value="Februari">Februari</option>
                                                                                    <option value="Maret">Maret</option>
                                                                                    <option value="April">April</option>
                                                                                    <option value="Mei">Mei</option>
                                                                                    <option value="Juni">Juni</option>
                                                                                    <option value="Juli">Juli</option>
                                                                                    <option value="Agustus">Agustus</option>
                                                                                    <option value="September">September</option>
                                                                                    <option value="Oktober">Oktober</option>
                                                                                    <option value="November">November</option>
                                                                                    <option value="Desember">Desember</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-lg-4">
                                                                            <div class="form-group">
                                                                                <input type="number" name="tahun" id="inputTahun" class="form-control" value="<?php echo $dt_profile->tahun ?>" placeholder="Tahun" onkeypress="return isNumberKey(event);">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="col-md-6 col-lg-6">
                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">Jenis Kelamin <label class="text-danger">*</label></label>
                                                                        <select name="jenis_kelamin" id="inputJenis_Kelamin" class="form-control" required="required">
                                                                            <?php if (empty($dt_profile->jenis_kelamin)): ?>
                                                                                <option value="" selected>Jenis Kelamin</option>
                                                                            <?php endif ?>
                                                                            <option <?php echo $dt_profile->jenis_kelamin == 'Pria' ? 'selected = "selected"': ''; ?> value="Pria">Pria</option>
                                                                            <option <?php echo $dt_profile->jenis_kelamin == 'Wanita' ? 'selected = "selected"': ''; ?> value="Wanita">Wanita</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="nomor_telepon">Nomor Ponsel <label class="text-danger">*</label></label>
                                                                <input type="number" name="nomor_telepon" class="form-control" id="nomor_telepon" placeholder="Nomor Ponsel" value="<?php echo $dt_profile->nomor_telepon ?>" onkeypress="return isNumberKey(event);" required>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6">
                                                                    <label for="alamat1">Alamat Sekarang <label class="text-danger">*</label></label>
                                                                    <div class="form-group">
                                                                        <input type="text" value="<?php echo $dt_profile->alamat_1 ?>" name="alamat1" class="form-control" id="alamat1" placeholder="Alamat 1" required>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-lg-6">
                                                                    <label for="alamat2"></label>
                                                                    <div class="form-group">
                                                                        <input type="text" value="<?php echo $dt_profile->alamat_2 ?>" name="alamat2" class="form-control" id="alamat2" placeholder="Alamat 2" style="margin-top: 8px;">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6">
                                                                    <label for="kota">Kota <label class="text-danger">*</label></label>
                                                                    <div class="form-group">
                                                                        <input type="text" value="<?php echo $dt_profile->kota ?>" name="kota" class="form-control" id="kota" placeholder="Kota">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-lg-6">
                                                                    <label for="kode_pos">Kode Pos <label class="text-danger">*</label></label>
                                                                    <div class="form-group">
                                                                        <input type="number" value="<?php echo $dt_profile->kode_pos ?>" name="kode_pos" class="form-control" id="kode_pos" placeholder="Kode Pos" onkeypress="return isNumberKey(event);">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="kode_pos">Propinsi <label class="text-danger">*</label></label>
                                                                    <select name="propinsi" id="inputPropinsi" class="select2 form-control" required="required" style="width: 100%; height:36px;">
                                                                        <?php if (!empty($dt_profile->provinsi)): ?>
                                                                            <option value="<?php echo $dt_profile->provinsi ?>" selected><?php echo $dt_profile->provinsi ?></option>
                                                                        <?php else: ?>
                                                                            <option value="" selected>Propinsi</option>
                                                                        <?php endif ?>
                                                                        
                                                                    </select>
                                                            </div>
                                                            
                                                            <div class="row">
                                                                <div class="col-md-6 col-lg-6">
                                                                    <label for="identitas">Identitas <label class="text-danger">*</label></label>
                                                                    <input type="text" name="identitas" id="inputIdentitas" class="form-control" value="<?php echo $dt_profile->identitas ?>" placeholder="SIM / KTP / PASSPORT">
                                                                </div>
                                                                <div class="col-md-6 col-lg-6">
                                                                    <label for="nomor_identitas">Nomor Identitas <label class="text-danger">*</label></label>
                                                                    <input type="number" name="nomor_identitas" onkeypress="return isNumberKey(event);" id="inputNomoridentitas" class="form-control" value="<?php echo $dt_profile->nomor_identitas ?>" placeholder="Nomor Identitas">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                            <button type="submit" class="btn btn-primary">Simpan Profile</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endforeach ?>
                                </div>

                                

                            </div>
                        </div>
                    </div>
                <!-- Column -->
                    
                </div>
                <!-- Row -->
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
               
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php $this->load->view('perusahaan/footer'); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <?php require_once(APPPATH .'views/include/pelamar/inc_script.php'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var base_url = window.location.origin;
            $.ajax({
                type: "GET",
                url: base_url+"/assets_pelamar/countries.json",
                dataType: "json",
                success: function (data_negara) {
                    $.each(data_negara, function(i, value) {
                        $('#inputNegara, .negara').append($('<option>').text(value).attr('value', value));
                    });
                },
                error: function (result) {
                }
            });

            $.ajax({
                type: "GET",
                url: base_url+"/assets_pelamar/indonesia.json",
                dataType: "json",
                success: function (data_propinsi) {
                    $.each(data_propinsi, function(i, value) {
                        $('#inputPropinsi, .propinsi').append($('<option>').text(value).attr('value', value));
                    });
                },
                error: function (result) {
                }
            });

            $(".select2").select2();

            var nilai_akhir = $('.nilai_akhir_update').val();
            if (nilai_akhir == "IPK") {
                $('.skor_nilai_update').show();
                $('.nilai_akhir_update').change(function(event) {
                    var chek_update = $(this).val();
                    if (chek_update == "IPK") {
                        $('.skor_nilai_update').show();
                    } else {
                        $('.skor_nilai_update').hide();
                        // $("input[name=nilai_asli]").val('');
                        // $("input[name=nilai_dari]").val('');
                    }
                });
            } else {
                 $('.skor_nilai_update').hide();
                 $('.nilai_akhir_update').change(function(event) {
                    var chek_update = $(this).val();
                    if (chek_update == "IPK") {
                        $('.skor_nilai_update').show();
                    } else {
                        $('.skor_nilai_update').hide();
                        // $("input[name=nilai_asli]").val('');
                        // $("input[name=nilai_dari]").val('');
                    }
                });
            }

            $('.skor_nilai').hide();
            $(".nilai_akhir").change(function(event) {
                /* Act on the event */
                var check = $(this).val();
                if (check == "IPK") {
                    $('.skor_nilai').show();
                    // $("input[name=nilai_asli]").val(0);
                    // $("input[name=nilai_dari]").val(0);
                } else {
                    $('.skor_nilai').hide();
                    $("input[name=nilai_asli]").val('');
                    $("input[name=nilai_dari]").val('');
                }
            });

            $(document).on('click', 'input[name="masih_kerja"]', function () {
                var checked = $(this).prop('checked');
                if (checked) {
                    $('#bulan_start_selesai option:first').prop('selected',true);
                    $('#tahun_start_selesai option:first').prop('selected',true);
                    $('#bulan_start_selesai').attr('disabled','disabled');
                    $('#tahun_start_selesai').attr('disabled','disabled');

                } else {
                    $('#bulan_start_selesai').removeAttr('disabled');
                    $('#tahun_start_selesai').removeAttr('disabled');
                   
                     
                }
            });

            $(document).on('click', 'input[name="masih_kerja_update"]', function () {
                var checked = $(this).prop('checked');
                
                if (checked) {
                    console.log("dipilih");
                    $('.bulan_start_selesai_update option:first').prop('selected',true);
                    $('.tahun_start_selesai_update option:first').prop('selected',true);
                    $('.bulan_start_selesai_update').attr('disabled','disabled');
                    $('.tahun_start_selesai_update').attr('disabled','disabled');

                } else {
                    console.log("g dipilih");
                    $('.bulan_start_selesai_update').removeAttr('disabled');
                    $('.tahun_start_selesai_update').removeAttr('disabled');
                   
                     
                }
            });
        });

        function PrintMe(DivID) {
            var disp_setting="toolbar=yes,location=no,";
            disp_setting+="directories=yes,menubar=yes,";
            disp_setting+="scrollbars=yes,width=650, height=600, left=100, top=25";
           var content_vlue = document.getElementById(DivID).innerHTML;
           var docprint=window.open("","",disp_setting);
           docprint.document.open();
           docprint.document.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"');
           docprint.document.write('"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">');
           docprint.document.write('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">');
           docprint.document.write('<head><title>My Title</title>');
           docprint.document.write('<style type="text/css">body{ margin:0px;');
           docprint.document.write('font-family:verdana,Arial;color:#000;');
           docprint.document.write('font-family:Verdana, Geneva, sans-serif; font-size:12px;}');
           docprint.document.write('a{color:#000;text-decoration:none;} </style>');
           docprint.document.write('</head><body onLoad="self.print()">');
           docprint.document.write(content_vlue);
           docprint.document.write('</body></html>');
           docprint.document.close();
           docprint.focus();
        }

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode
            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        }

        function isCharKey(event) {
           var value = String.fromCharCode(event.which);
           var pattern = new RegExp(/[a-zåäö ]/i);
           return pattern.test(value);
        }

        $("#hapus_pengalaman").on('click', function(event) {
            event.preventDefault();
            var url = $(this).attr('href');
            console.log("di pilih " + url);
           
            swal({
              title: "HAPUS PENGALAMAN INI",
              text: "Hapus Pengalaman Ini ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: '#DD6B55',
              confirmButtonText: 'Yes, Hapus!',
              cancelButtonText: "No, Batal!",
              confirmButtonClass: "btn-danger",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm) {
              if (isConfirm) {
                swal("Pengalaman Berhasil di hapus", "", "success");
                window.location.replace(url);
              } else {
                swal("Batal Hapus", "", "error");
              }
            });
        });

        $("#hapus_pendidikan").on('click', function(event) {
            event.preventDefault();
            var url = $(this).attr('href');
            console.log("di pilih " + url);
           
            swal({
              title: "HAPUS PENDIDIKAN INI",
              text: "Hapus Pendidikan Ini ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: '#DD6B55',
              confirmButtonText: 'Yes, Hapus!',
              cancelButtonText: "No, Batal!",
              confirmButtonClass: "btn-danger",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm) {
              if (isConfirm) {
                swal("Pendidikan Berhasil di hapus", "", "success");
                window.location.replace(url);
              } else {
                swal("Batal Hapus", "", "error");
              }
            });
        });

         $("#hapus_resume").on('click', function(event) {
            event.preventDefault();
            var url = $(this).attr('href');
            console.log("di pilih " + url);
           
            swal({
              title: "HAPUS RESUME INI",
              text: "Hapus Resume Ini ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: '#DD6B55',
              confirmButtonText: 'Yes, Hapus!',
              cancelButtonText: "No, Batal!",
              confirmButtonClass: "btn-danger",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm) {
              if (isConfirm) {
                swal("Resume Berhasil di hapus", "", "success");
                window.location.replace(url);
              } else {
                swal("Batal Hapus", "", "error");
              }
            });
        });

    $(function() {
            $('a[data-toggle="tab"]').on('click', function(e) {
                window.localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = window.localStorage.getItem('activeTab');
            if (activeTab) {
                $('#myTab a[href="' + activeTab + '"]').tab('show');
                window.localStorage.removeItem("activeTab");
            }
        });

        
    </script>
</body>

</html>