<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once(APPPATH .'views/include/pelamar/inc_style.php'); ?>
    <style type="text/css">
        @media (min-width: 1025px) and (max-width: 1280px) {
          .hidden-lg {
            display: none;
          }
        }

        @media (min-width: 1281px) {
            .hidden-lg {
                display: none;
              }
        }
        @media (min-width: 320px) and (max-width: 480px) {
            .hidden-xs {
                display: none;
            }

            .font-sm {
                font-weight: 600;
            }

            .container-fluid {
                padding: 0 0px 25px 0px;
            }

            .card-no-border .card {
                border: 0px;
                 border-radius: 0px; 
                 box-shadow: none; 
                padding: 6px;
            }
            .card {
                 box-shadow: none; 
                border-radius: 4px;
            }
        }

    </style>
</head>

<body class="fix-header card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('pelamar/header'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('pelamar/left_sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- <div class="row page-titles m-b-0">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Dashboard3</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard3</li>
                    </ol>
                </div>
                <div>
                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                </div>
            </div> -->
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    
                    <?php 
                        $kode_pelamar = $this->session->userdata('kode_pelamar');
                        $tambahan     = "select * from Tbl_Info_Tambahan where kode_pelamar    = '$kode_pelamar'";
                        $keterampilan = "select * from Tbl_Keterampilan where kode_pelamar  = '$kode_pelamar'";
                        $pengalaman   = "select * from Tbl_Pengalaman_Kerja where kode_pelamar = '$kode_pelamar'";
                        $pendidikan  = "select * from Tbl_Riwayat_Pendidikan where kode_pelamar = '$kode_pelamar'";

                        $h_tambahan     = $this->db->query($tambahan)->num_rows();
                        $h_keterampilan = $this->db->query($keterampilan)->num_rows();
                        $h_pengalaman   = $this->db->query($pengalaman)->num_rows();
                        $h_pendidikan   = $this->db->query($pendidikan)->num_rows();
                     ?>

                    <div class="col-lg-8 col-md-12 hidden-lg">
                        <?php if (($h_tambahan > 0) || ($h_keterampilan > 0) || ($h_pengalaman > 0) || ($h_pendidikan > 0)): ?>
                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong><?php echo $this->session->userdata('nama_pelamar'); ?> </strong> ,Selamat Datang Di Wegosales
                            </div>
                        <?php else: ?>
                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Selamat Datang Di Wigosales</strong>, Pengguna awal harus melengkapi data diri dengan menekan tombol Perbaharui Profil, data yang wajib di lengkapi di antaranya, pada menu Pengalaman, Pendidikan, Keterampilan, Info Lain dan Profil Saya
                            </div>
                        <?php endif ?>
                        
                    </div>

                    <!-- Column -->
                    <div class="col-lg-4">
                        <!-- Column -->
                        <div class="card"> <img class="card-img-top" src="<?php echo base_url('assets/images/background/profile-bg.jpg'); ?>" alt="Card image cap" style="max-height: 165px;">
                            <div class="card-body little-profile text-center">
                                <div class="pro-img"><img src="<?php echo base_url('assets_storage/img_avatar/'.$this->session->userdata('img_profile')); ?>" alt="user" /></div>
                                <h3 class="m-b-0">
                                    <?php echo $this->session->userdata('nama_pelamar') ?>
                                </h3>
                                <p><?php echo $this->session->userdata('nomor_telepon') ?></p>
                                <p><?php echo $this->session->userdata('alamat_email') ?></p>
                                <p><a class="btn btn-success" href="<?php echo base_url('pelamar/admin/profil') ?>" role="button" data-toggle="tooltip" data-placement="top" title="Update Profil"><i class="fa fa-cog"></i> Perbaharui Profil</a></p>
                                
                            </div>
                        </div>
                        <!-- Column -->
                    </div>

                    <!-- Column -->
                    <div class="col-lg-8 col-md-12 hidden-xs">
                        <div class="jumbotron">
                            <div class="container">
                                <h1 class="one">Hello, <?php echo $this->session->userdata('nama_pelamar'); ?></h1>
                                
                                 <?php if (($h_tambahan > 0) || ($h_keterampilan > 0) || ($h_pengalaman > 0) || ($h_pendidikan > 0)): ?>
                                     <h1 class="two" style="display: none;">Selamat Datang di Wegosales</h1>
                                 <?php else: ?>
                                     <h1 class="two" style="display: none;">Lengkapi data diri anda terlebih dahulu</h1>
                                 <?php endif ?>
                                
                            </div>
                        </div>
                        <div class="card-body b-l calender-sidebar">
                            <div id="calendar"></div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php $this->load->view('perusahaan/footer'); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <?php require_once(APPPATH .'views/include/pelamar/inc_script.php'); ?>
    <script type="text/javascript">
        setInterval("$('.one, .two').toggle();",3000);
    </script>
</body>

</html>