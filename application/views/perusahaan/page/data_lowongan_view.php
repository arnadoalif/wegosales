﻿<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once(APPPATH .'views/include/perusahaan/inc_style.php'); ?>
    <style type="text/css">
        .box-title:hover {
            color: red;
        }
    </style>

</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('perusahaan/header'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('perusahaan/left_sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Lowongan</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <a class="btn btn-primary" href="<?php echo base_url('perusahaan/admin/tambah_lowongan') ?> " role="button"> Buka Lowongan</a>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('perusahaan/admin'); ?>">Home</a></li>
                        <li class="breadcrumb-item active"> Lowongan</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->

                <?php if (isset($_SESSION['sendmessage'])): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['sendmessage'] ?>
                    </div>
                <?php endif ?>
                <?php if (isset($_SESSION['error_data'])): ?>
                    <div class="alert alert-warning" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                    </div>
                <?php endif ?>
               
               <div class="row">
                    <!-- .col -->

                    <?php foreach ($data_lowongan as $dt_lowongan): ?>
                    
                        <?php if ($dt_lowongan->status_lowongan == "Active"): ?>
                             
                            <div class="col-md-12 col-lg-12" >
                                <div class="card card-body">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-7 text-left">
                                            <!-- <a href="app-contact-detail.html"><img src="" alt="user" class="img-responsive" style="max-width: 50%;"></a> -->
                                            <a data-toggle="modal" href='#modal-id<?php echo $dt_lowongan->slug_url ?>'><h3 class="box-title m-b-0"> <i class="fa fa-list-alt"></i> <?php echo $dt_lowongan->nama_lowongan; ?> - 
                                                <?php if ($dt_lowongan->status_lowongan == "Active"): ?>
                                                    <span class="label label-success">Active</span> 
                                                <?php else: ?>
                                                    <span class="label label-danger">Expired</span>
                                                <?php endif ?>
                                                 
                                                <?php if (!empty($dt_lowongan->status_requment)): ?>
                                                    - <label style="color: blue; font-size: 15px;"><i class="fa fa-star"></i></label>
                                                <?php else: ?>
                                                <?php endif ?>

                                            </h3>
                                            </a>
                                            <small>
                                                <p><?php echo $dt_lowongan->Nama_Perusahaan ?> / <?php echo $dt_lowongan->Alamat_Email ?></p>
                                            </small>
                                            <i class="fa fa-map-marker"></i> 
                                                <?php if (!empty($dt_lowongan->Alamat_perusahaan)): ?>
                                                    <?php echo $dt_lowongan->Alamat_perusahaan ?>
                                                <?php else: ?>
                                                    <a href="<?php echo base_url('perusahaan/admin/profil'); ?>" title="Atur Alamat">Atur Alamat</a>
                                                <?php endif ?>
                                            <br>
                                            <small>
                                                <i class="fa fa-phone"></i> <?php echo $dt_lowongan->Nomor_Telepon ?>
                                            </small> &nbsp;&nbsp;&nbsp;
                                            <small>
                                                <i class="fa fa-calendar"></i> <?php echo $dt_lowongan->tgl_insert ?> <span class="label label-success">open</span>
                                                <i class="fa fa-calendar"></i> <?php echo $dt_lowongan->tanggal_tutup ?> <span class="label label-danger">close</span> &nbsp;&nbsp;&nbsp;
                                            </small>
                                            <br>

                                            <small>
                                                <a href="<?php echo base_url('perusahaan/admin/edit-lowongan/'.$dt_lowongan->kode_lowongan) ?>" title=""><span class="label label-info"><i class="fa fa-pencil"></i> Edit Lowongan</span></a>
                                            </small> &nbsp;&nbsp;&nbsp;
                                            <small>
                                                <?php if (!empty($dt_lowongan->status_requment)): ?>
                                                    <a data-toggle="modal" href='#edit_metode<?php echo $dt_lowongan->kode_lowongan ?>'><span class="label label-info"><i class="fa fa-pencil"></i> Edit Metode Requirement</span></a>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a id="hapus_metode_lowongan" href="<?php echo base_url('perusahaan/admin/remove-metode/'.$dt_lowongan->kode_lowongan); ?>"><span class="label label-danger"><i class="fa fa-trash"></i> Hapus Metode Requirement</span></a>
                                                <?php else: ?>
                                                    <a data-toggle="modal" href='#tambah_metode<?php echo $dt_lowongan->kode_lowongan ?>'><span class="label label-primary"><i class="fa fa-plus"></i> Tambah Metode Requirement</span></a>
                                                <?php endif ?>

                                                 &nbsp;&nbsp;&nbsp;
                                                <a id="hapus_lowongan" href="<?php echo base_url('perusahaan/admin/remove-lowongan/'.$dt_lowongan->kode_lowongan); ?>"><span class="label label-danger"><i class="fa fa-trash"></i> Hapus Lowongan</span></a>
                                                
                                            </small>

                                        </div>
                                        <div class="col-md-4 col-lg-3">
                                            
                                        </div>
                                        <div class="col-md-4 col-lg-2">
                                            <a href="<?php echo base_url('perusahaan/admin/data_pelamar/'.$dt_lowongan->kode_lowongan) ?>" role="button">
                                                <h3 class="box-title m-b-0" style="margin-top: 29px; text-align: center;">  
                                                    <?php 
                                                        $this->load->model('Lowongan_model');
                                                        $m_lowongan = new Lowongan_model();
                                                        $data_pelamar = $m_lowongan->count_lowongan_posisi('Tbl_Apply', $dt_lowongan->kode_lowongan)->result();
                                                     ?>
                                                     <?php foreach ($data_pelamar as $count_pelamar): ?>
                                                         <?php if (!empty($count_pelamar->tot_pegawai)): ?>
                                                             <?php echo $count_pelamar->tot_pegawai ?>
                                                         <?php else: ?>
                                                             0
                                                         <?php endif ?>
                                                     <?php endforeach ?>
                                                     <br> 
                                                <small class="text-center">Apply Job</small></h3>
                                            </a> 

                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php else: ?>

                            <div class="col-md-12 col-lg-12" >
                                <div class="card card-body" style="background: gainsboro;">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-7 text-left">
                                            <!-- <a href="app-contact-detail.html"><img src="" alt="user" class="img-responsive" style="max-width: 50%;"></a> -->
                                            <a data-toggle="modal" href='#modal-id<?php echo $dt_lowongan->slug_url ?>'><h3 class="box-title m-b-0"> <i class="fa fa-list-alt"></i> <?php echo $dt_lowongan->nama_lowongan; ?> - 
                                                <?php if ($dt_lowongan->status_lowongan == "Active"): ?>
                                                    <span class="label label-success">Open</span> 
                                                <?php else: ?>
                                                    <span class="label label-danger">Close</span>
                                                <?php endif ?>
                                                 
                                                <?php if (!empty($dt_lowongan->status_requment)): ?>
                                                    - <label style="color: blue; font-size: 15px;"><i class="fa fa-star"></i></label>
                                                <?php else: ?>
                                                <?php endif ?>

                                            </h3>
                                            </a>
                                            <small>
                                                <p><?php echo $dt_lowongan->Nama_Perusahaan ?> / <?php echo $dt_lowongan->Alamat_Email ?></p>
                                            </small>
                                            <i class="fa fa-map-marker"></i> 
                                                <?php if (!empty($dt_lowongan->Alamat_perusahaan)): ?>
                                                    <?php echo $dt_lowongan->Alamat_perusahaan ?>
                                                <?php else: ?>
                                                    <a href="<?php echo base_url('perusahaan/admin/profil'); ?>" title="Atur Alamat">Atur Alamat</a>
                                                <?php endif ?>
                                            <br>
                                            <!-- <small>
                                                <a href="<?php echo base_url('perusahaan/admin/edit-lowongan/'.$dt_lowongan->kode_lowongan) ?>" title=""><span class="label label-info"><i class="fa fa-pencil"></i> Edit Lowongan</span></a>
                                            </small> &nbsp;&nbsp;&nbsp; -->
                                            <small>
                                                <i class="fa fa-phone"></i> <?php echo $dt_lowongan->Nomor_Telepon ?>
                                            </small> &nbsp;&nbsp;&nbsp;
                                            <small>
                                                <i class="fa fa-calendar"></i> <?php echo $dt_lowongan->tgl_insert ?> <span class="label label-success">open</span>
                                                <i class="fa fa-calendar"></i> <?php echo $dt_lowongan->tanggal_tutup ?> <span class="label label-danger">close</span> &nbsp;&nbsp;&nbsp;
                                            </small>
                                            <br>
                                            <small>
                                                
                                                <a id="hapus_lowongan" href="<?php echo base_url('perusahaan/admin/remove-lowongan/'.$dt_lowongan->kode_lowongan); ?>"><span class="label label-danger"><i class="fa fa-trash"></i> Hapus Lowongan</span></a>
                                                
                                            </small>

                                        </div>
                                        <div class="col-md-4 col-lg-3">
                                            
                                        </div>
                                        <div class="col-md-4 col-lg-2">
                                            <a href="<?php echo base_url('perusahaan/admin/data_pelamar/'.$dt_lowongan->kode_lowongan) ?>" role="button">
                                                <h3 class="box-title m-b-0" style="margin-top: 29px; text-align: center;">  
                                                    <?php 
                                                        $this->load->model('Lowongan_model');
                                                        $m_lowongan = new Lowongan_model();
                                                        $data_pelamar = $m_lowongan->count_lowongan_posisi('Tbl_Apply', $dt_lowongan->kode_lowongan)->result();
                                                     ?>
                                                     <?php foreach ($data_pelamar as $count_pelamar): ?>
                                                         <?php if (!empty($count_pelamar->tot_pegawai)): ?>
                                                             <?php echo $count_pelamar->tot_pegawai ?>
                                                         <?php else: ?>
                                                             0
                                                         <?php endif ?>
                                                     <?php endforeach ?>
                                                     <br> 
                                                <small class="text-center">Apply Job</small></h3>
                                            </a> 

                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php endif ?>

                    

                    <div class="modal fade" id="modal-id<?php echo $dt_lowongan->slug_url ?>">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                    <h4 class="modal-title">
                                        <?php if (!empty($dt_lowongan->status_requment)): ?>
                                            <a class="btn btn-mg btn-primary" data-toggle="modal" href='#metode<?php echo $dt_lowongan->slug_url ?>'>Metode Requirement</a>
                                        <?php endif ?>
                                        Lowongan (<strong><?php echo $dt_lowongan->nama_lowongan; ?></strong>)</h4>
                                </div>
                                <div class="modal-body">
                                    
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4">
                                             <a href=""><img src="<?php echo base_url('assets_storage/img_iklan/'.$dt_lowongan->cover_iklan) ?>" alt="user" class="img-responsive" "></a>
                                        </div>
                                        <div class="col-md-8 col-lg-8">
                                            <table class="table table-hover">
                                                
                                                <tbody>
                                                    <tr>
                                                        <td>Nama Lowongan</td>
                                                        <td><?php echo $dt_lowongan->nama_lowongan; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gaji Perbulan 
                                                            <?php if (!empty($dt_lowongan->show_salary)): ?>
                                                                <span class="label text-sm-left label-success">show</span> 
                                                            <?php else: ?>
                                                                <span class="label text-sm-left label-danger">no show</span> 
                                                            <?php endif ?>
                                                        </td>
                                                        <td>Min
                                                            <?php if (!empty($dt_lowongan->salary_min)): ?>
                                                                <?php echo "Rp ".number_format($dt_lowongan->salary_min) ?>
                                                            <?php else: ?>
                                                                0
                                                            <?php endif ?>
                                                            
                                                            <?php if (!empty($dt_lowongan->salary_max)): ?>

                                                                <?php echo "- Max Rp ".number_format($dt_lowongan->salary_max) ?>
                                                            <?php else: ?>
                                                                
                                                            <?php endif ?>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pekerjaan Jenis</td>
                                                        <td><?php echo $dt_lowongan->pekerjaan_jenis ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Posisi Spesialis</td>
                                                        <td><?php echo $dt_lowongan->posisi_spesialis ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Keahlian</td>
                                                        <td><?php echo $dt_lowongan->keahlian ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td>Tingkat Pendidikan</td>
                                                        <td><?php echo $dt_lowongan->tingkat_pendidikan ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bidang Studi</td>
                                                        <td><?php echo $dt_lowongan->bidang_studi ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pengalaman</td>
                                                        <td><?php echo $dt_lowongan->pengalaman ?> Tahun</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kategori Daerah </td>
                                                        <td>
                                                            <?php foreach ($data_provinsi as $t): ?>
                                                                <?php if (in_array($t->nama_provinsi, explode(", ", $dt_lowongan->kategori_daerah))): ?>
                                                                    <span class='label text-sm-left label-danger'><?php echo $t->nama_provinsi ?></span>
                                                                <?php endif ?>
                                                            <?php endforeach ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Deskripsi Pekerjaan</td>
                                                        <td>
                                                            <?php echo $dt_lowongan->deskripsi_pekerjaan ?>
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="metode<?php echo $dt_lowongan->slug_url ?>">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <!-- <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Modal title</h4>
                                </div> -->
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12">
                                             <a href=""><img src="<?php echo base_url('assets_storage/img_rekut/'.$dt_lowongan->metode_rekut) ?>" alt="user" class="img-responsive" "></a>
                                             <a href=""><img src="<?php echo base_url('assets_storage/img_metode/'.$dt_lowongan->cover_metode_rekut) ?>" alt="user" class="img-responsive" "></a>

                                        </div>
                                        <div class="col-md-12 col-lg-12">
                                            <table class="table table-hover">
                                                
                                                <tbody>
                                                    <?php if (!empty($dt_lowongan->deskripsi_metode_rekut)): ?>
                                                        <tr>
                                                            <td>Deskripsi Metode Requirement</td>
                                                            <td>
                                                                <?php echo $dt_lowongan->deskripsi_metode_rekut ?>
                                                            </td>
                                                        </tr>
                                                    <?php endif ?>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="tambah_metode<?php echo $dt_lowongan->kode_lowongan ?>">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Tambah Metode Requirement Lowongan <?php echo $dt_lowongan->nama_lowongan ?></h4>
                                </div>
                                <form action="<?php echo base_url('perusahaan/action_tambah_requiredment'); ?>" method="POST" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        
                                        <div class="row">
                                            <div class="col-sm-offset-1 col-sm-10">
                                                <input type="hidden" name="kode_lowongan" id="inputKode_l" class="form-control" value="<?php echo $dt_lowongan->kode_lowongan ?>">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Pilih Metode Requirement</label>
                                                    <div class="col-sm-9">
                                                        <select name="tipe_metode_rekut" class="form-control" required="required">
                                                            <option value="" selected>Pilih Metode Rekutmen</option>
                                                            <option value="1">Apprenticeship Program</option>
                                                            <option value="2">Test Case</option>
                                                            <option value="3">Interview</option>
                                                            <option value="4">Test & Interview</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <input type="hidden" name="kode_lowongan" id="inputKode_l" class="form-control" value="<?php echo $dt_lowongan->kode_lowongan ?>">
                                                    <label class="control-label text-right col-md-3">Upload Gambar</label>
                                                    <div class="col-sm-9">
                                                        <input type="file" name="gamabaran_requirement" class="form-control" accept="image/*" onchange="loadFile(event)">
                                                        <img id="output" style="max-width: 25%;" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Deskripsi Requirement <span style="color: red; font-size: 12px;"><i>Tulisakan deskripsi / metode requirement dilowongan</i></span></label>
                                                    <div class="col-sm-9">
                                                        
                                                        <?php if (isset($_SESSION['deskripsi_requirement'])): ?>
                                                            <textarea name="deskripsi_requirement" id="input" class="form-control summernote" rows="3" required="required">
                                                                <?php echo $_SESSION['deskripsi_requirement'] ?>
                                                            </textarea>
                                                        <?php else: ?>
                                                            <textarea name="deskripsi_requirement" id="input" class="form-control summernote" rows="3" required="required">
                                                                
                                                            </textarea>
                                                        <?php endif ?>
                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary">Simpan Metode</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="edit_metode<?php echo $dt_lowongan->kode_lowongan ?>">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                     <h4 class="modal-title">Edit Metode Requirement Lowongan <?php echo $dt_lowongan->nama_lowongan ?></h4>
                                </div>
                                <form action="<?php echo base_url('perusahaan/action_edit_requiredment'); ?>" method="POST" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        
                                        <div class="row">
                                            <div class="col-sm-offset-1 col-sm-10">
                                                <div class="form-group row">
                                                    <input type="hidden" name="kode_lowongan" id="inputKode_l" class="form-control" value="<?php echo $dt_lowongan->kode_lowongan ?>">
                                                    <label class="control-label text-right col-md-3">Pilih Metode Requirement</label>
                                                    <div class="col-sm-9">
                                                        <select name="tipe_metode_rekut" class="form-control" required="required">
                                                            <option value="" selected>Pilih Metode Rekutmen</option>
                                                            <option <?php echo $dt_lowongan->metode_rekut == '1' ? 'selected = "selected"': ''; ?> value="1">Apprenticeship Program</option>
                                                            <option <?php echo $dt_lowongan->metode_rekut == '2' ? 'selected = "selected"': ''; ?> value="2">Test Case</option>
                                                            <option <?php echo $dt_lowongan->metode_rekut == '3' ? 'selected = "selected"': ''; ?> value="3">Interview</option>
                                                            <option <?php echo $dt_lowongan->metode_rekut == '4' ? 'selected = "selected"': ''; ?> value="4">Test & Interview</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Upload Gambar</label>
                                                    <div class="col-sm-9">
                                                        <input type="file" name="gamabaran_requirement" class="form-control" accept="image/*" onchange="loadFile(event)">
                                                        <img id="output" style="max-width: 25%;" />
                                                        <input type="hidden" name="img_rekut_olg" id="inputImg_rek" class="form-control" value="<?php echo $dt_lowongan->cover_metode_rekut ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Deskripsi Requirement <span style="color: red; font-size: 12px;"><i>Tulisakan deskripsi / metode requirement dilowongan</i></span></label>
                                                    <div class="col-sm-9">
                                                        
                                                        <?php if (isset($_SESSION['deskripsi_requirement'])): ?>
                                                            <textarea name="deskripsi_requirement" id="input" class="form-control summernote" rows="3" required="required">
                                                                <?php echo $_SESSION['deskripsi_requirement'] ?>
                                                            </textarea>
                                                        <?php else: ?>
                                                            <textarea name="deskripsi_requirement" id="input" class="form-control summernote" rows="3" required="required">
                                                                <?php echo $dt_lowongan->deskripsi_metode_rekut; ?>
                                                            </textarea>
                                                        <?php endif ?>
                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <?php endforeach ?>
                    <!-- /.col -->
                   
                </div>

                <!-- Row -->
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php $this->load->view('perusahaan/footer'); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
     <?php require_once(APPPATH .'views/include/perusahaan/inc_script.php'); ?>
     <script type="text/javascript">
        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
          };

         $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });

        $('.inline-editor').summernote({
            airMode: true
        });

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode
            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        }

        function isCharKey(event) {
           var value = String.fromCharCode(event.which);
           var pattern = new RegExp(/[a-zåäö ]/i);
           return pattern.test(value);
        }

        $("#hapus_lowongan").on('click', function(event) {
            event.preventDefault();
            var url = $(this).attr('href');
            swal({
              title: "HAPUS LOWONGAN",
              text: "Hapus Lowongan sekarang ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: '#DD6B55',
              confirmButtonText: 'Yes, Hapus!',
              cancelButtonText: "No, Batal!",
              confirmButtonClass: "btn-danger",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm) {
              if (isConfirm) {
                swal("Berhasil di hapus", "", "success");
                // console.log(url);
                window.location.replace(url);
              } else {
                swal("Batal Hapus", "", "error");
              }
            });
            console.log("hapus lowongan");
        });

        

        $("#hapus_metode_lowongan").on('click', function(event) {
            event.preventDefault();
            var url = $(this).attr('href');
            swal({
              title: "HAPUS METODE LOWONGAN",
              text: "Hapus Metode Lowongan sekarang ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: '#DD6B55',
              confirmButtonText: 'Yes, Hapus!',
              cancelButtonText: "No, Batal!",
              confirmButtonClass: "btn-danger",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm) {
              if (isConfirm) {
                swal("Metode Berhasil di hapus", "", "success");
                // console.log(url);
                window.location.replace(url);
              } else {
                swal("Batal Hapus", "", "error");
              }
            });
            console.log("hapus lowongan");
        });
     </script>
</body>

</html>