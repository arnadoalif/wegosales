<!DOCTYPE html>
<html lang="en">

<head>

    <?php require_once(APPPATH .'views/include/perusahaan/inc_style.php'); ?>
    <style type="text/css">
        a.visited {
            color:#205081;
        }
        .box-title:hover {
            color: red;
        }

        .modal-lg {
            max-width: 1000px;
        }
    </style>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('perusahaan/header'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('perusahaan/left_sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Pelamar</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('perusahaan/admin'); ?>">Home</a></li>
                        <li class="breadcrumb-item active">Pelamar</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
               <!-- .row -->
                <div class="row">

                    <!-- <div class="col-md-3 col-lg-3">
                        
                        <div class="card card-body" style="position: fixed;width: 300px;height: auto;">

                            <div class="panel-body">
                                <h5>Lokasi</h5>
                                <div class="checkbox">
                                    <input type="checkbox" name="show_salary" id="wil_yogja" class="filled-in" value="Yogyakarta"/>
                                    <label for="wil_yogja"> Yogyakarta</label><span class="pull-right">(0)</span>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox" name="show_salary" id="jawa_tengah" class="filled-in" value="Jawa Tengah"/>
                                    <label for="jawa_tengah"> Jawa Tengah</label><span class="pull-right">(0)</span>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox" name="show_salary" id="jawa_barat" class="filled-in" value="Jawa Barat"/>
                                    <label for="jawa_barat"> Jawa Barat</label><span class="pull-right">(0)</span>
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox" name="show_salary" id="jawa_timur" class="filled-in" value="Jawa Timur"/>
                                    <label for="jawa_timur"> Jawa Timur</label><span class="pull-right">(0)</span>
                                </div>
                            </div>

                            <div class="panel-body">
                                <h5>Salary</h5>
                                <select name="salary" id="input" class="form-control" required="required">
                                    <option value="max" selected>Salary > 1 Juta</option>
                                    <option value="min">Salary < 1 Juta </option>
                                </select>     
                            </div>

                            <button type="button" class="btn btn-block btn-default" style="margin-top: 20px"> CARI </button>
                        </div>
                    </div> -->

                    <div class="col-md-12 col-lg-12">
                        <div class="row">
                            <?php foreach ($data_apply as $dt_apply): ?>
                                <?php 
                                    $info_lainnya = $this->db->query("SELECT * FROM Tbl_Info_Tambahan WHERE kode_pelamar = '$dt_apply->kode_pelamar'")->row();
                                    $info_pengalaman =$this->db->query("SELECT TOP 1 * FROM Tbl_Pengalaman_Kerja WHERE kode_pelamar = '$dt_apply->kode_pelamar' ORDER BY kode_pengalaman DESC")->result();
                                    $info_pendidikan = $this->db->query("SELECT kualifikasi, tahun_lulusan from Tbl_Riwayat_Pendidikan where kode_pelamar = '$dt_apply->kode_pelamar' order by kode_pendidikan desc")->row();
                                 ?>

                                <div class="col-md-4 col-lg-4">
                                    <div class="card card-body">
                                        <div class="row">
                                            <div class="col-md-8 col-lg-9">
                                                <a data-toggle="modal" href='#pelamar-id<?php echo $dt_apply->kode_pelamar ?>'>
                                                    <h5 class="box-title m-b-0">
                                                        <?php echo $dt_apply->nama_pelamar ?>
                                                        <?php 
                                                            $this->load->model('Lib_model');
                                                            $m_lib = new Lib_model();

                                                            $result = $m_lib->calculate_age($dt_apply->kode_pelamar); 
                                                            echo ("<span style='font-size: 14px; font-weight: 600;'> / ".$result."<span>");
                                                        ?>
                                                    </h5>
                                                </a>
                                                    <?php foreach ($info_pengalaman as $f_penggalaman): ?>

                                                        <small><?php echo $f_penggalaman->posisi_kerja ?></small><br>
                                                        <small><?php echo $f_penggalaman->nama_perusahaan ?> &nbsp;<br>
                                                                <?php echo $f_penggalaman->bulan_start_kerja ?>, <?php echo $f_penggalaman->tahun_start_kerja ?>
                                                                <?php if (empty($f_penggalaman->masih_kerja)): ?>
                                                                    <?php echo "- ".$f_penggalaman->bulan_start_selesai ?>, <?php echo $f_penggalaman->tahun_start_selesai ?>
                                                                <?php else: ?>
                                                                    - Sampai Sekarang
                                                                <?php endif ?>

                                                        </small>
                                                    <?php endforeach ?>
                                                <p></p>                                               
                                            </div>
                                            <div class="col-md-4 col-lg-3 text-center">
                                                <a data-toggle="modal" href='#pelamar-id<?php echo $dt_apply->kode_pelamar ?>'>
                                                    <img src="<?php echo base_url('assets_storage/img_avatar/'.$dt_apply->img_profile); ?>" alt="user" class="img-responsive lazy" data-original="<?php echo base_url('assets_storage/img_avatar/'.$dt_apply->img_profile); ?>">
                                                </a>
                                            </div>
                                        
                                        <div class="row">
                                            <div class="col-md-6 col-lg-12" style="padding-left: 30px;">
                                                <small>
                                                    <?php if(!empty($dt_apply->provinsi)): ?>
                                                         <label><i class="fa fa-map-marker"></i> <?php echo $dt_apply->provinsi ?></label>
                                                    <?php endif ?>
                                                    <label>
                                                        <?php if (!empty($info_lainnya->gaji)): ?>
                                                            <i class="fa fa-dollar"></i> <?php echo $info_lainnya->mata_uang ?> - <?php echo number_format($info_lainnya->gaji); ?>
                                                        <?php else: ?>
                                                            <i class="fa fa-dollar"></i> Kosong
                                                        <?php endif ?>
                                                         
                                                        <?php if (!empty($info_pendidikan)): ?>
                                                            <i class="fa fa-graduation-cap"></i> <?php echo $info_pendidikan->kualifikasi ?> <?php echo "/ (".$info_pendidikan->tahun_lulusan.")" ?></label>  
                                                        <?php endif ?>
                                                        
                                                </small>
                                            </div>
                                        </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            
                            <div class="modal fade" id="pelamar-id<?php echo $dt_apply->kode_pelamar ?>">
                                <?php 
                                    $det_pengalaman   = $this->db->query("SELECT * FROM Tbl_Pengalaman_Kerja WHERE kode_pelamar = '$dt_apply->kode_pelamar' ORDER BY kode_pengalaman DESC")->result();
                                    $det_pendidikan   = $this->db->query("SELECT * from Tbl_Riwayat_Pendidikan where kode_pelamar = '$dt_apply->kode_pelamar' order by kode_pendidikan DESC")->result();
                                    $det_keterampilan = $this->db->query("SELECT * from Tbl_Keterampilan where kode_pelamar = '$dt_apply->kode_pelamar'")->result();
                                    $det_info_lainnya = $this->db->query("SELECT * FROM Tbl_Info_Tambahan WHERE kode_pelamar = '$dt_apply->kode_pelamar'")->result();

                                ?>
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <!-- <button type="button" id="cmd" class="btn btn-primary"> <i class="fa fa-download"></i> DOWNLOAD CV </button> -->
                                            <a class="btn btn-default" href="<?php echo base_url('perusahaan/admin/cv_pdf/'.$dt_apply->kode_pelamar.'/'.$dt_apply->kode_lowongan) ?>" title="" target="_blank" role="button"><i class="fa fa-download"></i> DOWNLOAD CV</a>
                                        </div>

                                        <div class="modal-body">

                                            <div class="d-flex flex-row comment-row">
                                                <div class="p-2"><span class=""><img src="<?php echo base_url('assets_storage/img_avatar/'.$dt_apply->img_profile); ?>" alt="user" width="100"></span></div>
                                                <div class="comment-text w-100">
                                                    <h5><?php echo $dt_apply->nama_pelamar ?>
                                                        <?php 
                                                            $this->load->model('Lib_model');
                                                            $m_lib = new Lib_model();

                                                            $result = $m_lib->calculate_age($dt_apply->kode_pelamar); 
                                                            echo ("<span style='font-size: 14px; font-weight: 600;'> / ".$result."<span>");
                                                        ?>
                                                    </h5>
                                                    <div class="comment-footer">
                                                        <?php foreach ($info_pengalaman as $f_penggalaman): ?>
                                                            <span class="date"><strong><?php echo $f_penggalaman->posisi_kerja ?></strong></span><br>
                                                            <label><?php echo $f_penggalaman->nama_perusahaan ?></label>
                                                        <?php endforeach ?>

                                                        <br>
                                                        <span class="" style="font-size: 15px;"> <i class="fa fa-phone"></i> <?php echo $dt_apply->nomor_telepon ?> | <i class="fa fa-envelope"></i> <?php echo $dt_apply->alamat_email ?> | <i class="fa fa-dollar"></i> 
                                                            <?php if (!empty($info_lainnya->gaji)): ?>
                                                                <?php echo $info_lainnya->mata_uang ?> - <?php echo number_format($info_lainnya->gaji); ?>
                                                            <?php else: ?>
                                                                Kosong
                                                            <?php endif ?>
                                                              | <i class="fa fa-map-marker"></i> <?php echo $dt_apply->kota.', ',$dt_apply->provinsi ?></span> 
                                                        <br>
                                                        <span class="" style="font-size: 15px;"> <i class="fa fa-map"></i> Pilih Lokasi <strong> <?php echo $dt_apply->wilayah_daftar ?></strong>  </span>

                                                    </div>
                                                    <p class="m-b-5 m-t-10"></p>

                                                </div>
                                            </div>

                                        <div id="createpdf">

                                            <hr>
                                            <fieldset>
                                                <legend><i class="fa fa-quote-left"></i> Mengapa mempekerjakan saya ?</legend>
                                                <p>
                                                    <?php echo '" '.$dt_apply->deskripsi_promosi. '"' ?>
                                                </p>
                                            </fieldset>

                                            <hr>
                                            <fieldset>
                                                <legend><i class="fa fa-briefcase"></i> Pengalaman</legend>
                                                 
                                                <?php foreach ($det_pengalaman as $dt_pengalaman): ?>
                                                <!-- Comment Row -->
                                                    <div class="d-flex flex-row comment-row">
                                                        <div class="p-2">
                                                            <p><?php echo $dt_pengalaman->bulan_start_kerja ?>, <?php echo $dt_pengalaman->tahun_start_kerja ?></p> - 
                                                            <p>
                                                                <?php if (empty($dt_pengalaman->masih_kerja)): ?>
                                                                    <?php echo $dt_pengalaman->bulan_start_selesai ?>, <?php echo $dt_pengalaman->tahun_start_selesai ?>
                                                                <?php else: ?>
                                                                    Sampai Sekarang
                                                                <?php endif ?>
                                                            </p>
                                                        </div>
                                                        <div class="comment-text w-90">
                                                            <h5><?php echo $dt_pengalaman->posisi_kerja ?></h5>
                                                            <div class="comment-footer">
                                                                <span class="date"> <i class="fa fa-building"></i> <?php echo $dt_pengalaman->nama_perusahaan ?> | <?php echo $dt_pengalaman->negara ?></span>
                                                                
                                                            </div>
                                                            <table class="table">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Industri</td>
                                                                        <td><?php echo $dt_pengalaman->industri ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Spesialisasi</td>
                                                                        <td><?php echo $dt_pengalaman->spesialisasi ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Bidang Pekerjaan</td>
                                                                        <td><?php echo $dt_pengalaman->bidang_pekerjaan ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Jabatan</td>
                                                                        <td><?php echo $dt_pengalaman->jabatan ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Gaji Perbulan</td>
                                                                        <td>
                                                                            <?php echo $dt_pengalaman->mata_uang ?> - <?php echo number_format($dt_pengalaman->gaji); ?>
                                                                            <!-- <?php if (!empty($dt_pengalaman->gaji) && (!empty($dt_pengalaman->mata_uang))): ?>
                                                                                
                                                                            <?php else: ?>
                                                                                Kosong
                                                                            <?php endif ?> -->
                                                                                
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <p class="m-b-5 m-t-10"><?php echo $dt_pengalaman->keterangan_kerja ?></p>
                                                        </div>
                                                    </div>
                                                <!-- End Comment Row -->
                                                <?php endforeach ?>

                                            </fieldset>

                                            <hr>
                                            <fieldset>
                                                <legend><i class="fa fa-graduation-cap"></i> Pendidikan</legend>

                                                <?php foreach ($det_pendidikan as $dt_pendidikan): ?>
                                                
                                                <!-- Comment Row -->
                                                <div class="d-flex flex-row comment-row">
                                                    <div class="p-2">
                                                        <p><?php echo $dt_pendidikan->bulan_wisuda ?> , <?php echo $dt_pendidikan->tahun_lulusan ?></p>
                                                    </div>
                                                    <div class="comment-text w-90">
                                                        <h5><?php echo $dt_pendidikan->nama_universitas ?></h5>
                                                        <div class="comment-footer">
                                                            <span class="date"><?php echo $dt_pendidikan->kualifikasi ?> di <?php echo $dt_pendidikan->jurusan ?> | <?php echo $dt_pendidikan->lokasi ?></span>
                                                            
                                                        </div>
                                                        <table class="table">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Jurusan</td>
                                                                    <td><?php echo $dt_pendidikan->jurusan ?></td>
                                                                </tr>
                                                                <?php if ($dt_pendidikan->grade == "IPK"): ?>
                                                                    <tr>
                                                                        <td><?php echo $dt_pendidikan->grade ?></td>
                                                                        <td><?php echo $dt_pendidikan->nilai_asli ?> / <?php echo $dt_pendidikan->nilai_dari ?></td>
                                                                    </tr>
                                                                <?php else: ?>
                                                                    <tr>
                                                                        <td>Hasil Studi</td>
                                                                        <td>
                                                                            <?php if ($dt_pendidikan->grade == "Masih Belajar"): ?>
                                                                                <span class="label label-info"><?php echo $dt_pendidikan->grade ?></span> 
                                                                            <?php else: ?>
                                                                                <span class="label label-danger"><?php echo $dt_pendidikan->grade ?></span> 
                                                                            <?php endif ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php endif ?>
                                                                
                                                                
                                                            </tbody>
                                                        </table>
                                                        <p class="m-b-5 m-t-10"><?php echo $dt_pendidikan->informasi_tambahan ?></p>
                                                    </div>
                                                </div>
                                                <!-- End Comment Row -->
                                                <?php endforeach ?>
                                            </fieldset>

                                            <hr>
                                            <fieldset>
                                                <legend><i class="fa fa-code"></i> Keterampilan</legend>
                                                <div class="form-group row">
                                                    <label for="keterampilan" class="col-sm-3 control-label">Keterampilan</label>
                                                    <?php foreach ($det_keterampilan as $dt_keterampilan): ?>
                                                    <div class="col-sm-9">
                                                       <?php echo $dt_keterampilan->keterampilan ?>
                                                    </div>
                                                    <?php endforeach ?>
                                                </div>
                                            </fieldset>

                                            <hr>
                                            <fieldset>
                                                <legend><i class="fa fa-comments-o"></i> Bahasa</legend>
                                                <div class="form-group row">
                                                    <label for="bahasa" class="col-sm-3 control-label">Bahasa</label>
                                                    <div class="col-sm-9">
                                                    <?php foreach ($det_keterampilan as $dt_keterampilan): ?>
                                                        <?php echo $dt_keterampilan->kuasai_bahasa ?>
                                                    <?php endforeach ?>
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <hr>
                                            <fieldset>
                                                <legend><i class="fa fa-align-justify"></i> Info Gaji</legend>
                                                <?php foreach ($det_info_lainnya as $dt_info): ?>
                                        
                                                <table class="table table-hover">
                                                    <tbody>
                                                        <tr>
                                                            <td>Gaji yang diharapkan</td>
                                                            <td>
                                                                <?php if (!empty($dt_info->gaji)): ?>
                                                                     <?php echo $dt_info->mata_uang .' - '. number_format($dt_info->gaji); ?>
                                                                <?php else: ?>
                                                                    Kosong
                                                                <?php endif ?>
                                                                 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Lokasi kerja yang diharapkan</td>
                                                            <td><?php echo $dt_info->lokasi_kerja_1 .", ". $dt_info->lokasi_kerja_2 .", ". $dt_info->lokasi_kerja_3?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Informasi lainnya</td>
                                                            <td><?php echo $dt_info->informasi_lainnya ?> </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <?php endforeach ?>
                                            </fieldset>

                                            <hr>
                                            <fieldset>
                                                <legend><i class="fa fa-user"></i> Profil Saya</legend>
                                   
                                                <div class="card-body">
                                                    <table class="table table-hover">
                                                        <tbody>
                                                            <tr>
                                                                <td>Jenis Kelamin</td>
                                                                <td>
                                                                    <?php if (!empty($dt_apply->jenis_kelamin)): ?>
                                                                        <?php echo $dt_apply->jenis_kelamin ?>
                                                                    <?php else: ?> <i>Not Set</i> <?php endif ?>
                                                                </td>
                                                            </tr> 
                                                            <tr>
                                                                <td>Alamat 1</td>
                                                                <td>
                                                                    <?php if (!empty($dt_apply->alamat_1)): ?>
                                                                        <?php echo $dt_apply->alamat_1 ?>
                                                                    <?php else: ?> <i>Not set</i> <?php endif ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Alamat 2</td>
                                                                <td>
                                                                    <?php if (!empty($dt_apply->alamat_2)): ?>
                                                                        <?php echo $dt_apply->alamat_2 ?>
                                                                    <?php else: ?>
                                                                        <i>Not set</i>
                                                                    <?php endif ?>
                                                                </td>
                                                            </tr>
                                                             <tr>
                                                                <td>Tanggal Lahir</td>
                                                                <td>
                                                                    <?php echo $dt_apply->tanggal.' '. $dt_apply->bulan.' '. $dt_apply->tahun ?>
                                                                </td>
                                                            </tr>
                                                            <?php if (!empty($dt_apply->file_resume)): ?>
                                                                <tr>
                                                                    <td>File Resume</td>
                                                                    <td>
                                                                        <a href="<?php echo base_url('assets_storage/file_resume/'.$dt_apply->file_resume) ?>" target="_blank"><i class="fa fa-eye"></i> Lihat Resume</a> <a href="<?php echo base_url('assets_storage/file_resume/'.$dt_apply->file_resume); ?>" title="" download="<?php echo "Resume_".str_replace(' ', '_', $dt_apply->nama_pelamar).'.pdf' ?>" class="text-info pull-right"> <i class="fa fa-download"></i>  Download Resume</a></td>
                                                                </tr>
                                                            <?php endif ?>

                                                            <?php if (!empty($dt_apply->identitas)): ?>
                                                                <tr>
                                                                    <td>Identitas</td>
                                                                    <td><?php echo $dt_apply->identitas ?></td>
                                                                </tr>
                                                            <?php endif ?>
                                                            
                                                            <?php if (!empty($dt_apply->nomor_identitas)): ?>
                                                                <tr>
                                                                    <td>Nomor Identitas</td>
                                                                    <td><?php echo $dt_apply->nomor_identitas ?></td>
                                                                </tr>
                                                            <?php endif ?>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                                
                                            </fieldset>

                                        </div>


                                        </div> <!-- end -->

                                        
                                    </div>
                                </div>
                            </div>

                        <?php endforeach ?>
                        </div>
                    </div> 

                </div>
                <!-- /.row -->
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php $this->load->view('perusahaan/footer'); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
     <?php require_once(APPPATH .'views/include/perusahaan/inc_script.php'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery_lazyload/lazyload.min.js'); ?>"></script>
    <script type="text/javascript">
            var doc = new jsPDF();
            var specialElementHandlers = {
                '#editor': function (element, renderer) {
                    return true;
                }
            };

            $('#cmd').click(function () {
                doc.fromHTML($('#createpdf').html(), 15, 15, {
                    'width': 170,
                        'elementHandlers': specialElementHandlers
                });
                doc.save('sample-file.pdf');
            });

            $(document).ready(function() {
                $("img.lazy").lazyload();
            });
            
            // function getPDFFileButton () {
            //     // Select which div with id that need to be printed
            //     // to print body $('body')
            //     // here printing div with content id $("#content")
            //     // using html canvas to save as required pdf to image to preserve css
            //     return html2canvas($('body'), {
            //         background: "#ffffff",
            //         onrendered: function(canvas) {
            //             var myImage = canvas.toDataURL("image/jpeg,1.0");
            //             // Adjust width and height
            //             var imgWidth = (canvas.width * 20) / 240;
            //             var imgHeight = (canvas.height * 20) / 240; 
            //             // jspdf changes
            //             var pdf = new jsPDF('p', 'mm', 'a4');
            //             pdf.addImage(myImage, 'JPEG', 15, 2, imgWidth, imgHeight); // 2: 19
            //             pdf.save('Download.pdf');
            //         }
            //     });
            // }       
            // $("#btnPrint").on("click", function () {
            //     getPDFFileButton ()
            // });
            
            $(document).ready(function() {
                $(this).change(function(event) {
                    /* Act on the event */
                    var salary = $("select[name='salary']").val();
                    console.log(salary);
                });

                $(document).on('click', 'input[name="show_requirement"]', function () {
                    var checked = $(this).prop('checked');
                    if (checked) {
                        console.log("dipilih");
                       
                    } else {
                        console.log("tidak dipilih");
                    }
                });
                
            });

     </script>
</body>

</html>