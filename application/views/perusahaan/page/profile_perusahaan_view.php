﻿<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once(APPPATH .'views/include/perusahaan/inc_style.php'); ?>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('perusahaan/header'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('perusahaan/left_sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Profil</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('perusahaan/admin'); ?>">Home</a></li>
                        <li class="breadcrumb-item active">Profil</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
               
               <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            
                            <div class="card-body">
                                <?php foreach ($data_perusahaan as $dt_perusahaan): ?>
                                    <form action="<?php echo base_url('perusahaan/action_update_data_perusahaan'); ?>" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="kode_perusahaan" class="form-control" value="<?php echo $dt_perusahaan->Kode_Perusahaan ?>">
                                        <div class="form-body">
                                            
                                            <?php if (isset($_SESSION['sendmessage'])): ?>
                                                <div class="alert alert-success" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                    </button>
                                                    <?php echo $_SESSION['sendmessage'] ?>
                                                </div>
                                            <?php endif ?>
                                            <?php if (isset($_SESSION['error_data'])): ?>
                                                <div class="alert alert-warning" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                    </button>
                                                    <?php echo $_SESSION['error_data'] ?>
                                                </div>
                                            <?php endif ?>

                                            <h3 class="card-title">Profil Perusahaan</h3>
                                            <hr>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-success">
                                                        <label class="control-label">Nama Perusahaan</label>
                                                        <input type="text" name="nama_perusahaan" id="inputNama_perusahaan" class="form-control" value="<?php echo $dt_perusahaan->Nama_Perusahaan ?>">
                                                        <!-- <small class="form-control-feedback"> Select your gender </small>  -->
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Nama Pengguna</label>
                                                        <input type="text" onkeypress="return isCharKey(event);" name="nama_pengguna" id="input" class="form-control" value="<?php echo $dt_perusahaan->Nama_Pengguna ?>">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Nomor Telepon</label>
                                                        <input type="text" name="nomor_telepon" id="inputNomor_telepon" class="form-control" value="<?php echo $dt_perusahaan->Nomor_Telepon ?>" onkeypress="return isNumberKey(event);">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Alamat Email</label>
                                                        <input type="email" name="alamat_email" id="inputAlamat_email" class="form-control" readonly value="<?php echo $dt_perusahaan->Alamat_Email ?>">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Nomor Telepon Kantor</label>
                                                        <input type="text" name="nomor_telepon_kantor" id="input_nomor_telepon_kantor" class="form-control" value="<?php echo $dt_perusahaan->Nomor_Tlp_kantor ?>" onkeypress="return isNumberKey(event);">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Alamat Website</label>
                                                        <input type="text" name="alamat_website" id="input_alamat_website" class="form-control" value="<?php echo $dt_perusahaan->Website ?>">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->

                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Industri</label>
                                                        <select name="industri" id="inputindustri" class="form-control" required="required">
                                                            <?php if (!empty($dt_perusahaan->Industri)): ?>
                                                                <option value="<?php echo $dt_perusahaan->Industri ?>" selected><?php echo $dt_perusahaan->Industri ?></option>
                                                            <?php else: ?>
                                                                <option value="" selected>-  Pilih Industri -</option>
                                                            <?php endif ?>
                                                            
                                                            <option value="Agrikultural / Perkebunan / Peternakan Unggas / Perikanan"> Agrikultural / Perkebunan / Peternakan Unggas / Perikanan </option>
                                                             <option value="Akuntansi / Audit / Pelayanan Pajak"> Akuntansi / Audit / Pelayanan Pajak </option>
                                                             <option value="Asuransi"> Asuransi </option>
                                                             <option value="Automobil / Mesin Tambahan Automotif / Kendaraan" > Automobil / Mesin Tambahan Automotif / Kendaraan </option>
                                                             <option value="Bahan Kimia / Pupuk / Pestisida" > Bahan Kimia / Pupuk / Pestisida </option>
                                                             <option value="BioTeknologi / Farmasi / Riset klinik"> BioTeknologi / Farmasi / Riset klinik </option>
                                                             <option value="Call Center / IT-Enabled Services / BPO"> Call Center / IT-Enabled Services / BPO </option>
                                                             <option value="Elektrikal &amp; Elektronik"> Elektrikal &amp; Elektronik </option>
                                                             <option value="Hiburan / Media"> Hiburan / Media </option>
                                                             <option value="Hotel / Pariwisata"> Hotel / Pariwisata </option>
                                                             <option value="Hukum / Legal"> Hukum / Legal </option>
                                                             <option value="Ilmu Pengetahuan &amp; Teknologi "> Ilmu Pengetahuan &amp; Teknologi  </option>
                                                             <option value="Industri Berat / Mesin / Peralatan"> Industri Berat / Mesin / Peralatan </option>
                                                             <option value="Jual Beli Saham / Sekuritas"> Jual Beli Saham / Sekuritas </option>
                                                             <option value="Jurnalisme"> Jurnalisme </option>
                                                             <option value="Kayu / Fiber / Kertas"> Kayu / Fiber / Kertas </option>
                                                             <option value="Keamanan / Penegak hukum"> Keamanan / Penegak hukum </option>
                                                             <option value="Kelautan / Aquakultur"> Kelautan / Aquakultur </option>
                                                             <option value="Kesehatan / Medis "> Kesehatan / Medis  </option>
                                                             <option value="Komputer / Teknik Informatika (Perangkat Keras)" > Komputer / Teknik Informatika (Perangkat Keras) </option>
                                                             <option value="Komputer / Teknik Informatika (Perangkat Lunak)"> Komputer / Teknik Informatika (Perangkat Lunak) </option>
                                                             <option value="Konstruksi / Bangunan / Teknik"> Konstruksi / Bangunan / Teknik </option>
                                                             <option value="Konsultasi (Bisnis &amp; Manajemen)"> Konsultasi (Bisnis &amp; Manajemen) </option>
                                                             <option value="Konsultasi (IT, Ilmu Pengetahuan, Teknis &amp; Teknikal)"> Konsultasi (IT, Ilmu Pengetahuan, Teknis &amp; Teknikal) </option>
                                                             <option value="Layanan Umum / Tenaga Penggerak"> Layanan Umum / Tenaga Penggerak </option>
                                                             <option value="Lingkungan / Kesehatan / Keamanan"> Lingkungan / Kesehatan / Keamanan </option>
                                                             <option value="Luar Angkasa / Aviasi / Pesawat Terbang" > Luar Angkasa / Aviasi / Pesawat Terbang </option>
                                                             <option value="Makanan &amp; Minuman / Katering / Rumah Makan"> Makanan &amp; Minuman / Katering / Rumah Makan </option>
                                                             <option value="Manajemen / Konsulting HR"> Manajemen / Konsulting HR </option>
                                                             <option value="Manufaktur / Produksi"> Manufaktur / Produksi </option>
                                                             <option value="Minyak / Gas / Petroleum"> Minyak / Gas / Petroleum </option>
                                                             <option value="Olahraga"> Olahraga </option>
                                                             <option value="Organisasi Nirlaba / Pelayanan Sosial / LSM"> Organisasi Nirlaba / Pelayanan Sosial / LSM </option>
                                                             <option value="Pakaian"> Pakaian </option>
                                                             <option value="Pameran / Manajemen acara / PIKP"> Pameran / Manajemen acara / PIKP </option>
                                                             <option value="Pelayanan Arsitek / Desain Interior"> Pelayanan Arsitek / Desain Interior </option>
                                                             <option value="Pelayanan Perbaikan &amp; Pemeliharaan"> Pelayanan Perbaikan &amp; Pemeliharaan </option>
                                                             <option value="Pemerintahan / Pertahanan"> Pemerintahan / Pertahanan </option>
                                                             <option value="Pendidikan "> Pendidikan  </option>
                                                             <option value="Perawatan / Kecantikan / Fitnes"> Perawatan / Kecantikan / Fitnes </option>
                                                             <option value="Perbankan / Pelayanan Keuangan" Perbankan / Pelayanan Keuangan </option>
                                                             <option value="Percetakan / Penerbitan"> Percetakan / Penerbitan </option>
                                                             <option value="Periklanan / Marketing / Promosi / Hubungan Masyarakat" Periklanan / Marketing / Promosi / Hubungan Masyarakat </option>
                                                             <option value="Permata / Perhiasan"> Permata / Perhiasan </option>
                                                             <option value="Perpustakaan / Museum"> Perpustakaan / Museum </option>
                                                             <option value="Pertambangan"> Pertambangan </option>
                                                             <option value="Polymer / Plastik / Karet / Ban"> Polymer / Plastik / Karet / Ban </option>
                                                             <option value="Produk Konsumen / Barang konsumen yang bergerak cepat"> Produk Konsumen / Barang konsumen yang bergerak cepat </option>
                                                             <option value="Properti / Real Estate"> Properti / Real Estate </option>
                                                             <option value="R&amp;D"> R&amp;D </option>
                                                             <option value="Retail / Merchandise"> Retail / Merchandise </option>
                                                             <option value="Semikonduktor / Fabrikasi"> Semikonduktor / Fabrikasi </option>
                                                             <option value="Seni / Desain / Fashion" > Seni / Desain / Fashion </option>
                                                             <option value="Tekstil  / Garment"> Tekstil  / Garment </option>
                                                             <option value="Telekomunikasi"> Telekomunikasi </option>
                                                             <option value="Tembakau"> Tembakau </option>
                                                             <option value="Transportasi / Logistik"> Transportasi / Logistik </option>
                                                             <option value="Travel / Pariwisata"> Travel / Pariwisata </option>
                                                             <option value="Umum &amp; Grosir"> Umum &amp; Grosir </option>
                                                             <option value="Lainnya">Lainnya</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Negara</label>
                                                        <select name="negara" id="inputnegara" class="form-control negara" required="required">
                                                            <?php if (!empty($dt_perusahaan->Negara)): ?>
                                                                <option value="<?php echo $dt_perusahaan->Negara ?>" selected><?php echo $dt_perusahaan->Negara ?></option>
                                                                <option value="">Pilih Negara</option>
                                                            <?php else: ?>
                                                                <option value="" selected>Pilih Negara</option>
                                                            <?php endif ?>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->

                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Logo Perusahaan</label>
                                                        <input type="file" name="img_perusahaan" class="form-control" value="">
                                                        <input type="hidden" name="img_perusahaan_old" value="<?php echo $dt_perusahaan->Logo_perusahaan ?>" placeholder="">
                                                       <!--  <small><label>Setelah mengubah logo perusahaan </label>
                                                        </small> -->
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->


                                            <h3 class="box-title m-t-40">Alamat Perusahaan</h3>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12 ">
                                                    <!-- <div class="form-group" id="locationField">
                                                        <label>Alamat</label>
                                                        <input type="text" name="alamat_perusahan" id="alamat_perusahan" class="form-control" value="<?php echo $dt_perusahaan->Alamat_perusahaan ?>" onFocus="geolocate()">
                                                    </div> -->
                                                    <div class="form-group" id="locationField">
                                                        <label>Alamat</label>
                                                        <div id="locationField">
                                                          <input id="autocomplete" placeholder="Enter your address" onFocus="geolocate()" type="text" value="<?php echo $dt_perusahaan->Alamat_perusahaan ?>" name="alamat_perusahan" class="form-control"></input>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Latar belakang perusahaan</label>
                                                        <textarea name="deskripsi_perusahaan" id="inputDeskripsi_perusahaan" class="summernote form-control" rows="5" required="required">
                                                            <?php echo $dt_perusahaan->Deskripsi_perusahaan ?>
                                                        </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                            <a class="btn btn-inverse" href="<?php echo base_url('perusahaan/admin/'); ?>" role="button">Cancel</a>
                                        </div>
                                    </form>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php $this->load->view('perusahaan/footer'); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAONVpQftnLIjYuc_Q8PcTeSenFKJvcahg&libraries=places&callback=initAutocomplete" async defer></script>
     <?php require_once(APPPATH .'views/include/perusahaan/inc_script.php'); ?>

      <!-- <script type="text/javascript">
      var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
      };

      function initialize() {
        var options = {
        types: ['(cities)'],
        componentRestrictions: {country: "id"}
        };

        var input = document.getElementById('alamat_perusahaan');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
      }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script> -->

    <script type="text/javascript">
         var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
          };

          jQuery(document).ready(function() {

        $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
            });

            $('.inline-editor').summernote({
                airMode: true
            });

        });

        var base_url = window.location.origin;
        $.ajax({
            type: "GET",
            url: base_url+"/assets_pelamar/countries.json",
            dataType: "json",
            success: function (data_negara) {
                $.each(data_negara, function(i, value) {
                    $('#inputNegara, .negara').append($('<option>').text(value).attr('value', value));
                });
            },
            error: function (result) {
            }
        });

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode
            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        }

        function isCharKey(event) {
           var value = String.fromCharCode(event.which);
           var pattern = new RegExp(/[a-zåäö ]/i);
           return pattern.test(value);
        }
     </script>

     <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
     
</body>

</html>