<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once(APPPATH .'views/include/perusahaan/inc_style.php'); ?>

</head>


<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->

    <div class="container" id="pdf_create">
      <?php //echo '<pre>';print_r($data_apply); echo '</pre>';?>
      <?php foreach ($data_apply as $dt_apply): ?>
      <?php 
        $info_lainnya = $this->db->query("SELECT * FROM Tbl_Info_Tambahan WHERE kode_pelamar = '$dt_apply->kode_pelamar'")->row();
        $info_pengalaman =$this->db->query("SELECT TOP 1 * FROM Tbl_Pengalaman_Kerja WHERE kode_pelamar = '$dt_apply->kode_pelamar' ORDER BY kode_pengalaman DESC")->result();
        $info_pendidikan = $this->db->query("SELECT kualifikasi, tahun_lulusan from Tbl_Riwayat_Pendidikan where kode_pelamar = '$dt_apply->kode_pelamar' order by kode_pendidikan desc")->row();
        $info_lowongan  = $this->db->query("SELECT * FROM Tbl_Lowongan WHERE kode_lowongan = '$dt_apply->kode_lowongan' AND kode_perusahaan = '$dt_apply->kode_perusahaan'")->row();

        $det_pengalaman   = $this->db->query("SELECT * FROM Tbl_Pengalaman_Kerja WHERE kode_pelamar = '$dt_apply->kode_pelamar' ORDER BY kode_pengalaman DESC")->result();
        $det_pendidikan   = $this->db->query("SELECT * from Tbl_Riwayat_Pendidikan where kode_pelamar = '$dt_apply->kode_pelamar' order by kode_pendidikan DESC")->result();
        $det_keterampilan = $this->db->query("SELECT * from Tbl_Keterampilan where kode_pelamar = '$dt_apply->kode_pelamar'")->result();
        $det_info_lainnya = $this->db->query("SELECT * FROM Tbl_Info_Tambahan WHERE kode_pelamar = '$dt_apply->kode_pelamar'")->result();


      ?>
      <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12 col-xlg-9 col-md-7">
                
                <div class="card">

                    <div class="card-body">
                      <h2 class="pull-right"><?php echo $nama_lowongan->nama_lowongan ?></h2>
                      <div class="row">                       
                        <!-- Comment Row -->

                        <div class="d-flex flex-row comment-row">
                            <div class="p-2">
                                <img src="<?php echo base_url('assets_storage/img_avatar/'.$dt_apply->img_profile); ?>" class=" img-rounded" width="90" />
                            </div>
                            <div class="comment-text w-90">
                                <h5><?php echo $dt_apply->nama_pelamar ?>
                                  <?php 
                                      $this->load->model('Lib_model');
                                      $m_lib = new Lib_model();

                                      $result = $m_lib->calculate_age($dt_apply->kode_pelamar); 
                                      echo ("<span style='font-size: 14px; font-weight: 600;'> / ".$result."<span>");
                                  ?>
                                </h5>   
                                <div class="comment-footer">
                                    <div class="comment-footer">
                                        <?php foreach ($info_pengalaman as $f_penggalaman): ?>
                                            <span class="date"><strong><?php echo $f_penggalaman->posisi_kerja ?></strong></span><br>
                                            <label><?php echo $f_penggalaman->nama_perusahaan ?></label>
                                        <?php endforeach ?>
                                        
                                        <br>
                                        <span class="" style="font-size: 15px;"> <i class="fa fa-phone"></i> <?php echo $dt_apply->nomor_telepon ?> | <i class="fa fa-envelope"></i> <?php echo $dt_apply->alamat_email ?> | <i class="fa fa-dollar"></i> 
                                          <?php if (!empty($info_lainnya->mata_uang)): ?>
                                            <?php echo $info_lainnya->mata_uang ?> - <?php if (is_numeric($info_lainnya->gaji)) { echo number_format($info_lainnya->gaji); } else { echo $info_lainnya->gaji; }  ?>
                                          <?php else: ?>
                                            Kosong
                                          <?php endif ?>
                                          
                                          <i class="fa fa-map-marker"></i> <?php echo $dt_apply->kota.', ',$dt_apply->provinsi ?></span> 
                                          <br>
                                          <span class="" style="font-size: 15px;"> <i class="fa fa-map"></i> Pilih Lokasi <strong> <?php echo $dt_apply->wilayah_daftar ?></strong>  </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Comment Row -->
              
                      </div>
                          <hr>
                          <h4 class="font-medium m-t-30">Mengapa mempekerjakan saya ?</h4>
                          <p class="m-t-30"><?php echo '" '.$dt_apply->deskripsi_promosi. '"' ?></p>

                          <h4 class="font-medium m-t-30">Pengalaman</h4>
                          <?php foreach ($det_pengalaman as $dt_pengalaman): ?>
                            <!-- Comment Row -->
                              <div class="d-flex flex-row comment-row">
                                  <div class="p-2">
                                      <p><?php echo $dt_pengalaman->bulan_start_kerja ?>, <?php echo $dt_pengalaman->tahun_start_kerja ?></p> - 
                                      <p>
                                          <?php if (empty($dt_pengalaman->masih_kerja)): ?>
                                              <?php echo $dt_pengalaman->bulan_start_selesai ?>, <?php echo $dt_pengalaman->tahun_start_selesai ?>
                                          <?php else: ?>
                                              Sampai Sekarang
                                          <?php endif ?>
                                      </p>
                                  </div>
                                  <div class="comment-text w-90">
                                      <h5><?php echo $dt_pengalaman->posisi_kerja ?></h5>
                                      <div class="comment-footer">
                                          <span class="date"> <i class="fa fa-building"></i> <?php echo $dt_pengalaman->nama_perusahaan ?> | <?php echo $dt_pengalaman->negara ?></span>
                                          
                                      </div>
                                      <table class="table">
                                          <tbody>
                                              <tr>
                                                  <td>Industri</td>
                                                  <td><?php echo $dt_pengalaman->industri ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Spesialisasi</td>
                                                  <td><?php echo $dt_pengalaman->spesialisasi ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Bidang Pekerjaan</td>
                                                  <td><?php echo $dt_pengalaman->bidang_pekerjaan ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Jabatan</td>
                                                  <td><?php echo $dt_pengalaman->jabatan ?></td>
                                              </tr>
                                              <tr>
                                                  <td>Gaji Perbulan</td>
                                                  <td><?php echo $dt_pengalaman->mata_uang ?> - <?php if (is_numeric($dt_pengalaman->gaji)) { echo number_format($dt_pengalaman->gaji); } else { echo $dt_pengalaman->gaji; }  ?></td>
                                              </tr>
                                          </tbody>
                                      </table>
                                      <p class="m-b-5 m-t-10"><?php echo $dt_pengalaman->keterangan_kerja ?></p>
                                  </div>
                              </div>
                              <hr>
                            <!-- End Comment Row -->
                            <?php endforeach ?>
                          <hr>

                          <h4 class="font-medium m-t-30">Pendidikan</h4>
                          <?php foreach ($det_pendidikan as $dt_pendidikan): ?>                            
                            <!-- Comment Row -->
                            <div class="d-flex flex-row comment-row">
                                <div class="p-2">
                                    <p><?php echo $dt_pendidikan->bulan_wisuda ?> , <?php echo $dt_pendidikan->tahun_lulusan ?></p>
                                </div>
                                <div class="comment-text w-90">
                                    <h5><?php echo $dt_pendidikan->nama_universitas ?></h5>
                                    <div class="comment-footer">
                                        <span class="date"><?php echo $dt_pendidikan->kualifikasi ?> di <?php echo $dt_pendidikan->jurusan ?> | <?php echo $dt_pendidikan->lokasi ?></span>
                                        
                                    </div>
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td>Jurusan</td>
                                                <td><?php echo $dt_pendidikan->jurusan ?></td>
                                            </tr>
                                            <?php if ($dt_pendidikan->grade == "IPK"): ?>
                                                <tr>
                                                    <td><?php echo $dt_pendidikan->grade ?></td>
                                                    <td><?php echo $dt_pendidikan->nilai_asli ?> / <?php echo $dt_pendidikan->nilai_dari ?></td>
                                                </tr>
                                            <?php else: ?>
                                                <tr>
                                                    <td>Hasil Studi</td>
                                                    <td>
                                                        <?php if ($dt_pendidikan->grade == "Masih Belajar"): ?>
                                                            <span class="label label-info"><?php echo $dt_pendidikan->grade ?></span> 
                                                        <?php else: ?>
                                                            <span class="label label-danger"><?php echo $dt_pendidikan->grade ?></span> 
                                                        <?php endif ?>
                                                    </td>
                                                </tr>
                                            <?php endif ?>
                                            
                                            
                                        </tbody>
                                    </table>
                                    <p class="m-b-5 m-t-10"><?php echo $dt_pendidikan->informasi_tambahan ?></p>
                                </div>
                            </div>
                            <!-- End Comment Row -->
                            <?php endforeach ?>
                          <hr>

                          <h4 class="font-medium m-t-30">Keterampilan</h4>
                            <div class="form-group row">
                                <label for="keterampilan" class="col-sm-3 control-label">Keterampilan</label>
                                <?php foreach ($det_keterampilan as $dt_keterampilan): ?>
                                <div class="col-sm-9">
                                   <?php echo $dt_keterampilan->keterampilan ?>
                                </div>
                                <?php endforeach ?>
                            </div>
                          <hr>

                          <h4 class="font-medium m-t-30">Bahasa</h4>
                            <div class="form-group row">
                                <label for="bahasa" class="col-sm-3 control-label">Bahasa</label>
                                <div class="col-sm-9">
                                <?php foreach ($det_keterampilan as $dt_keterampilan): ?>
                                    <?php echo $dt_keterampilan->kuasai_bahasa ?>
                                <?php endforeach ?>
                                </div>
                            </div>
                          <hr>

                          <h4 class="font-medium m-t-30">Info Lain</h4>
                            <?php foreach ($det_info_lainnya as $dt_info): ?>
                              <table class="table table-hover">
                                  <tbody>
                                      <tr>
                                          <td>Gaji yang diharapkan</td>
                                          <td><?php echo $dt_info->mata_uang ." - "; ?> <?php if(is_numeric($dt_info->gaji)) { echo number_format($dt_info->gaji); } else { echo $dt_info->gaji; }  ?></td>
                                      </tr>
                                      <tr>
                                          <td>Lokasi kerja yang diharapkan</td>
                                          <td><?php echo $dt_info->lokasi_kerja_1 .", ". $dt_info->lokasi_kerja_2 .", ". $dt_info->lokasi_kerja_3?></td>
                                      </tr>
                                      <tr>
                                          <td>Informasi lainnya</td>
                                          <td><?php echo $dt_info->informasi_lainnya ?> </td>
                                      </tr>
                                  </tbody>
                              </table>
                              <?php endforeach ?>
                          <hr>

                          <h4 class="font-medium m-t-30">Profil Saya</h4>
                            <div class="card-body">
                              <table class="table table-hover">
                                  <tbody>
                                      <tr>
                                          <td>Jenis Kelamin</td>
                                          <td>
                                              <?php if (!empty($dt_apply->jenis_kelamin)): ?>
                                                  <?php echo $dt_apply->jenis_kelamin ?>
                                              <?php else: ?> <i>Not Set</i> <?php endif ?>
                                          </td>
                                      </tr> 
                                      <tr>
                                          <td>Alamat 1</td>
                                          <td>
                                              <?php if (!empty($dt_apply->alamat_1)): ?>
                                                  <?php echo $dt_apply->alamat_1 ?>
                                              <?php else: ?> <i>Not set</i> <?php endif ?>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>Alamat 2</td>
                                          <td>
                                              <?php if (!empty($dt_apply->alamat_2)): ?>
                                                  <?php echo $dt_apply->alamat_2 ?>
                                              <?php else: ?>
                                                  <i>Not set</i>
                                              <?php endif ?>
                                          </td>
                                      </tr>
                                       <tr>
                                          <td>Tanggal Lahir</td>
                                          <td>
                                              <?php echo $dt_apply->tanggal.' '. $dt_apply->bulan.' '. $dt_apply->tahun ?>
                                          </td>
                                      </tr>

                                      <?php if (!empty($dt_apply->identitas)): ?>
                                          <tr>
                                              <td>Identitas</td>
                                              <td><?php echo $dt_apply->identitas ?></td>
                                          </tr>
                                      <?php endif ?>
                                      
                                      <?php if (!empty($dt_apply->nomor_identitas)): ?>
                                          <tr>
                                              <td>Nomor Identitas</td>
                                              <td><?php echo $dt_apply->nomor_identitas ?></td>
                                          </tr>
                                      <?php endif ?>
                                      
                                  </tbody>
                              </table>
                          </div>
                          <hr>
                          
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
      <!-- Row -->

      <?php endforeach ?>

    </div>

    
     <?php require_once(APPPATH .'views/include/perusahaan/inc_script.php'); ?>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
     <script src="<?php echo base_url('assets/plugins/html2pdf/dist/html2pdf.bundle.min.js'); ?>" type="text/javascript"></script>
     <script type="text/javascript">
        $(document).ready(function() {
          var nama_pelamar = "<?php echo str_replace(' ','_',$dt_apply->nama_pelamar) ?>";
          var element = document.getElementById('pdf_create');
          // html2pdf(element);
          html2pdf(element, {
            margin:       0.2,
            filename:     nama_pelamar+'.pdf',
            html2canvas:  { dpi: 192, letterRendering: true },
            jsPDF:        { unit: 'in', format: 'a4', orientation: 'portrait' }
          });
          // var doc = new jsPDF();
          // var specialElementHandlers = {
          //       '#editor': function (element, renderer) {
          //           return true;
          //       }
          //   };
          //   doc.fromHTML($('#createpdf').html(), 15, 15, {
          //           'width': 170,
          //               'elementHandlers': specialElementHandlers
          //   });
          //   doc.save('sample-file.pdf');
        });
        // var doc = new jsPDF();
        //     var specialElementHandlers = {
        //         '#editor': function (element, renderer) {
        //             return true;
        //         }
        //     };

        //     $('#cmd').click(function () {
        //         doc.fromHTML($('#createpdf').html(), 15, 15, {
        //             'width': 170,
        //                 'elementHandlers': specialElementHandlers
        //         });
        //         doc.save('sample-file.pdf');
        //     });
     </script>
</body>

</html>