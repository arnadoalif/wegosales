<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once(APPPATH .'views/include/perusahaan/inc_style.php'); ?>
    <style type="text/css">
        .bootstrap-tagsinput {
            width: 46em;
        }

        #requirement {
            display: none;
        }

        .card-outline-info .card-header {
            background: #8d6c2c;
            border-color: #8d6c2c;
        }

        .card-info {
            background: #b80000;
            border-color: #b80000;
        }

        a {
            color: #8d6c2c;
        }
    </style>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('perusahaan/header'); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('perusahaan/left_sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Tambah Lowongan</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('perusahaan/admin'); ?>">Home</a></li>
                        <li class="breadcrumb-item active">Tambah Lowongan</li>
                    </ol>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                

                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        
                        <?php if (isset($_SESSION['sendmessage'])): ?>
                            <div class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                                <?php echo $_SESSION['sendmessage'] ?>
                            </div>
                        <?php endif ?>
                        <?php if (isset($_SESSION['error_data'])): ?>
                            <div class="alert alert-warning" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                </button>
                                <?php echo $_SESSION['error_data'] ?>
                            </div>
                        <?php endif ?>

                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Buka Lowongan</h4>
                            </div>
                            <div class="card-body">
                                <form action="<?php echo base_url('perusahaan/action_tambah_lowongan')?> " method="POST" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="form-body">
                                        <h3 class="box-title">Rincian pekerjaan</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <div class="row">
                                            <div class="col-sm-offset-1 col-sm-10">

                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Nama Lowongan *</label>
                                                    <input type="hidden" name="kode_perusahaan" id="inputKode_" class="form-control" value="<?php echo $this->session->userdata('kode_perusahaan') ?>">
                                                    <div class="col-sm-9">
                                                        <?php if (isset($_SESSION['nama_lowongan'])): ?>
                                                            <input type="text" name="nama_lowongan" value="<?php echo $_SESSION['nama_lowongan'] ?>" class="form-control" id="input-nm_lowongan" required>
                                                        <?php else: ?>
                                                            <input type="text" name="nama_lowongan" class="form-control" id="input-nm_lowongan" required>
                                                        <?php endif ?>
                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tanggal Tutup Lowongan*</label>
                                                    <div class="col-sm-9">
                                                        <?php if (isset($_SESSION['tanggal_tutup'])): ?>
                                                            <input type="date" name="tanggal_tutup" value="<?php echo $_SESSION['tanggal_tutup'] ?>" class="form-control" id="input-tgl_tutup" required>
                                                        <?php else: ?>
                                                            <input type="date" name="tanggal_tutup" class="input-sm form-control" id="input-tgl_tutup" required>
                                                        <?php endif ?>
                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Pekerjaan Jenis *</label>
                                                    <div class="col-sm-9">
                                                        <select name="pekerjaan_jenis" id="inputpekerjaan_jenis" class="form-control" required="required">
                                                            <option value="Kontrak">Kontak</option>
                                                            <option value="Waktu Penuh" selected>Waktu Penuh</option>
                                                            <option value="Magang">Magang</option>
                                                            <option value="Paruh Waktu">Paruh Waktu</option>
                                                            <option value="Sementara">Sementara</option>
                                                        </select>
                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Posisi Tingkat *</label>
                                                    <div class="col-sm-9">
                                                        <select name="posisi_spesialis" id="inputPosisi_spesialis" class="form-control" required="required">
                                                            <option value="" selected>- Pilih posisi tingkat -</option>
                                                            <option value="CEO/GM/Direktur/Manajer Senior"> CEO/GM/Direktur/Manajer Senior </option>
                                                            <option value="Manajer/Asisten Manajer"> Manajer/Asisten Manajer </option>
                                                            <option value="Supervisor/Koordinator"> Supervisor/Koordinator </option>
                                                            <option value="Pegawai (non-manajemen &amp; non-supervisor)"> Pegawai (non-manajemen &amp; non-supervisor) </option>
                                                            <option value="Lulusan baru/Pengalaman kerja kurang dari 1 tahun"> Lulusan baru/Pengalaman kerja kurang dari 1 tahun </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Gaji Bulanan</label>
                                                    <!-- <div class="col-sm-2">
                                                        <select name="mata_uang" id="inputPosisi_spesialis" class="form-control" required="required">
                                                            <option value=""></option>
                                                        </select>
                                                    </div> -->
                                                    <div class="col-sm-3">
                                                        <input type="text" name="salary_min" placeholder="Min" id="inputSalary" class="form-control" value="" required="required" onkeypress="return isNumberKey(event);">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="salary_max" placeholder="Max" id="inputSalary" class="form-control" value="" onkeypress="return isNumberKey(event);">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3"></label>
                                                    <div class="col-sm-9">
                                                        <input type="checkbox" name="show_salary" id="basic_checkbox_2" class="filled-in" value="check_salary"/>
                                                        <label for="basic_checkbox_2"> Tampilan gaji pada iklan untuk menarik calon yang tepat</label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <h3 class="box-title">Persyaratan Kerja (untuk pencocokan kadidat yang lebih baik)</h3>
                                        <hr class="m-t-0 m-b-40">
                                        <div class="row">
                                            <div class="col-sm-offset-1 col-sm-10">

                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Tingkat Pendidikan</label>
                                                    <div class="col-sm-9">
                                                        <select name="tinggkat_pendidikan" id="inputtinggkat_pendidikan" class="form-control" required="required">
                                                            <option value="" selected>- Pilih Tingkat Pendidikan -</option>
                                                            <option value="SMU">SMU</option>
                                                            <option value="Diploma">Diploma</option>
                                                            <option value="Sarjana">Sarjana</option>
                                                            <option value="Pasca Sarjana">Pasca Sarjana</option>
                                                            <option value="Gelar Doktor">Gelar Doktor</option>
                                                        </select>
                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Bidang Studi</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="bidang_studi" class="form-control" value="" placeholder="Bidang Studi Lowongan">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Pengalaman</label>
                                                    <div class="col-sm-9">
                                                        <select name="pengalaman" id="inputpengalaman" class="form-control" required="required">
                                                            <option value="" selected>- min Tahun dari pengalaman -</option>
                                                            <?php for ($i = 1; $i <= 24; $i++): ?>
                                                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                                            <?php endfor ?>
                                                                <option value="Lebih Dari 24">Lebih Dari 24</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Keahlian</label>
                                                    <div class="col-sm-9">
                                                        <!-- <input type="text" value="" data-role="tagsinput" class="form-control" style="width: 629px;"> -->
                                                        <input type="text" name="keahlian" value="" class="form-control" data-role="tagsinput" placeholder="Tambah Keahlian" style="width: 629px;"><br>
                                                        <small class="form-control-feedback text-danger"><i> Tambah pisah dengan tanda (,) </i></small>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <h3 class="box-title">Iklan Lowongan</h3>
                                        <hr class="m-t-0 m-b-40">
                                         <div class="row">
                                            <div class="col-sm-offset-1 col-sm-10">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Image Iklan <br><span style="color: red; font-size: 12px;"><i>Lampiran ini akan ditampilkan di halaman depan, size min 1mb, type [gif|jpg|png|jpeg] (wajib diisi)</i></span></label>
                                                    <div class="col-sm-5">
                                                        <input type="file" name="gambar_iklan" class="form-control" accept="image/*" onchange="loadFile(event)">
                                                        <img id="output" style="max-width: 25%;" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <h3 class="box-title">Deskripsi pekerjaan *</h3>
                                        <hr class="m-t-0 m-b-40">
                                         <div class="row">
                                            <div class="col-sm-offset-1 col-sm-10">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Deskripsi Pekerjaan <span style="color: red; font-size: 12px;"><i>Deskripsi Pekerjaan (Gambaran Pekerjaan, Persyaratan) serta catatan penting yang dianggap perlu</i></span></label>
                                                    <div class="col-sm-9">
                                                        
                                                        <?php if (isset($_SESSION['deskripsi_pekerjaan'])): ?>
                                                            <textarea name="deskripsi_pekerjaan" id="input" class="form-control summernote" rows="3" required="required">
                                                                <?php echo $_SESSION['deskripsi_pekerjaan'] ?>
                                                            </textarea>
                                                        <?php else: ?>
                                                            <textarea name="deskripsi_pekerjaan" id="input" class="form-control summernote" rows="3" required="required">
                                                                
                                                            </textarea>
                                                        <?php endif ?>
                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <h3 class="box-title">Kategorikan Lowongan *</h3>
                                        <hr class="m-t-0 m-b-40">
                                         <div class="row">
                                            <div class="col-sm-offset-1 col-sm-10">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Kategorikan lowongan <span style="color: red; font-size: 12px;"><i>data lowongan akan di kelompokan berdasarkan pilihan provinsi pada halaman depan pencarian lowongan, apa bila tidak di pilih maka lowongan tidak akan dikategorikan dan tidak dapat di publik</i></span></label>
                                                    <div class="col-sm-9">
                                                        
                                                        <select name="kategori_lowongan[]" required class="select2 m-b-10 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose">
                                                            <?php foreach ($data_provinsi as $dt_provinsi): ?>
                                                                <option value="<?php echo $dt_provinsi->nama_provinsi ?>"><?php echo $dt_provinsi->nama_provinsi ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <div class="card card-inverse card-info">
                                        <div class="card-header">
                                            <h4 class="m-b-0 text-white">Metode Requirement</h4></div>
                                        <div class="card-body">
                                            <h3 class="card-title"></h3>
                                            <div class="col-sm-9">
                                                <input type="checkbox" name="show_requirement" id="show_requirement" class="filled-in" value="show_requirement"/>
                                                <label for="show_requirement" class="text-white"> Tambahkan metode requirement</label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div id="requirement">
                                        <h3 class="box-title">Metode Requirement *</h3>
                                        <hr class="m-t-0 m-b-40">
                                         <div class="row">
                                            <div class="col-sm-offset-1 col-sm-10">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Pilih Metode Requirement</label>
                                                    <div class="col-sm-9">
                                                        <select name="tipe_metode_rekut" class="form-control" id="metode_select">
                                                            <option value="" selected>Pilih Metode Rekutmen</option>
                                                            <option value="1">Apprenticeship Program</option>
                                                            <option value="2">Test Case</option>
                                                            <option value="3">Interview</option>
                                                            <option value="4">Test & Interview</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Upload Gambar</i></span></label>
                                                    <div class="col-sm-9">
                                                        <input type="file" name="gamabaran_requirement" class="form-control" accept="image/*" onchange="loadFile(event)">
                                                        <img id="output" style="max-width: 25%;" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Deskripsi Requirement <span style="color: red; font-size: 12px;"><i>Tulisakan deskripsi / metode requirement dilowongan</i></span></label>
                                                    <div class="col-sm-9">
                                                        
                                                        <?php if (isset($_SESSION['deskripsi_requirement'])): ?>
                                                            <textarea name="deskripsi_requirement" id="input" class="form-control summernote" rows="3" required="required">
                                                                <?php echo $_SESSION['deskripsi_requirement'] ?>
                                                            </textarea>
                                                        <?php else: ?>
                                                            <textarea name="deskripsi_requirement" id="input" class="form-control summernote" rows="3" required="required">
                                                                
                                                            </textarea>
                                                        <?php endif ?>
                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    </div>
                                    <hr>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                        <button type="button" class="btn btn-inverse">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6"> </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php $this->load->view('perusahaan/footer'); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
     <?php require_once(APPPATH .'views/include/perusahaan/inc_script.php'); ?>
     <script type="text/javascript">
         var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
          };

          jQuery(document).ready(function() {

            $('.summernote').summernote({
                height: 350, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: false // set focus to editable area after initializing summernote
            });

            $('.inline-editor').summernote({
                 airMode: true
            });

            $(document).on('click', 'input[name="show_requirement"]', function () {
                var checked = $(this).prop('checked');
                if (checked) {
                    $("#requirement").show();
                    $("#metode_select").attr('required','required');
                   
                } else {
                    $("#requirement").hide();
                    $("#metode_select").removeAttr('required','required');
                }
            });

            $(".select2").select2();
            $('.selectpicker').selectpicker();

        });

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode
            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        }

        function isCharKey(event) {
           var value = String.fromCharCode(event.which);
           var pattern = new RegExp(/[a-zåäö ]/i);
           return pattern.test(value);
        }
     </script>
</body>

</html>