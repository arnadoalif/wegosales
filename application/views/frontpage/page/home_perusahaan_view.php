<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once(APPPATH .'views/include/front_pelamar/inc_front_style.php'); ?>
    <style type="text/css">
        .text-danger {
            color: #b80000 !important;
        }

         .btn-info, .btn-info.disabled {
            background: #b80000;
            border: 1px solid #8d6c2c;
        }

        .contac-top {
            border-radius: 21px;
            position: fixed;
            bottom: 100px;
            right: 31px;
            margin: 0 auto;
            z-index: 12;
            padding: 10px;
            background: #093055;
            color: #fff;
            width: 327px;
            font-size: 12px;
        }

        #google_translate_element {
            position: absolute;
            margin: 0 auto;
            left: 7em;
        }

        @media (min-width: 320px) and (max-width: 480px) {
            .hidden-sm {
                display: none;
            }

            .contac-top {
                border-radius: 21px;
                position: fixed;
                bottom: 30px;
                right: 31px;
                margin: 0 auto;
                z-index: 12;
                padding: 10px;
                background: #093055;
                color: #fff;
                width: 327px;
                font-size: 12px;
            }

            #google_translate_element {
                display: none;
            } 
        }

    </style>
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topheader" id="top">
            <div class="fix-width">
                <nav class="navbar navbar-expand-md navbar-light p-l-0">
                    
                    <!-- Logo will be here -->
                    <a class="navbar-brand" href="<?php echo base_url(''); ?>"><img src="<?php echo base_url('assets_front/images/logo.png') ?> " alt="Wegosales" /></a>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                    <!-- This is the navigation menu -->
                     <div id="google_translate_element"></div>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav ml-auto stylish-nav">
                            
                            <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('pilih-wilayah') ?>" >Cari Lowongan |</a> </li>
                            <?php if (empty($this->session->userdata('kode_perusahaan'))): ?>
                                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('perusahaan/login') ?>" >Masuk</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('perusahaan/register') ?>" >Mendaftar</a> </li>
                            <?php else: ?>
                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->nama_perusahaan ?></a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> 
                                        <a class="dropdown-item" href="<?php echo base_url('perusahaan/admin/profil') ?>">Profil Perusahaan</a> 
                                        <a class="dropdown-item" href="<?php echo base_url('perusahaan/out') ?>">Logout</a> 
                                    </div>
                                </li>
                            <?php endif ?>
                            
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="fix-width">
                    <div class="row banner-text justify-content-center">
                        <div class="col-md-8 m-t-20 text-center">
                            <h1>Anda tertarik di <span class="text-danger">Wegosales</span> </h1>
                            <p class="subtext"><span class="font-medium">Wegosales </span> Hadir untuk mencarikan kadidat untuk anda</p>
                            <!-- <div class="down-btn"> 
                                <a href="<?php echo base_url('pelamar/register') ?>" class="btn btn-info m-b-10">DAFTAR SEKARANG</a> 
                                <a href="" class="btn btn-success m-b-10">Cari Lowongan</a> 
                            </div> -->
                        </div>    
                        <div class="col-md-12 text-center">
                            <div class="hero-banner"> <img src="<?php echo base_url('assets_front/images/we%20go%20sales.png') ?>" alt="Gambar Perusahaan"  class="img-fluid"/> </div>
                        </div>    
                        
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Featured Section -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12" id="demos">
                        <div class="text-center" > 

                            <h2 class="display-7">Begitu Mudahnya <span class="text-danger">Mencari Kadidat</span> Terbaik Di <span class="text-danger">Wegosales.com</span> ?</h2>

                        </div>
                        <div class="col-lg-12">
                            <div class="row text-center">
                                <div class="col-md-3">
                                    <img style="max-width: 60%;" src="<?php echo base_url('assets_front/images/icon_front_perusahaan/registrasi.png'); ?>" alt="Registrasi Perusahaan" class="img-responsive" />
                                    <h5 class="p-20 text-danger">Registrasi Perusahaan</h5>
                                </div>
                                <div class="col-md-3">
                                    <img style="max-width: 60%;" src="<?php echo base_url('assets_front/images/icon_front_perusahaan/buat_iklan.png'); ?>" alt="Buat Iklan Lowongan Sesuai Dengan Kebutuhan" class="img-responsive" />
                                    <h5 class="p-20 text-danger">Buat Iklan Lowongan Sesuai Dengan Kebutuhan</h5>
                                </div>
                                <div class="col-md-3">
                                    <img style="max-width: 60%;" src="<?php echo base_url('assets_front/images/icon_front_perusahaan/pilih_model_seleksi.png'); ?>" alt="Pilih Model Seleksi Dan Posting Job" class="img-responsive" />
                                    <h5 class="p-20 text-danger">Pilih Model Seleksi Dan Posting Job</h5>
                                </div>
                                <div class="col-md-3">
                                    <img style="max-width: 60%;" src="<?php echo base_url('assets_front/images/icon_front_perusahaan/seleksi_cv_kadidat.png'); ?>" alt="Seleksi CV Kadidat Terbaik" class="img-responsive" />
                                    <h5 class="p-20 text-danger">Seleksi CV Kadidat Terbaik</h5> 
                                </div>
                                
                            </div>
                        </div>

                       
                    </div>
                </div>

                <div class="contac-top">
                    <span class="text-white">Catatan : Wegosales masih dalam pengembangan jika memiliki kendala dalam menggunakan wegosales bisa hubungi <br>0821-3798-4316 (Alif) <br> <a href="mailto:admin@wegosales.com" >admin@wegosales.com</a> <br>
                    Jam kerja: 9.00am – 5.00pm (Senin – Sabtu)
                    </span>
                </div>

                <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
                <footer class="footer row" style="background: #093055;">
                    <div class="fix-width">
                        <div class="row">
                            <div class="col-lg-3 col-md-6"><img src="<?php echo base_url('assets_front/images/logo.png'); ?>" alt="Wegosales" />
                                <p class="m-t-30">
                                    <span class="text-white">Wegosales.com
                                    </span> Situs website online yang menyediakan informasi lowongan kerja</p>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <p class="m-t-30">
                                        <!-- <span class="text-white">Catatan : Wegosales masih dalam pengembangan jika memiliki kendala dalam menggunakan wegosales bisa hubungi <br>0821-3798-4315 (Alif) <br> <a href="mailto:admin@wegosales.com" >admin@wegosales.com</a> <br>
                                        Jam kerja: 9.00am – 5.00pm (Senin – Sabtu)
                                        </span> -->
                                    </p>
                                    
                                </div>
                                    
                                <div class="clearfix"></div>
                                <div class="col-md-12 sub-footer">
                                    <span>Copyright 2018. &copy; Wegosales <a class="text-white" href="<?php echo base_url(); ?>" target="_blank"> 
                                    </a></span>
                                   
                                </div>
                            </div>
                        </div>
                </footer>

                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <?php require_once(APPPATH .'views/include/front_pelamar/inc_front_script.php'); ?>
</body>

</html>
