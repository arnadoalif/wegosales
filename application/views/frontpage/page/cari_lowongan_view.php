<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once(APPPATH .'views/include/front_pelamar/inc_front_style.php'); ?>
    <style type="text/css">
        .container-fluid {
            padding: 0px 15px 0 15px;
        }

        .btn-info, .btn-info.disabled {
            background: #b80000;
            border: 1px solid #8d6c2c;
        }

        .hidden-lg {
            display: none;
        }

        @media (min-width: 320px) and (max-width: 480px) {
            .hidden-sm {
                display: none;
            }

            .hidden-lg {
                display: grid;
            }

            body{ /*margin-top:20px;*/}
            .glyphicon { margin-right:5px;}
            .section-box h2 { margin-top:0px;}
            .section-box h2 a { font-size:15px; }
            .glyphicon-heart { color:#e74c3c;}
            .glyphicon-comment { color:#27ae60;}
            .separator { padding-right:5px;padding-left:5px; }
            .section-box hr {margin-top: 0;margin-bottom: 5px;border: 0;border-top: 1px solid rgb(199, 199, 199);}

            .panel.panel-horizontal {
                display:table;
                width:100%;
            }
            .panel.panel-horizontal > .panel-heading, .panel.panel-horizontal > .panel-body, .panel.panel-horizontal > .panel-footer {
                display:table-cell;
            }
            .panel.panel-horizontal > .panel-heading, .panel.panel-horizontal > .panel-footer {
                width: 25%;
                border:0;
                vertical-align: middle;
            }
            .panel.panel-horizontal > .panel-heading {
                border-right: 1px solid #ddd;
                border-top-right-radius: 0;
                border-bottom-left-radius: 4px;
                
            }
            .panel.panel-horizontal > .panel-footer {
                border-left: 1px solid #ddd;
                border-top-left-radius: 0;
                border-bottom-right-radius: 4px;
            }

            .email {
                font-size: 1em;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

	    
        }

        

    </style>
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header  id="top">
            <div class="fix-width">
                <nav class="navbar navbar-expand-md navbar-light p-l-0">
                    
                    <!-- Logo will be here -->
                    <a class="navbar-brand" href="<?php echo base_url('pilih-wilayah') ?>"><img src="<?php echo base_url('assets_front/images/logo.png') ?> " alt="Wegosales" /></a>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                    <!-- This is the navigation menu -->
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav ml-auto stylish-nav">
                             <?php if (empty($this->session->userdata('kode_pelamar'))): ?>
                                 <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('pelamar/login') ?>">Masuk</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('pelamar/register') ?>">Mendaftar</a> </li>
                             <?php else: ?>
                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->nama_pelamar ?></a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> 
                                        <a class="dropdown-item" href="<?php echo base_url('pelamar/admin/profil') ?>">Profil Saya</a> 
                                        <a class="dropdown-item" href="<?php echo base_url('pelamar/out-front') ?>">Logout</a> 
                                    </div>
                                </li>
                             <?php endif ?>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->

            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Featured Section -->
                <!-- ============================================================== -->
                <div class="col-lg-12">
                    <div class="text-center" > 
                        <h2 class="display-7">Lowongan Lokasi <?php echo $nama_provinsi ?></h2>
                    </div>
                </div>
                <?php if (count($data_lowongan) == 0): ?>
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Lowongan </strong> tidak tersedia <a href="<?php echo base_url('pilih-wilayah') ?>" title="">Kembali</a>
                    </div>
                <?php else: ?>
                    
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center"> 
                        </div>
                        <div class="max-width hidden-sm">
                            <div class="row text-center">
                            <?php foreach ($data_lowongan as $dt_lowongan): ?>
                               
                                <?php if ($dt_lowongan->status_sekarang == "Active"): ?>
                                    
                                        <div class="col-md-2">
                                            <!-- <h5 class="p-20"><?php echo $dt_lowongan->nama_lowongan ?></h5>  -->
                                            <a href="<?php echo base_url('detail-lowongan/'.$kode_provinsi.'/'.$dt_lowongan->slug_url) ?>" title="<?php echo $dt_lowongan->nama_lowongan ?>">
                                                <div class="image-box"> <img src="<?php echo base_url('assets_storage/img_iklan/'.$dt_lowongan->cover_iklan) ?> " alt="<?php echo $dt_lowongan->nama_lowongan ?>" class="img-responsive" />
                                                    <div class="image-overly"> 
                                                        <!-- <a class="btn btn-rounded btn-info" href="<?php echo base_url('detail-lowongan/'.$dt_lowongan->slug_url) ?>">Detail Lowongan</a>  -->
                                                        <button type="button" class="btn btn-rounded btn-info">Detail Lowongan</button>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    
                                <?php endif ?>     
                            <?php endforeach ?>      
                        </div>

                    </div>
                </div>
            
                <div class="container hidden-lg">
                    <div class="col-md-10">

                        <div class="row">

                            <?php foreach ($data_lowongan as $dt_m_lowongan): ?>
                                <?php if ($dt_m_lowongan->status_sekarang == "Active"): ?>
                            <div class="col-md-12" style="padding: 13px;">
                                <div class="row">
                                    <div class="panel panel-default panel-horizontal">
                                        <div class="panel-heading text-center" style="width:10em;">
                                            <a href="<?php echo base_url('detail-lowongan/'.$kode_provinsi.'/'.$dt_m_lowongan->slug_url) ?>" title="$dt_m_lowongan->nama_lowongan">
                                                <img src="<?php echo base_url('assets_storage/img_iklan/'.$dt_m_lowongan->cover_iklan); ?>" class="img-responsive" alt="<?php echo $dt_m_lowongan->nama_lowongan; ?>">
                                            </a>
                                        </div>
                                        
                                        <div class="panel-body">                
                                            <div class="col-xs-12 col-md-12 section-box">
                                                <h2>
                                                    <a href="<?php echo base_url('detail-lowongan/'.$kode_provinsi.'/'.$dt_m_lowongan->slug_url) ?>">
                                                        <?php echo $dt_m_lowongan->nama_lowongan; ?></a>
                                                </h2>
                                                <h5>
                                                    <?php 
                                                        $data_perusahaan = $this->db->query("SELECT * FROM Tbl_Perusahaan WHERE kode_perusahaan = '$dt_m_lowongan->kode_perusahaan'")->row();
                                                     ?>
                                                    <?php echo $data_perusahaan->Nama_Perusahaan  ?></h5>
                                                <p><i class="fa fa-map-marker"></i> <?php echo $data_perusahaan->Alamat_perusahaan ?></p>
                                                <p>
                                                    <?php if (empty($this->session->userdata('status_pelamar'))): ?>
                                                        <h5 class="card-subtitle mb-2 text-muted"><i class="fa fa-dollar"></i> <a href="<?php echo base_url('pelamar/login') ?>" title="login pelamar"> Login Wegosales</a></h5>
                                                    <?php else: ?>
                                                        
                                                        <?php if (!empty($dt_m_lowongan->show_salary)): ?>
                                                            <h5 class="panel-title">

                                                                Min
                                                                <?php if (!empty($dt_m_lowongan->salary_min)): ?>
                                                                    <?php echo "Rp ".number_format($dt_m_lowongan->salary_min) ?>
                                                                <?php else: ?>
                                                                    0
                                                                <?php endif ?>
                                                                
                                                                <?php if (!empty($dt_m_lowongan->salary_max)): ?>

                                                                    <?php echo "- Max Rp ".number_format($dt_m_lowongan->salary_max) ?>
                                                                <?php else: ?>
                                                                    
                                                                <?php endif ?>

                                                            </h5>
                                                        <?php else: ?>
                                                            <h5 class="panel-title"><i class="fa fa-dollar"></i> Tidak ditampilkan</h5>
                                                        <?php endif ?>

                                                    <?php endif ?>

                                                </p>
                                                
                                                <div>
                                                    <a class="btn btn-block btn-info" href="<?php echo base_url('detail-lowongan/'.$kode_provinsi.'/'.$dt_m_lowongan->slug_url) ?>" role="button">Selengkapnya</a>
                                                </div>
                                                <hr />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
				<?php endif ?>   

                            <?php endforeach ?>
                            
                        </div>
                    </div>
                </div>

                <?php endif ?>


                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <?php require_once(APPPATH .'views/include/front_pelamar/inc_front_script.php'); ?>
    <?php $this->load->view('frontpage/script_tracking'); ?>
</body>

</html>
