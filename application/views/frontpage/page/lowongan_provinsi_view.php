﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="situs online yang menyediakan layanan pencari lowongan, tau untuk mencari pekerjaan dengan bidang yang di atur dalam sistem wegosales untuk mencari kriteria pekerja dengan standar yang telah di tentukan">
    <meta name="author" content="wegosales">
    <meta name="keywords" itemprop="keywords" content="cari lowongan kerja, lowongan kerja jogja, loker kerja, cari kerja, lowongan kerja 2018, situs lowongan kerja, situs loker">
    <link rel="canonical" href="http://www.wegosales.com/" />
    <meta name="robots" content="follow,index" />

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets_front/images/logo_icon.png') ?>">
    <title>Wegosales</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets_pelamar/css/style.css'); ?> " rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url('assets_pelamar/css/colors/blue.css'); ?> " id="theme" rel="stylesheet">



    <style type="text/css">
        .text-info {
            color: #7a5409 !important;
        }

        .btn-info, .btn-info.disabled {
            background: #b80000;
            border: 1px solid #8d6c2c;
        }

        .btn-success, .btn-success.disabled {
            background: #7a5409;
            border: 1px solid #b80000;
        }

        .ribbon-info {
            background: #7a5409 !important;
        }

        a {
            color: #7a5409;
        }
        
        .btn-primary, .btn-primary.disabled {
            background: #b80000;
            border: 1px solid #7a5409;
            /*box-shadow: 0 2px 2px 0 rgba(116, 96, 238, 0.14), 0 3px 1px -2px rgba(116, 96, 238, 0.2), 0 1px 5px 0 rgba(116, 96, 238, 0.12);
            transition: 0.2s ease-in;*/
        }

        .card .card-subtitle {
            font-weight: 900;
            margin-bottom: 15px;
            color: #000;
         }

         .link_provinsi a:hover {

         }

        @media (min-width: 320px) and (max-width: 480px) {
             .decs-gaji {

             }

             .img-m {
                display: none;
             }

             .row {
                margin-top: -10px;
             }

            .hidden-xs {
                display: none;
            }

            .text-sm-h5 {
                font-size: 30px;
                text-align: center;
            }

            .p-sm {
                top: 25px;
            }

            #google_translate_element {
                display: none;
            }
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topheader" id="top">
            <div class="fix-width">
                <nav class="navbar navbar-expand-md navbar-light p-l-0">
                    
                    <a class="navbar-brand" href="<?php echo base_url() ?>"><img src="<?php echo base_url('assets_front/images/logo.png') ?> " alt="Wegosales" /></a>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                    
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav ml-auto stylish-nav">
                                <!-- <li class="nav-item">  </li> -->
                            <?php if (empty($this->session->userdata('kode_pelamar'))): ?>
                                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('pelamar/login') ?>">Masuk</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('pelamar/register') ?>">Mendaftar</a> </li>
                            <?php else: ?>
                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->nama_pelamar ?></a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> 
                                        <a class="dropdown-item" href="<?php echo base_url('pelamar/admin/profil') ?>">Profil Saya</a> 
                                        <a class="dropdown-item" href="<?php echo base_url('pelamar/out-front') ?>">Logout</a> 
                                    </div>
                                </li>
                            <?php endif ?>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>



        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
               <!-- Row -->
                <div class="row">
                    <div id="google_translate_element"></div>
                    <div class="col-lg-12">
                        <div class="text-center" > 
                            <h2 class="display-7">Pilih Lokasi Lowongan</h2>
                        </div>
                    </div>

                    <?php 
                        $this->load->model('Lib_model');
                        $m_lib = new Lib_model();
                     ?>
                    <?php $i=0; foreach ($data_provinsi as $dt_provinsi): ?>
                        <?php $total_lowongan = $m_lib->count_data_lowongan_daerah($dt_provinsi->nama_provinsi)->row(); ?>

                        <?php if ($total_lowongan->total > 0): ?>
                            
                        <?php if (($i++ % 2) == 0): ?>
                            <div class="col-lg-3 col-md-6">
                                <a href="<?php echo base_url('cari-lowongan/'.$dt_provinsi->kode_provinsi); ?>" title="<?php echo $dt_provinsi->nama_provinsi ?>" class="link_provinsi"> 
                                    <div class="card">
                                        <div class="d-flex flex-row">
                                            <div class="p-10 bg-info">
                                                <h3 class="text-white box m-b-0"><i class="fa fa-map-marker"></i></h3></div>
                                            <div class="align-self-center m-l-20">
                                                <h3 class="m-b-0 text-info">
                                                    <?php $total_lowongan = $m_lib->count_data_lowongan_daerah($dt_provinsi->nama_provinsi)->row(); 
                                                        echo $total_lowongan->total;
                                                    ?>
                                                </h3>
                                                <h5 class="text-muted m-b-0"><?php echo $dt_provinsi->nama_provinsi ?></h5></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php else: ?>
                            <div class="col-lg-3 col-md-6">
                                <a href="<?php echo base_url('cari-lowongan/'.$dt_provinsi->kode_provinsi); ?>" title="<?php echo $dt_provinsi->nama_provinsi ?>"> 
                                    <div class="card">
                                        <div class="d-flex flex-row">
                                            <div class="p-10 bg-danger">
                                                <h3 class="text-white box m-b-0"><i class="fa fa-map-marker"></i></h3></div>
                                            <div class="align-self-center m-l-20">
                                                <h3 class="m-b-0 text-info">
                                                    <?php $total_lowongan = $m_lib->count_data_lowongan_daerah($dt_provinsi->nama_provinsi)->row(); 
                                                        echo $total_lowongan->total;
                                                    ?>
                                                </h3>
                                                <h5 class="text-muted m-b-0"><?php echo $dt_provinsi->nama_provinsi ?></h5></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endif ?>

                        <?php endif ?>

                    
                    <!-- Column -->
                    <?php endforeach ?>
                    
                </div>
                <!-- Row -->
 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                © 2018 Wegosales
            </footer>
                            <!--  <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
                <footer class="footer row" style="background: #093055;">
                   
                </footer> -->
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/popper.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url('assets_pelamar/js/jquery.slimscroll.js'); ?> "></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url('assets_pelamar/js/waves.js'); ?> "></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url('assets_pelamar/js/sidebarmenu.js'); ?> "></script>
    <!--stickey kit -->
    <script src="<?php echo base_url('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/sparkline/jquery.sparkline.min.js'); ?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url('assets_pelamar/js/custom.min.js'); ?> "></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/styleswitcher/jQuery.style.switcher.js'); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var base_url = window.location.origin;
            $.ajax({
                type: "GET",
                url: base_url+"/assets_front/indonesia.json",
                dataType: "json",
                success: function (data_negara) {
                    $.each(data_negara, function(i, value) {
                        $('#inputNegara, .negara').append($('<option>').text(value).attr('value', value));
                    });
                },
                error: function (result) {
                }
            });
        });
    </script>
    <script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'id', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true, gaTrack: true, gaId: 'UA-115062658-1'}, 'google_translate_element');
    }
    </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <?php $this->load->view('frontpage/script_tracking'); ?>
   
</body>

</html>
