<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once(APPPATH .'views/include/front_pelamar/inc_front_style.php'); ?>
    <style type="text/css">
        .bg-info {
            background: #7a5409 !important;
        }
    </style>
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topheader" id="top">
            <div class="fix-width">
                <nav class="navbar navbar-expand-md navbar-light p-l-0">
                    
                    <!-- Logo will be here -->
                    <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets_front/images/logo.png') ?> " alt="Wegosales" /></a>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                    <!-- This is the navigation menu -->
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <!-- <ul class="navbar-nav ml-auto stylish-nav">
                            
                            <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('pelamar/login') ?>" target="_blank">Masuk</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('pelamar/register') ?>" target="_blank">Mendaftar</a> </li>
                            <li class="nav-item"> <a class="m-t-5 btn btn-info font-13" href="<?php echo base_url('perusahaan') ?> " style="width:120px;">Perusahaan</a> </li>
                        </ul> -->
                    </div>
                </nav>
            </div>
        </header>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Testimonial -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Call to action bar -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12 call-to-action bg-info">
                        <div class="fix-width">
                            <div class="row">
                                <div class="col-md-6 m-t-20 m-b-20"><span>Terimakasih, diri anda sudah terdaftar pada lowongan ini...</span></div>
                                <div class="col-md-6 align-self-center text-right"><a href="<?php echo base_url('pilih-wilayah'); ?>" class="btn btn-outline btn-rounded btn-default buy-btn m-t-10 m-b-10">Cari lowongan </a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->
                
                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <?php require_once(APPPATH .'views/include/front_pelamar/inc_front_script.php'); ?>
	<?php $this->load->view('frontpage/script_tracking'); ?>
</body>

</html>
