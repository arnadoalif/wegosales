﻿<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once(APPPATH .'views/include/front_pelamar/inc_front_style.php'); ?>
    <style type="text/css">
        .text-danger {
            color: #7a5409 !important;
        }

        .btn-info, .btn-info.disabled {
            background: #b80000;
            border: 1px solid #8d6c2c;
        }

        .btn-infohover {
            background: #8d6c2c;
            border: 1px solid #b80000;
        }

        .btn-info:hover, .btn-info.disabled:hover {
            background: #7a5409;
            opacity: 0.7;
            border: 1px solid #b80000;
        }

        .btn-success, .btn-success.disabled {
            background: #7a5409;
            border: 1px solid #b80000;
        }

        .img-costume img {
            max-width: 35%;
        }

        .contac-top {
            border-radius: 21px;
            position: fixed;
            bottom: 100px;
            right: 31px;
            margin: 0 auto;
            z-index: 12;
            padding: 10px;
            background: #093055;
            color: #fff;
            width: 327px;
            font-size: 12px;
        }

        #google_translate_element {
            position: absolute;
            margin: 0 auto;
            left: 7em;
        }

        /* Extra Small Devices, Phones */ 
        @media only screen and (min-width: 768px) {
            #img-costume {
                max-width: 50%;
            }
        }

        @media (min-width: 320px) and (max-width: 480px) {
            .hidden-sm {
                display: none;
            }

            .contac-top {
                border-radius: 21px;
                position: fixed;
                bottom: 30px;
                right: 31px;
                margin: 0 auto;
                z-index: 12;
                padding: 10px;
                background: #093055;
                color: #fff;
                width: 327px;
                font-size: 12px;
            }

            #google_translate_element {
                display: none;
            } 
        }

	.new {
                background-color: yellow;
                color: #f40c0c;
                border-radius: 5px;
                font-weight: bold;
                padding: 2px;
            }
    </style>
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header>
            <div class="fix-width">
                <nav class="navbar navbar-expand-md navbar-light p-l-0">
                    
                    <!-- Logo will be here -->
                    <a class="navbar-brand" href="<?php echo base_url(''); ?>"><img src="<?php echo base_url('assets_front/images/logo.png') ?> " alt="Wegosales" /></a>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> 
                    </button>
                    <div id="google_translate_element"></div>
                    <!-- This is the navigation menu -->
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav ml-auto stylish-nav">
                               
                            <?php if (empty($this->session->userdata('kode_pelamar'))): ?>
                                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('pelamar/login') ?>">Masuk</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('pelamar/register') ?>">Mendaftar</a> </li>
                             <?php else: ?>
                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->nama_pelamar ?></a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> 
                                        <a class="dropdown-item" href="<?php echo base_url('pelamar/admin/profil') ?>">Profil Saya</a> 
                                        <a class="dropdown-item" href="<?php echo base_url('pelamar/out-front') ?>">Logout</a> 
                                    </div>
                                </li>
                             <?php endif ?>

                            <li class="nav-item hidden-sm"> <a class="m-t-5 btn btn-info font-13" href="<?php echo base_url('perusahaan') ?> " style="width:120px;">Perusahaan</a> </li>
                            
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="fix-width">
                    <div class="row banner-text justify-content-center">
                        <div class="col-md-8 m-t-20 text-center">
                            <h1>Kumpulan lowongan kerja dan peluang usaha di <br> <span class="text-danger">Wegosales</span> </h1>
                            <p class="subtext"><span class="font-medium">Wegosales </span> Hadir untuk menyediakan informasi lowongan pekerjaan dan peluang usaha diseluruh indonesia</p>
                            <div class="down-btn"> 
                                <a href="<?php echo base_url('pelamar/register') ?>" class="btn btn-info m-b-10">DAFTAR SEKARANG</a> 
                                <a href="<?php echo base_url('pilih-wilayah') ?>" class="btn btn-success m-b-10">CARI LOWONGAN</a> 
                                <a href="<?php echo base_url('peluang-usaha') ?>" class="btn btn-danger m-b-10">PELUANG USAHA <sup class="new">new</sup></a> 
                            </div>
                        </div>    
                        <div class="col-md-12 text-center">
                            <div class="hero-banner"> <img src="<?php echo base_url('assets_front/images/we%20go%20sales%20banner.jpg') ?>" alt="Gambar Lowongan"  class="img-fluid"/> </div>
                        </div>     
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Featured Section -->
                <!-- ============================================================== -->
                
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center" > 
                                <h2 class="display-7">Bagaimana <span class="text-danger">Cara Melamar Pekerjaan</span>  Di <span class="text-danger">Wegosales.com</span> ?</h2>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row text-center">
                                <div class="col-md-3">
                                    <img style="max-width: 60%;" src="<?php echo base_url('assets_front/images/icon_front/icon_cari.png'); ?>" alt="Cari Lowongan Yang Sesuai" class="img-responsive" />
                                    <h5 class="p-20 text-danger">Cari Lowongan Yang Sesuai</h5>
                                </div>
                                <div class="col-md-3">
                                    <img style="max-width: 60%;" src="<?php echo base_url('assets_front/images/icon_front/registrasi.png'); ?>" alt="Registrasi Personal" class="img-responsive" />
                                    <h5 class="p-20 text-danger">Registrasi Personal</h5>
                                </div>
                                <div class="col-md-3">
                                    <img style="max-width: 60%;" src="<?php echo base_url('assets_front/images/icon_front/upload_cv.png'); ?>" alt="Isi Data Diri & Upload CV" class="img-responsive" />
                                    <h5 class="p-20 text-danger">Isi Data Diri & Upload CV Terbaru</h5>
                                </div>
                                <div class="col-md-3">
                                    <img style="max-width: 60%;" src="<?php echo base_url('assets_front/images/icon_front/tunggu_panggilan.png'); ?>" alt="Tunggu Panggilan Wawancara" class="img-responsive " />
                                    <h5 class="p-20 text-danger">Tunggu Penggailan Wawancara/Test</h5> 
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            

                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->
                    <div class="alert contac-top">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Catatan : Wegosales masih dalam pengembangan jika memiliki kendala dalam menggunakan wegosales bisa hubungi <br>0821-3798-4316 (Alif) <br> <a href="mailto:admin@wegosales.com" >admin@wegosales.com</a> <br>
                    Jam kerja: 9.00am – 5.00pm (Senin – Sabtu)
                    </div>
                    
                <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
                <footer class="footer row" style="background: #093055;">
                    <div class="fix-width">
                        <div class="row">
                            <div class="col-lg-3 col-md-6"><img src="<?php echo base_url('assets_front/images/logo.png'); ?>" alt="Wegosales" />
                                <p class="m-t-30">
                                    <span class="text-white">Wegosales.com
                                    </span> Situs website online yang menyediakan informasi lowongan kerja</p>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <!-- <p class="m-t-30">
                                        <span class="text-white">Catatan : Wegosales masih dalam pengembangan jika memiliki kendala dalam menggunakan wegosales bisa hubungi <br>0821-3798-4315 (Alif) <br> <a href="mailto:admin@wegosales.com" >admin@wegosales.com</a> <br>
                                        Jam kerja: 9.00am – 5.00pm (Senin – Sabtu)
                                        </span>
                                    </p> -->
                                    
                                </div>
                                    
                                <div class="clearfix"></div>
                                <div class="col-md-12 sub-footer">
                                    <span>Copyright 2018. &copy; Wegosales <a class="text-white" href="<?php echo base_url(); ?>" target="_blank"> 
                                    </a></span>
                                   
                                </div>
                            </div>
                        </div>
                </footer>
                    <!-- ============================================================== -->
                    <!-- End footer -->
                    <!-- ============================================================== -->
                </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <?php require_once(APPPATH .'views/include/front_pelamar/inc_front_script.php'); ?>
    <?php $this->load->view('frontpage/script_tracking'); ?>
</body>

</html>
