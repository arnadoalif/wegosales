﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="situs online yang menyediakan layanan pencari lowongan, tau untuk mencari pekerjaan dengan bidang yang di atur dalam sistem wegosales untuk mencari kriteria pekerja dengan standar yang telah di tentukan">
    <meta name="author" content="wegosales">
    <meta name="keywords" itemprop="keywords" content="cari lowongan kerja, lowongan kerja jogja, loker kerja, cari kerja, lowongan kerja 2018, situs lowongan kerja, situs loker">
    <link rel="canonical" href="http://www.wegosales.com/" />
    <meta name="robots" content="follow,index" />

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets_front/images/logo_icon.png') ?>">
    <title>Wegosales</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets_pelamar/css/style.css'); ?> " rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url('assets_pelamar/css/colors/blue.css'); ?> " id="theme" rel="stylesheet">



    <style type="text/css">
        .text-info {
            color: #7a5409 !important;
        }

        .btn-info, .btn-info.disabled {
            background: #b80000;
            border: 1px solid #8d6c2c;
        }

        .btn-success, .btn-success.disabled {
            background: #7a5409;
            border: 1px solid #b80000;
        }

        .ribbon-info {
            background: #7a5409 !important;
        }

        a {
            color: #7a5409;
        }
        
        .btn-primary, .btn-primary.disabled {
            background: #b80000;
            border: 1px solid #7a5409;
            /*box-shadow: 0 2px 2px 0 rgba(116, 96, 238, 0.14), 0 3px 1px -2px rgba(116, 96, 238, 0.2), 0 1px 5px 0 rgba(116, 96, 238, 0.12);
            transition: 0.2s ease-in;*/
        }

        .card .card-subtitle {
            font-weight: 900;
            margin-bottom: 15px;
            color: #000;
         }

        @media (min-width: 320px) and (max-width: 480px) {
             .decs-gaji {

             }

             .img-m {
                display: none;
             }

             .row {
                margin-top: -10px;
             }

            .hidden-xs {
                display: none;
            }

            .text-sm-h5 {
                font-size: 30px;
                text-align: center;
            }

            .p-sm {
                top: 25px;
            }

            #google_translate_element {
                display: none;
            }

	    .container-fluid {
                padding: 0px 15px 25px 10px;
		margin-top: -55px;
            }

        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topheader" id="top">
            <div class="fix-width">
                <nav class="navbar navbar-expand-md navbar-light p-l-0">
                    
                    <a class="navbar-brand" href="<?php echo base_url() ?>"><img src="<?php echo base_url('assets_front/images/logo.png') ?> " alt="Wegosales" /></a>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>

                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav ml-auto stylish-nav">
                             <?php if (empty($this->session->userdata('kode_pelamar'))): ?>
                                 <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('pelamar/login') ?>">Masuk</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('pelamar/register') ?>">Mendaftar</a> </li>
                             <?php else: ?>
                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->nama_pelamar ?></a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> 
                                        <a class="dropdown-item" href="<?php echo base_url('pelamar/admin/profil') ?>">Profil Saya</a> 
                                        <a class="dropdown-item" href="<?php echo base_url('pelamar/out-front') ?>">Logout</a> 
                                    </div>
                                </li>
                             <?php endif ?>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                       <div id="google_translate_element"></div>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(''); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('pilih-wilayah'); ?>">Pilih Wilayah</a></li>
                            <li class="breadcrumb-item active">Detail Lowongan</li>
                        </ol>
                    </div>
                    
                </div>
                
                <?php if (isset($_SESSION['sendmessage'])): ?>
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['sendmessage'] ?>
                    </div>
                <?php endif ?>
                <?php if (isset($_SESSION['error_data'])): ?>
                    <div class="alert alert-warning" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $_SESSION['error_data'] ?>
                    </div>
                <?php endif ?>

                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                
                <?php foreach ($data_lowongan as $dt_lowongan): ?>
                    
                    <div class="card">
                        <div class="card-body">
                            
                            <div class="row">
                                    <div class="col-lg-2">
                                        <img src="<?php echo base_url('assets_storage/img_perusahaan/'.$dt_lowongan->Logo_perusahaan); ?>" class="img-responsive img-m" alt="Image" style="max-width: 60%;">
                                    </div>

                                    <div class="col-lg-4">
                                        <h3 class="card-subtitle mb-2"><?php echo $dt_lowongan->nama_lowongan ?></h3>
                                        <?php if (!empty($dt_lowongan->Website)): ?>
                                            <a target="_blank" class="card-link" href="<?php echo $dt_lowongan->Website ?>" title="<?php echo $dt_lowongan->Nama_Perusahaan ?>"><?php echo $dt_lowongan->Nama_Perusahaan ?></a>
                                            <p class="card-text">Lowongan Tutup : <?php echo date("d/m/Y", strtotime($dt_lowongan->tanggal_tutup)); ?></p>
                                        <?php else: ?>
                                            <p class="card-text"><?php echo $dt_lowongan->Nama_Perusahaan ?></p>
                                            <p class="card-text">Lowongan Tutup : <?php echo date("d/m/Y", strtotime($dt_lowongan->tanggal_tutup)); ?></p>
                                        <?php endif ?>
                                    </div>

                                    <div class="col-lg-6 p-sm">

                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <?php if (empty($this->session->userdata('status_pelamar'))): ?>
                                                    <h4 class="card-subtitle mb-2 text-muted"><i class="fa fa-dollar"></i> <a href="<?php echo base_url('pelamar/login') ?>" title="login pelamar"> Login untuk melihat Gaji</a></h4>
                                                <?php else: ?>
                                                    
                                                    <?php if (!empty($dt_lowongan->show_salary)): ?>
                                                        <h3 class="panel-title"><i class="fa fa-dollar"></i> 

                                                            Min
                                                            <?php if (!empty($dt_lowongan->salary_min)): ?>
                                                                <?php echo "Rp ".number_format($dt_lowongan->salary_min) ?>
                                                            <?php else: ?>
                                                                0
                                                            <?php endif ?>
                                                            
                                                            <?php if (!empty($dt_lowongan->salary_max)): ?>

                                                                <?php echo "- Max Rp ".number_format($dt_lowongan->salary_max) ?>
                                                            <?php else: ?>
                                                                
                                                            <?php endif ?>

                                                        </h3>
                                                    <?php else: ?>
                                                        <h3 class="panel-title"><i class="fa fa-dollar"></i> Tidak ditampilkan</h3>
                                                    <?php endif ?>

                                                <?php endif ?>
                                                  
                                            </div>
                                            <div class="panel-body">
                                                <p><i class="fa fa-briefcase"></i> Min <?php echo $dt_lowongan->pengalaman ?> Tahun (<?php echo $dt_lowongan->posisi_spesialis ?>)</p>
                                                <p><i class="fa fa-map-marker"></i> <?php echo $dt_lowongan->Alamat_perusahaan ?>
                                                <?php if (!empty($dt_lowongan->Negara)): ?>
                                                    <label><?php echo ", ".$dt_lowongan->Negara ?></label>
                                                <?php endif ?></p>
                                            </div>
                                        </div>

                                    </div>
                            </div>

                        </div>
                    </div>

		    <div class="card ">
                        <div class="card-body">
                            <?php if (empty($this->session->userdata('kode_pelamar'))): ?>
                                <a href="<?php echo base_url('pelamar/register'); ?>" class="btn btn-default pull-right">Mendaftar</a>
                                <!-- <a href="<?php echo base_url('pelamar/login'); ?>" class="btn btn-primary">Masuk</a> -->
                                <a class="btn btn-primary pull-right" data-toggle="modal" href='#login'>Login</a>
                            <?php else: ?>
                                <!-- <a href="<?php echo $dt_lowongan->kode_lowongan ?>" class="btn btn-info">LAMAR SEKARANG</a> -->
                                <a class="btn btn-danger pull-right" data-toggle="modal" href='#lamar_lowongan'><i class="fa fa-briefcase"></i> LAMAR SEKARANG</a>
                            <?php endif ?>
                        </div>
                    </div>
                    
                    <?php if (!empty($dt_lowongan->status_requment)): ?>
                        <div class="ribbon-wrapper card">
                            <div class="ribbon ribbon-info">Metode Rekutmen</div>
                            <img src="<?php echo base_url('assets_storage/img_rekut/'.$dt_lowongan->metode_rekut) ?>" class="img-responsive" alt="Image">
 			   <?php if (!empty($dt_lowongan->cover_metode_rekut)): ?>
                                <img src="<?php echo base_url('assets_storage/img_metode/'.$dt_lowongan->cover_metode_rekut) ?>" class="img-responsive" alt="Image">
                            <?php else: ?>
                            <?php endif ?>
                            
                            <?php if (empty($dt_lowongan->deskripsi_metode_rekut)): ?>
                                <h3>Deskripsi</h3>
                                <p class="ribbon-content"><?php echo $dt_lowongan->deskripsi_metode_rekut ?></p>
                            <?php else: ?>
                                 
                                <p class="ribbon-content"><?php echo $dt_lowongan->deskripsi_metode_rekut ?></p>
                            <?php endif ?>
                            
                        </div>
                    <?php else: ?>
                        <div class="ribbon-wrapper card">
                            <div class="ribbon ribbon-default">Metode Rekutmen</div>
                            <img src="<?php echo base_url('assets_storage/img_rekut/3.png') ?>" class="img-responsive" alt="Image">
                        </div>
                    <?php endif ?>

                    
                    <div class="row">
                        <div class="col-xs-12 col-lg-6">
                            
                            <div class="ribbon-wrapper card">
                                <div class="ribbon ribbon-info">Deskripsi Kerjaan</div>
                                <p class="ribbon-content">
                                    <div class="row">
                                        <div class="col-xs-12 col-lg-6">
                                            <small class="text-muted">Jenis Pekerjaan</small>
                                            <h6><?php echo $dt_lowongan->pekerjaan_jenis ?></h6>
                                        </div>
                                        <div class="col-xs-12 col-lg-6">
                                            <small class="text-muted">Posisi Spesialis </small>
                                            <h6><?php echo $dt_lowongan->posisi_spesialis ?></h6>
                                        </div>
                                        <div class="col-xs-12 col-lg-6">
                                            <small class="text-muted">Keahlian</small>
                                            <h6><?php echo $dt_lowongan->keahlian ?></h6>
                                        </div>
                                        <div class="col-xs-12 col-lg-6">
                                            <small class="text-muted">Tingkat Pendidikan </small>
                                            <h6><?php echo $dt_lowongan->tingkat_pendidikan ?></h6>
                                        </div>
                                        <div class="col-xs-12 col-lg-6">
                                            <small class="text-muted">Bidang Studi </small>
                                            <h6><?php echo $dt_lowongan->bidang_studi ?></h6>
                                        </div>
                                    </div>
                                    <hr>

                                    <?php echo $dt_lowongan->deskripsi_pekerjaan ?>        
                                </p>
                            </div>

                        </div>
                        <div class="col-xs-12 col-lg-6">
                            
                            <div class="ribbon-wrapper card">
                                <div class="ribbon ribbon-info">Gambaran Perusahaan</div>

                                <p class="ribbon-content">

                                    <div class="row">
                                        <div class="col-xs-12 col-lg-6">
                                            <small class="text-muted">Alamat Email</small>
                                            <h6><?php echo $dt_lowongan->Alamat_Email ?></h6>
                                        </div>
                                        <div class="col-xs-12 col-lg-6">
                                            <small class="text-muted">Industri </small>
                                            <h6><?php echo $dt_lowongan->Industri ?></h6>
                                        </div>
                                        <div class="col-xs-12 col-lg-6">
                                            <small class="text-muted">Nomor Kantor</small>
                                            <h6><?php echo $dt_lowongan->Nomor_Tlp_kantor ?></h6>
                                        </div>
                                        <div class="col-xs-12 col-lg-6">
                                            <small class="text-muted">Website </small>
                                            <?php if (!empty($dt_lowongan->Website)): ?>
                                                <h6><a href="<?php echo $dt_lowongan->Website ?>" target="_blank" title="<?php $dt_lowongan->Nama_Perusahaan ?>"><?php echo $dt_lowongan->Website ?></a></h6>
                                            <?php else: ?>
                                                -
                                            <?php endif ?>
                                        </div>
                                        
                                    </div>

                                </p>
                            </div>

                            <div class="ribbon-wrapper card">
                                <div class="ribbon ribbon-info">Deskripsi Perusahaan</div>
                                <p class="ribbon-content"><?php echo $dt_lowongan->Deskripsi_perusahaan ?></p>
                            </div>

                        </div>
                    </div>

                    <div class="card ">
                        <div class="card-body">
                            
                            <!-- <a class="btn btn-info" href="javascript:fbShare('<?php echo current_url(); ?>', 'Fb Share', 'Facebook share popup', '<?php echo base_url('assets_storage/img_iklan/'.$dt_lowongan->cover_metode_rekut)?>', 520, 350)" role="button"><i class="fa fa-facebook"></i></a> -->

                            <?php if (empty($this->session->userdata('kode_pelamar'))): ?>
                                <a href="<?php echo base_url('pelamar/register'); ?>" class="btn btn-default pull-right">Mendaftar</a>
                                <!-- <a href="<?php echo base_url('pelamar/login'); ?>" class="btn btn-primary">Masuk</a> -->
                                <a class="btn btn-primary pull-right" data-toggle="modal" href='#login'>Login</a>
                            <?php else: ?>
                                <!-- <a href="<?php echo $dt_lowongan->kode_lowongan ?>" class="btn btn-info">LAMAR SEKARANG</a> -->
                                <a class="btn btn-danger pull-right" data-toggle="modal" href='#lamar_lowongan'><i class="fa fa-briefcase"></i> LAMAR SEKARANG</a>
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="modal fade" id="lamar_lowongan">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Lamar Lowongan <?php echo $dt_lowongan->nama_lowongan ?></h4>
                                </div>
                                
                                <form action="<?php echo base_url('frontpage/action_summit_lowongan') ?>" method="POST">
                                    
                                    <div class="modal-body">
                                        <input type="hidden" name="apikey" id="input" class="form-control" value="<?php echo $dt_lowongan->api_key ?>" required="required" pattern="" title="">
                                        <input type="hidden" name="kode_lowongan" id="input" class="form-control" value="<?php echo $dt_lowongan->kode_lowongan ?> " required="required" pattern="" title="">
                                        <input type="hidden" name="kode_perusahaan" id="input" class="form-control" value="<?php echo $dt_lowongan->kode_perusahaan ?> " required="required" pattern="" title="">
                                        <input type="hidden" name="nama_provinsi" id="input" class="form-control" value="<?php echo $nama_provinsi ?> " required="required" pattern="" title="">


                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                            <h3 class="text-info"><i class="fa fa-exclamation-circle"></i> Informasi</h3> ketika apply lowongan ini data diri akan diserahkan pada pembuka lowongan, sebaiknya perbaharui dahulu data diri anda...
                                            <a href="<?php echo base_url('pelamar/admin/profil') ?>" title="Lengkapi Profil">Lengkapi Profil</a>
                                        </div>


                                        <div class="form-group">
                                            <label for="promosikan">Promosikan diri Anda! (Disaranakan)</label>
                                            
                                                <textarea class="form-control counted" name="message" required placeholder="Beritahu perusahaan mengapa Anda paling sesuai untuk posisi ini. Sebutkan keahlian Anda dapat berkontribusi. hindari informasi umum seperti Saya orang yang bertanggung jawab." rows="5"></textarea>
                                                <!-- <h6 class="pull-right" id="counter">300 characters remaining</h6> -->
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary">Lamar Sekarang</button>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>

                <?php endforeach ?>


                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <div class="modal fade" id="login">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Login Wegosales</h4>
                            </div>
                            <div class="modal-body">
                                
                                <div class="login-box card">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material" id="loginform" method="POST" action="<?php echo base_url('login/action_login_pelamar_front'); ?>">
                                            <!-- <a href="javascript:void(0)" class="text-center db"><img src="<?php echo base_url('assets/images/logo-icon.png'); ?>" alt="Home" /><br/><img src="<?php echo base_url('assets/images/logo-text.png'); ?>" alt="Home" /></a> -->
                                            
                                            <div class="form-group m-t-40">
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="text" required="" name="email" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input class="form-control" type="password" required="" name="password" placeholder="Password">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group text-center m-t-20">
                                                <div class="col-xs-12">
                                                    <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                                                </div>
                                            </div>
                                           
                                        </form>
                                        
                                    </div>
                                </div>

                            </div>
                            
                        </div>
                    </div>
                </div>


 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                © 2018 Wegosales
            </footer>
                            <!--  <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
                <footer class="footer row" style="background: #093055;">
                   
                </footer> -->
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
    <script type="text/javascript">
         function fbShare(url, title, descr, image, winWidth, winHeight) {
            var winTop = (screen.height / 2) - (winHeight / 2);
            var winLeft = (screen.width / 2) - (winWidth / 2);
            window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        }
    </script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/popper.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url('assets_pelamar/js/jquery.slimscroll.js'); ?> "></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url('assets_pelamar/js/waves.js'); ?> "></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url('assets_pelamar/js/sidebarmenu.js'); ?> "></script>
    <!--stickey kit -->
    <script src="<?php echo base_url('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/sparkline/jquery.sparkline.min.js'); ?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url('assets_pelamar/js/custom.min.js'); ?> "></script>
    <script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'id', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true, gaTrack: true, gaId: 'UA-115062658-1'}, 'google_translate_element');
    }
    </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/styleswitcher/jQuery.style.switcher.js'); ?>"></script>
   <?php $this->load->view('frontpage/script_tracking'); ?>
</body>

</html>
