﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets_front/images/logo_icon.png') ?>">
    <title>Pelamar - Register</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets_pelamar/css/style.css'); ?> " rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url('assets_pelamar/css/colors/blue.css'); ?> " id="theme" rel="stylesheet">
    <style type="text/css">
        html body .text-primary {
            color: #b80000;
        }

        .btn-info, .btn-info.disabled {
            background: #b80000;
            border: 1px solid #8d6c2c;
        }

        .btn-info:hover {
            background: #8d6c2c;
            border: 1px solid #8d6c2c;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar" style="background-image:url(<?php echo base_url('assets/images/background/login-register.jpg') ?> );">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material" id="loginform" method="POST" action="<?php echo base_url('login/action_register_pelamar'); ?>">
                    <a href="<?php echo base_url(); ?>" class="text-center db"><img src="<?php echo base_url('assets_front/images/logo_icon.png') ?>" alt="Home" /></a>
                   <?php if (isset($_SESSION['sendmessage'])): ?>
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                            <?php echo $_SESSION['sendmessage'] ?>
                        </div>
                    <?php endif ?>
                    <?php if (isset($_SESSION['error_data'])): ?>
                        <div class="alert alert-warning" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                            <?php echo $_SESSION['error_data'] ?>
                        </div>
                    <?php endif ?>
                    <h3 class="box-title m-t-40 m-b-0">Register </h3><small></small>
                    <div class="form-group m-t-20">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" name="nama_pengguna" required="" placeholder="Nama Lengkap">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" name="email" required="" placeholder="Email">
                        </div>
                    </div>
                     <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" name="nomor_telepon" required="" placeholder="Nomor Telepon">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="password" required="" placeholder="Kata Sandi">
                        </div>
                    </div>
                    
                    <!-- <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary p-t-0">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup"> I agree to all <a href="#">Terms</a></label>
                            </div>
                        </div>
                    </div> -->
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Register</button>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>Sudah memiliki akun ? <a href="<?php echo base_url('pelamar/login'); ?>" class="text-info m-l-5"><b>Login</b></a></p>
                        </div>
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            <h3 class="text-info"><i class="fa fa-exclamation-circle"></i> Informasi</h3> 
                            akun wegosales hanya dapat digunakan satu email akun anda, apa bila mengalamai gangguan dalam register dapat hubungi pihak wegosales
                            <strong>0821-3798-4316</strong> Terimakasih
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/jquery/jquery.min.js'); ?>"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/popper.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url('assets_pelamar/js/jquery.slimscroll.js'); ?> "></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url('assets_pelamar/js/waves.js'); ?> "></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url('assets_pelamar/js/sidebarmenu.js'); ?> "></script>
    <!--stickey kit -->
    <script src="<?php echo base_url('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/sparkline/jquery.sparkline.min.js'); ?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url('assets_pelamar/js/custom.min.js'); ?> "></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('assets/plugins/styleswitcher/jQuery.style.switcher.js'); ?>"></script>
</body>

</html>