<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keterampilan_model extends CI_Model {

	public function view_all_keterampilan($table, $kode_pelamar) {
		$this->db->select('*');
		$this->db->where('kode_pelamar', $kode_pelamar);
		return $this->db->get($table, 1);
	}

	public function update_data_keterampilan($table, $kode_key, $data) {
		$this->db->select('*');
		$this->db->where('kode_pelamar', $kode_key);
		$this->db->update($table, $data);
	}

	public function insert_data_keterampilan($table, $data) {
		$this->db->insert($table, $data);
	}


	

}

/* End of file Keterampilan_model.php */
/* Location: ./application/models/Keterampilan_model.php */