<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelamar_model extends CI_Model {

	public function view_data_pelamar() {
		return $this->db->get('Tbl_Pelamar');
	}

	public function view_data_pelamar_by_id_pelamar($table, $id_pelamar) {
		$this->db->select('*');
		$this->db->where('kode_pelamar', $id_pelamar);
		return $this->db->get($table, 1);
	}

	public function view_data_pelamar_by_id_lowongan($table, $kode_lowongan) {
		$this->db->select('*');
		$this->db->where('kode_lowongan', $kode_lowongan);
		return $this->db->get($table);
	}

	public function insert_data_pelamar($table, $data) {
		$this->db->insert($table, $data);
	}

	public function cek_login_pelamar($table, $data) {
		return $this->db->get_where($table, $data);
	}

	public function update_data_profile($table, $key_data, $data) {
		$this->db->select('*');
		$this->db->where('kode_pelamar', $key_data);
		$this->db->update($table, $data);
	}

	public function edit_data_pelamar($table, $key_data, $where) {
		$this->db->select('*');
		$this->db->where('Kd_Pelamar', $Value);
		$this->db->update($table, $key_data);
	}

	public function delete_data_pelamar($table, $key_data) {
		$this->db->select('*');
		$this->db->where('Kd_Pelamar', $key_data);
		$this->db->delete($table);
	}

	public function update_data_password($table, $kode_pelamar, $where) {
		$this->db->select('*');
		$this->db->where('kode_pelamar', $kode_pelamar);
		$this->db->update($table, $where);
	}
	

}

/* End of file Pelamar_model.php */
/* Location: ./application/models/Pelamar_model.php */