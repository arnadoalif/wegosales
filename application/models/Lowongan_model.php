<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lowongan_model extends CI_Model {

	public function view_all_lowongan() {
		$this->db->select('*');
		return $this->db->get('Tbl_Lowongan');
	}

	public function view_all_lowongan_desc() {
		
		$tanggal_sekarang = $tgl = date('Y-m-d');
		$sql_lowongan_close_front = "select *,
									case when '$tanggal_sekarang' >= tanggal_tutup then 'Close'else 'Active' END status_sekarang
									from Tbl_Lowongan order by kode_lowongan desc";

	 	// $this->db->order_by('kode_lowongan', 'desc');
		// return $this->db->get('Tbl_Lowongan');
		return $this->db->query($sql_lowongan_close_front);
	}

	public function view_all_lowongan_and_perusahaan($kode_perusahaan) {
		$tanggal_sekarang = $tgl = date('Y-m-d');
		// $sql = " select * from Tbl_Lowongan, Tbl_Perusahaan where Tbl_Lowongan.kode_perusahaan = Tbl_Perusahaan.Kode_Perusahaan AND Tbl_Perusahaan.Kode_Perusahaan = '$kode_perusahaan'";

		$sql_lowongan_close = "
		                        select *,
								case when '$tanggal_sekarang' >= Tbl_Lowongan.tanggal_tutup then 'Close'else 'Active' END status_lowongan
								from Tbl_Lowongan, Tbl_Perusahaan where Tbl_Lowongan.kode_perusahaan = Tbl_Perusahaan.Kode_Perusahaan AND Tbl_Perusahaan.Kode_Perusahaan = '$kode_perusahaan'
		                     ";
		return $this->db->query($sql_lowongan_close);
	}

	public function view_all_lowongan_by_slug($table, $kode_lowongan) {
		$this->db->select('*');
		$this->db->where('slug_url', $kode_lowongan);
		return $this->db->get($table, 1);
	}

	public function count_pekerja($table, $clu, $kode_lowongan) {
		$query_total_pendaftar = "select count(Jenis_Kelamin) as $clu from $table where Jenis_Kelamin = '$clu' and Kd_Posisi_Kerja = '$kode_lowongan' group by Kd_Posisi_Kerja";
		// print_r($query_total_pendaftar);
		return $this->db->query($query_total_pendaftar);
	}

	public function count_lowongan_posisi($table, $kode_lowongan) {
		$query_total_lowongan = "select count(kode_lowongan) as tot_pegawai from $table where kode_lowongan = '$kode_lowongan'";
		return $this->db->query($query_total_lowongan);
	}

	public function edit_data_lowongan($table, $kode_key, $data) {
		$this->db->select('*');
		$this->db->where('kode_lowongan', $kode_key);
		$this->db->update($table, $data);
	}

	public function edit_data_lowongan_perusahaan($table, $kode_key, $kode_perusahaan, $data) {
		$this->db->select('*');
		$this->db->where('kode_perusahaan', $kode_perusahaan);
		$this->db->where('kode_lowongan', $kode_key);
		$this->db->update($table, $data);
	}

	public function delete_data_lowongan($table, $kode_key) {
		$this->db->select('*');
		$this->db->where('kode_lowongan', $kode_key);
		$this->db->delete($table);
	}

	

	public function view_all_lowongan_by_kode_lowongan($table, $kode_lowongan) {
		$this->db->select('*');
		$this->db->where('Kd_Lowongan', $kode_lowongan);
		return $this->db->get($table, 1);
	}

	public function insert_data_lowongan($table, $data) {
		$this->db->insert($table, $data);
	}

	

	public function buat_kode($nomor_terakhir, $kunci, $jumlah_karakter = 0) {
	    /* mencari nomor baru dengan memecah nomor terakhir dan menambahkan 1
	    string nomor baru dibawah ini harus dengan format XXX000000 
	    untuk penggunaan dalam format lain anda harus menyesuaikan sendiri */
	    // $nomor_baru = intval(substr($nomor_terakhir, strlen($kunci))) + 1;
	    // print_r($nomor_terakhir."<br>");
	    $nomor_baru = $nomor_terakhir + 1;
	//    menambahkan nol didepan nomor baru sesuai panjang jumlah karakter
		// print_r($nomor_baru);
	    $nomor_baru_plus_nol = str_pad($nomor_baru, $jumlah_karakter, "0", STR_PAD_LEFT);
	//    menyusun kunci dan nomor baru
	    $kode = $kunci . $nomor_baru_plus_nol;
	    return $kode;
	}	

	public function view_lowongan_by_kode_provinsi($kode_provinsi) {
		$tanggal_sekarang = $tgl = date('Y-m-d');
		$sql = "select *, case when '$tanggal_sekarang' >= tanggal_tutup then 'Close'else 'Active' END status_sekarang 
				from Tbl_Lowongan where kategori_daerah like '%$kode_provinsi%'";
		return $this->db->query($sql);
	}

}

/* End of file Lowongan_model.php */
/* Location: ./application/models/Lowongan_model.php */