<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Querycv_model extends CI_Model {

	public function view_all_profile_cv($kode_pelamar) {
		$SQL = " 
			select 
			    TOP 1
			    pelamar.kode_pelamar,
				pelamar.nama_pelamar,
				pelamar.alamat_email,
				pelamar.nomor_telepon,
				pelamar.kota,
				pelamar.provinsi,
				info.mata_uang,
				info.gaji,
				pengalaman.posisi_kerja,
				pengalaman.nama_perusahaan
			 from Tbl_Pelamar pelamar, Tbl_Info_Tambahan info, Tbl_Pengalaman_Kerja pengalaman
			  where pengalaman.kode_pelamar = '$kode_pelamar' order by kode_pengalaman desc;
			 ";
		return $this->db->query($SQL);
	}
	

}

/* End of file Querycv_model.php */
/* Location: ./application/models/Querycv_model.php */