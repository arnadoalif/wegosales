<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengalaman_model extends CI_Model {

	public function view_all_data_pengalaman_by_pelamar($table, $kode_pelamar) {
		$this->db->select('*');
		$this->db->where('kode_pelamar', $kode_pelamar);
		return $this->db->get($table);
	}

	public function insert_data_pengalaman_by_pelamar($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_data_pengalaman_by_pelamar($table, $kode_key, $kode_pelamar, $data) {
		$this->db->select('*');
		$this->db->where('kode_pengalaman', $kode_key);
		$this->db->where('kode_pelamar', $kode_pelamar);
		return $this->db->update($table, $data);
	}
	
	public function delete_data_pengalaman_by_pelamar($table, $kode_key, $kode_pelamar) {
		$this->db->select('*');
		$this->db->where('kode_pengalaman', $kode_key);
		$this->db->where('kode_pelamar', $kode_pelamar);
		$this->db->delete($table);
	}

}

/* End of file Pengalaman_model.php */
/* Location: ./application/models/Pengalaman_model.php */