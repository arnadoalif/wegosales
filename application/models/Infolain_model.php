<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Infolain_model extends CI_Model {

	public function view_all_info_lainnya_by_kode_pelamar($table, $kode_key) {
		$this->db->select('*');
		$this->db->where('kode_pelamar', $kode_key);
	    return $this->db->get($table, 1);
	}

	public function update_data_info_lainnya($table, $kode_key, $data) {
		$this->db->select('*');
		$this->db->where('kode_pelamar', $kode_key);
		$this->db->update($table, $data);
	}

	public function insert_data_info_lainnya($table, $data) {
		$this->db->insert($table, $data);
	}

}

/* End of file Infolain_model.php */
/* Location: ./application/models/Infolain_model.php */