<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan_model extends CI_Model {

	public function view_data_pendidikan_by_pelamar($table, $kode_pelamar)	 {
		$this->db->select('*');
		$this->db->where('kode_pelamar', $kode_pelamar);
		return $this->db->get($table);
	}

	public function insert_data_pendidikan_by_pelamar($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_data_pendidikan_by_pelamar($table, $key_kode_pendidikan, $key_data, $where) {
		$this->db->select('*');
		$this->db->where('kode_pendidikan', $key_kode_pendidikan);
		$this->db->where('kode_pelamar', $key_data);
		$this->db->update($table, $where);
	}

	public function delete_data_pendidikan_by_pelamar($table, $key_kode_pendidikan, $key_data) {
		$this->db->select('*');
		$this->db->where('kode_pendidikan', $key_kode_pendidikan);
		$this->db->where('kode_pelamar', $key_data);
		$this->db->delete($table);
	}

}

/* End of file Pendidikan_model.php */
/* Location: ./application/models/Pendidikan_model.php */