<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lib_model extends CI_Model {

	public function buat_kode($nomor_terakhir, $kunci, $jumlah_karakter = 0) {
	    /* mencari nomor baru dengan memecah nomor terakhir dan menambahkan 1
	    string nomor baru dibawah ini harus dengan format XXX000000 
	    untuk penggunaan dalam format lain anda harus menyesuaikan sendiri */
	    // $nomor_baru = intval(substr($nomor_terakhir, strlen($kunci))) + 1;
	    // print_r($nomor_terakhir."<br>");
	    $nomor_baru = $nomor_terakhir + 1;
	//    menambahkan nol didepan nomor baru sesuai panjang jumlah karakter
		// print_r($nomor_baru);
	    $nomor_baru_plus_nol = str_pad($nomor_baru, $jumlah_karakter, "0", STR_PAD_LEFT);
	//    menyusun kunci dan nomor baru
	    $kode = $kunci . $nomor_baru_plus_nol;
	    return $kode;
	}	

	function time_elapsed_string($datetime, $full = false) {
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}

	function cutText($text, $length, $mode = 2)
	{
		if ($mode != 1)
		{
			$char = $text{$length - 1};
			switch($mode)
			{
				case 2: 
					while($char != ' ') {
						$char = $text{--$length};
					}
				case 3:
					while($char != ' ') {
						$char = $text{++$num_char};
					}
			}
		}
		return substr($text, 0, $length);
	}

	public function create_slug($string, $space="-") {
        $string = utf8_encode($string);

        $string = preg_replace("/[^a-zA-Z0-9 \-]/", "", $string);
        $string = trim(preg_replace("/\\s+/", " ", $string));
        $string = strtolower($string);
        $string = str_replace(" ", $space, $string);

        return $string;
    }

    public function find_detect_tgl_lowongan_kadaluarsa() {
    	$date = date('y-m-d');
    	$sql_kadaluarsa = " 
            select @tgl_sekarang = '$date'
			SELECT TOP (1000) [kode_lowongan]
			      ,[api_key]
			      ,[slug_url]
			      ,[nama_lowongan]
			      ,[kode_perusahaan]
			      ,[tanggal_tutup]
			      ,[tgl_insert]
				  ,case when @tgl_sekarang >= tanggal_tutup then 'Close'
				  else 'Active' END sts,
				  @tgl_sekarang
			  FROM Tbl_Lowongan
    	 ";
    	 return $this->db->query($sql_kadaluarsa);
    }

    public function find_detect_tgl_lowongan_kadaluarsa_perusahaan($kode_perusahaan, $tanggal_tutup) {
    	$sql = " 	
			declare @tgl_sekarang date;  
			select @tgl_sekarang = '$tanggal_tutup'
			SELECT TOP (1000) [kode_lowongan]
			      ,[api_key]
			      ,[slug_url]
			      ,[nama_lowongan]
			      ,[kode_perusahaan]
			      ,[tanggal_tutup]
			      ,[tgl_insert]
				  ,case when @tgl_sekarang >= tanggal_tutup then 'Close'
				  else 'Active' END sts,
				  @tgl_sekarang
			  FROM Tbl_Lowongan WHERE kode_perusahaan = '$kode_perusahaan'
    	 ";
    	 return $this->db->query($sql);
    }

    public function view_provinsi() {
    	$this->db->select('*');
    	return $this->db->get('Tbl_Provinsi');
    }

    public function count_data_lowongan_daerah($nama_provinsi) {
    	$date = date('y-m-d');
    	//$sql = "select count(kode_lowongan) as total from Tbl_Lowongan where '2018-03-12' <= tanggal_tutup AND kategori_daerah LIKE '%$nama_provinsi%'";
	$sql = "select count(kode_lowongan) as total from Tbl_Lowongan where 
				REPLACE(CONVERT(varchar(10), CONVERT(datetime, GETDATE(), 111), 111), '/', '-')
				<= tanggal_tutup AND kategori_daerah LIKE '%$nama_provinsi%'";
    	return $this->db->query($sql);
    }

    public function calculate_age($kode_pelamar) {
    	$sql = "SELECT tahun FROM Tbl_Pelamar WHERE kode_pelamar = '$kode_pelamar'";
    	$result = $this->db->query($sql)->row();

    	if (!empty($result->tahun)) {
    		$date = $result->tahun;
			$data = intval(date('Y', time() - strtotime($date))) - 1970;
			return $data ." Tahun";
    	} else {
    		return 0;
    	}
    }

}

/* End of file Lib_model.php */
/* Location: ./application/models/Lib_model.php */