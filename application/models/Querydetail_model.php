<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Querydetail_model extends CI_Model {

	public function view_appaly() {
		return $this->db->get('Tbl_Apply');
	}

	public function view_data_perusahaan_lowongan($slug_url) {
		$sql_query = "select * from Tbl_Perusahaan , Tbl_Lowongan where Tbl_Perusahaan.Kode_Perusahaan = Tbl_Lowongan.kode_perusahaan and Tbl_Lowongan.slug_url = '$slug_url'";
		// $this->db->select('*');
		// $this->db->where('slug_url', $slug_url);
		// $this->db->from($table1);
		// $this->db->from($table2);
		// return $this->db->get();
		return $this->db->query($sql_query);
	}

	public function view_data_pelamar_detail_lowongan($kode_lowongan) {
		$sql_pelamar = "select pelamar.*, job.* from Tbl_Apply job, Tbl_Pelamar pelamar WHERE job.kode_pelamar = pelamar.kode_pelamar AND kode_lowongan = '$kode_lowongan'";
		// $sql_pelamar = "select pelamar.*, info.* from 
		// 				Tbl_Apply job, 
		// 				Tbl_Pelamar pelamar,
		// 				Tbl_Info_Tambahan info
		// 				WHERE job.kode_pelamar = info.kode_pelamar AND job.kode_pelamar = pelamar.kode_pelamar AND kode_lowongan = '$kode_lowongan'";
		return $this->db->query($sql_pelamar);
	}

	public function view_data_pelamar_detail_lowongan_cv($kode_pelamar, $kode_lowongan) {
		$sql_pelamar = "select pelamar.*, job.* from Tbl_Apply job, Tbl_Pelamar pelamar WHERE job.kode_pelamar = pelamar.kode_pelamar AND kode_lowongan = '$kode_lowongan' AND pelamar.kode_pelamar = '$kode_pelamar'";
		return $this->db->query($sql_pelamar);
	}

	public function insert_apply_lowongan($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_apply_lowongan($table, $kode_lowongan, $kode_pelamar, $data) {
		$this->db->select('*');
		$this->db->where('kode_lowongan', $kode_lowongan);
		$this->db->where('kode_pelamar', $kode_pelamar);
		$this->db->update($table, $data);
	} 

	public function delete_data_apply_lowongan($table, $kode_lowongan) {
		$this->db->select('*');
		$this->db->where('kode_lowongan', $kode_lowongan);
		$this->db->delete($table);
	}

	public function view_lowongan_by_perusahaan($kode_perusahaan) {
		// $sql = "SELECT * from Tbl_Perusahaan, Tbl_Lowongan where Tbl_Perusahaan.Kode_Perusahaan = Tbl_Lowongan.kode_perusahaan and Tbl_Perusahaan.Kode_Perusahaan = '$kode_perusahaan'";
		$tanggal_sekarang = $tgl = date('Y-m-d');
		$sql = "SELECT *, 
				case when '$tanggal_sekarang' >= Tbl_Lowongan.tanggal_tutup then 'Close'else 'Active' END status_sekarang
 				from Tbl_Perusahaan, Tbl_Lowongan where Tbl_Perusahaan.Kode_Perusahaan = Tbl_Lowongan.kode_perusahaan and Tbl_Perusahaan.Kode_Perusahaan = '$kode_perusahaan' ";
		return $this->db->query($sql);
	}

	public function view_data_pelamar_by_super_root($kode_perusahaan) {
		$sql = "select 
				    Tbl_Apply.kode_lowongan,
					Tbl_Perusahaan.Nama_Perusahaan,
					Tbl_Lowongan.nama_lowongan,
					Tbl_Pelamar.nama_pelamar,
					Tbl_Pelamar.img_profile
				from Tbl_Apply, Tbl_Lowongan, Tbl_Perusahaan, Tbl_Pelamar
				Where 
				Tbl_Apply.kode_lowongan = Tbl_Lowongan.kode_lowongan and 
				Tbl_Perusahaan.Kode_Perusahaan = Tbl_Lowongan.kode_perusahaan and
				Tbl_Apply.kode_pelamar = Tbl_Pelamar.kode_pelamar 
				and Tbl_Apply.kode_perusahaan = '$kode_perusahaan'
		      ";
		return $this->db->query($sql);
	}

}

/* End of file Querydetail_model.php */
/* Location: ./application/models/Querydetail_model.php */