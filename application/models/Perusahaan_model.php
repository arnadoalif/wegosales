<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan_model extends CI_Model {

	public function view_data_perusahaan() {
		return $this->db->get('Tbl_Perusahaan');
	}	

	public function find_data_perusahaan_by_nomor_telepon($table, $nomor_telepon) {
		$this->db->where('nomor_telepon', $nomor_telepon);
		return $this->db->get($table);
	}

	public function find_perusahaan_by_kode_api($table, $apikey, $kode_perusahaan) {
		$this->db->select('*');
		$this->db->where('Api_Key', $apikey);
		$this->db->where('Kode_Perusahaan', $kode_perusahaan);
		return $this->db->get($table, 1);
	}

	public function insert_data_perusahaan($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_data_perusahaan($table, $kode_key, $apikey, $where) {
		$this->db->select('*');
		$this->db->where('Kode_Perusahaan', $kode_key);
		$this->db->where('Api_Key', $apikey);
		$this->db->update($table, $where);
	}

	public function cek_login_perusahaan($table,$where){		
		return $this->db->get_where($table,$where);
	}

	public function delete_data_perusahaan($table, $kode_key) {
		$this->db->select('*');
		$this->db->where('kode_perusahaan', $kode_perusahaan);
		$this->db->delete($table);
	}

	public function update_data_password($table, $apikey, $where) {
		$this->db->select('*');
		$this->db->where('Api_Key', $apikey);
		$this->db->update($table, $where);
	}

	

}

